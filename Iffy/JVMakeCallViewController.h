//
//  JVMakeCallViewController.h
//  Iffy
//
//  Created by Joseph Ayo-Vaughan on 2/2/14.
//  Copyright (c) 2014 Joseph Ayo-Vaughan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Sinch/Sinch.h>

@interface JVMakeCallViewController : UIViewController
@property(nonatomic, readwrite, strong) id<SINCallClient> client;

@property (weak, nonatomic) IBOutlet UIButton *startCallButton;

- (IBAction)startCall:(UIButton *)sender;
@end
