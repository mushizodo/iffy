//
//  ProfileUserInfoCell.h
//  Iffy
//
//  Created by Joseph Ayo-Vaughan on 2/1/14.
//  Copyright (c) 2014 Joseph Ayo-Vaughan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProfileUserInfoSpaceCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *headerImageView;

@property (weak, nonatomic) IBOutlet UIButton *viewPhotoButton;
@property (weak, nonatomic) IBOutlet UIImageView *profilePhotoView;
@property (weak, nonatomic) IBOutlet UIImageView *profilePhotoViewBG;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UIImageView *whiteCutout;
@property (weak, nonatomic) IBOutlet UIButton *followersButton;
@property (weak, nonatomic) IBOutlet UIButton *followingButton;
@end
