//
//  JVMainTabBarController.h
//  Iffy
//
//  Created by Joseph Ayo-Vaughan on 4/25/14.
//  Copyright (c) 2014 Joseph Ayo-Vaughan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Sinch/Sinch.h>

@interface JVMainTabBarController : UITabBarController <SINClientDelegate>

@property(nonatomic, readwrite, strong) id<SINClient> client;

- (void)updateTabBarForWhiteBackground;

@end
