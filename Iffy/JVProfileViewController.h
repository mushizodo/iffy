//
//  JVProfileViewController.h
//  Iffy
//
//  Created by Joseph Ayo-Vaughan on 3/5/14.
//  Copyright (c) 2014 Joseph Ayo-Vaughan. All rights reserved.
//

#import <Parse/Parse.h>

@interface JVProfileViewController : PFQueryTableViewController <UISearchBarDelegate, UISearchDisplayDelegate, UIScrollViewDelegate>

- (IBAction)viewProfilePic:(UIButton *)sender;
- (void)refreshSearchTableView;
- (void)refreshMainTableView;
- (void)beginQueriesWithSearchText: (NSString *)searchText;

@end
