//
//  ProfileViewController.m
//  Iffy
//
//  Created by Joseph Ayo-Vaughan on 1/25/14.
//  Copyright (c) 2014 Joseph Ayo-Vaughan. All rights reserved.
//

#import "ProfileViewController.h"
#import <Parse/Parse.h>
#import <QuartzCore/QuartzCore.h>
#import "JVHelperTools.h"
#import "SearchTableViewCell.h"

#define kUsersChosen @"users"
#define kPlacesChosen @"places"
#define kTopicsChosen @"topics"

@interface ProfileViewController () {
	NSMutableArray *resultObjects;
	__block int queryCount;
	UIView *tableLoadingView;
	JVHelperTools *jvht;
	NSString *chosenSearch;
	UITableViewController *tvc;
}

@end

@implementation ProfileViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	[[UIApplication sharedApplication]setStatusBarStyle:UIStatusBarStyleLightContent];
	jvht = [JVHelperTools new];
	
	tvc = [UITableViewController new];
	tvc.tableView = self.searchDisplayController.searchResultsTableView;
	UIRefreshControl *refreshControl = [[UIRefreshControl alloc]init];
	[refreshControl setTintColor:[jvht getOrangeButtonNormalColor]];
	[refreshControl addTarget:self action:@selector(refreshSearchTableView) forControlEvents:UIControlEventValueChanged];
	tvc.refreshControl = refreshControl;
	
	UITableViewController *mainTVC = [UITableViewController new];
	mainTVC.tableView = self.mainTableView;
	mainTVC.tableView.delegate = self;
	[self.view.superview addSubview:self.mainTableView];
	
	queryCount = 0;
	resultObjects = [[NSMutableArray alloc]init];
	
	if (is5thGen) {
		tableLoadingView = [[UIView alloc]initWithFrame:CGRectMake(0, 200, 320, 75)];
	} else {
		tableLoadingView = [[UIView alloc]initWithFrame:CGRectMake(0, 100, 320, 75)];
	}
	
	[jvht addProgressviewToSuperview:tableLoadingView];
	
	chosenSearch = @"users";
	
	if (SYSTEM_VERSION_LESS_THAN(@"7.0")) {
		self.searchDisplayController.searchBar.frame = CGRectMake(self.searchDisplayController.searchBar.frame.origin.x, -44, self.searchDisplayController.searchBar.frame.size.width, self.searchDisplayController.searchBar.frame.size.height);
		NSLog(@"%f", self.searchDisplayController.searchBar.frame.origin.y);
	}
	
	[self.searchDisplayController.searchResultsTableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
	
	[self.searchDisplayController.searchBar setScopeBarButtonTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[jvht getRedButtonNormalColor], UITextAttributeTextColor, [UIFont fontWithName:@"GillSans-Light" size:15], UITextAttributeFont, [UIColor clearColor], UITextAttributeTextShadowColor, nil] forState:UIControlStateNormal];
	
	[self.searchDisplayController.searchBar setScopeBarButtonTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[jvht getButtonHighlightedTextColor], UITextAttributeTextColor, [UIFont fontWithName:@"GillSans-Light" size:15], UITextAttributeFont, [UIColor clearColor], UITextAttributeTextShadowColor, nil] forState:UIControlStateHighlighted];
	
	[self.searchDisplayController.searchBar setScopeBarButtonTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor whiteColor], UITextAttributeTextColor, [UIFont fontWithName:@"GillSans-Light" size:15], UITextAttributeFont, [UIColor clearColor], UITextAttributeTextShadowColor, nil] forState:UIControlStateSelected];
		
	[self.searchDisplayController.searchBar setTintColor:[jvht getRedButtonHighlightedColor]];
		
	self.mainScrollView.contentSize = CGSizeMake(320.0, 1000.0);
	self.mainScrollView.delegate = self;
	
	self.profilePhotoView.layer.masksToBounds = YES;
	self.profilePhotoView.layer.cornerRadius = self.profilePhotoView.frame.size.height/2;
	
	self.profilePhotoViewBG.alpha = 0.5;
	self.profilePhotoViewBG.layer.borderColor = [UIColor colorWithRed:155.0/255.0 green:50.0/255.0 blue:50.0/255.0 alpha:0.8].CGColor;
//	self.profilePhotoViewBG.layer.borderWidth = 1.0;
	self.profilePhotoViewBG.layer.cornerRadius = self.profilePhotoViewBG.frame.size.height/2;
	
	self.viewPhotoButton.layer.borderColor = [UIColor colorWithRed:40.0/255.0 green:40.0/255.0 blue:40.0/255.0 alpha:0.2].CGColor;
//	self.viewPhotoButton.layer.borderWidth = 1.0;
	self.viewPhotoButton.layer.cornerRadius = self.viewPhotoButton.frame.size.height/2;
	
	self.nameLabel.text = [PFUser currentUser][@"name"];
	
	UIBarButtonItem *addButton = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"search_alien.png"] style:UIBarButtonItemStylePlain target:self action:@selector(goToSearch)];
	self.navigationItem.leftBarButtonItem = addButton;
	
	[self.searchDisplayController.searchResultsTableView registerNib:[UINib nibWithNibName:@"SearchTableViewCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"JVCell"];
	
//	if (!isiPhone5) {
//		self.searchDisplayController.searchBar.frame = CGRectOffset(self.searchDisplayController.searchBar.frame, 0, -44.0);
//	}
	
}

- (void)viewDidAppear:(BOOL)animated {
	[super viewDidAppear:animated];
	
	NSString *docDir = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)[0];
	NSString *smallImagePath = [docDir stringByAppendingPathComponent:@"user.jpg"];
	
	if ([[NSFileManager defaultManager]fileExistsAtPath:smallImagePath]) {
		UIImage *profImage = [UIImage imageWithContentsOfFile:smallImagePath];
		
		self.profilePhotoView.image = profImage;
		
	}else if ([PFUser currentUser][@"photo_small"] != nil) {
		
		PFFile *imageFile = [PFUser currentUser][@"photo_small"];
		
		[imageFile getDataInBackgroundWithBlock:^(NSData *data, NSError *error) {
			if (!error) {
				self.profilePhotoView.image = [UIImage imageWithData:data];
			} else {
				self.profilePhotoView.image = [UIImage imageNamed:@"default_photo.png"];
			}
		}];
		
	}else {
		self.profilePhotoView.image = [UIImage imageNamed:@"default_photo.png"];
	}
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)viewProfilePic:(UIButton *)sender {
}

- (void)goToSearch {
	[self.searchDisplayController.searchBar becomeFirstResponder];
}

- (void)refreshSearchTableView {
	[self beginQueriesWithSearchText:self.searchDisplayController.searchBar.text];
}

- (void)beginQueriesWithSearchText: (NSString *)searchText {
	[resultObjects removeAllObjects];
	
	queryCount = 0;
	[self.searchDisplayController.searchResultsTableView reloadData];
	
	[self.view.superview addSubview:tableLoadingView];
	
	if (![searchText isEqualToString:@""]) {
		PFQuery *countQuery;
		if ([chosenSearch isEqualToString:kUsersChosen]) {
			countQuery = [PFUser query];
		} else if([chosenSearch isEqualToString:kPlacesChosen]) {
			countQuery = [PFQuery queryWithClassName:@"Places"];
		} else {
			countQuery = [PFQuery queryWithClassName:@"Topics"];
		}
		//		countQuery  = [PFUser query];
		countQuery.limit = 10;
		[countQuery whereKey:@"name" hasPrefix:searchText];
		[countQuery whereKey:@"canIndex" equalTo:[NSNumber numberWithBool:YES]];
		[countQuery countObjectsInBackgroundWithBlock:^(int number, NSError *error) {
			if (!error) {
				queryCount = number;
				if (queryCount == 0) {
					[tableLoadingView removeFromSuperview];
				}
				[self.searchDisplayController.searchResultsTableView reloadData];
			} else {
				queryCount = 0;
				[tableLoadingView removeFromSuperview];
				[self.searchDisplayController.searchResultsTableView reloadData];
			}
		}];
		
		PFQuery *findQuery;
		if ([chosenSearch isEqualToString:kUsersChosen]) {
			findQuery = [PFUser query];
		} else if([chosenSearch isEqualToString:kPlacesChosen]) {
			findQuery = [PFQuery queryWithClassName:@"Places"];
		} else {
			findQuery = [PFQuery queryWithClassName:@"Topics"];
		}
		//		findQuery = [PFUser query];
		findQuery.limit = 10;
		[findQuery whereKey:@"name" hasPrefix:searchText];
		[findQuery whereKey:@"canIndex" equalTo:[NSNumber numberWithBool:YES]];
		[findQuery findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
			if (!error) {
				for (PFObject *obj in objects) {
					[resultObjects addObject:obj];
				}
				
				[self.searchDisplayController.searchResultsTableView reloadData];
			}
		}];
	} else {
		[tableLoadingView removeFromSuperview];
	}
	
	[self.searchDisplayController.searchResultsTableView reloadData];
	
	[tvc.refreshControl endRefreshing];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	return queryCount;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"JVCell";
	
	SearchTableViewCell *cell = (SearchTableViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
	
	if (cell == nil) {
		NSLog(@"cell is nil");
		NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"SearchTableViewCell" owner:self options:nil];
		cell = [nib objectAtIndex:0];
	}
	
	cell.userImageView.layer.masksToBounds = YES;
	cell.userImageView.layer.cornerRadius = cell.userImageView.frame.size.height/2;
	
//	double delayInSeconds = 2.0;
//	dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
//	dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
		[tableLoadingView removeFromSuperview];
		if ([resultObjects count] != 0 && [resultObjects count] == queryCount && indexPath.row <= [resultObjects count]) {
			
			PFUser *user = [resultObjects objectAtIndex:indexPath.row];
			cell.userNameLabel.text = user[@"name"];
			cell.userInfoLabel.text = user[@"email"];
			if ([cell.userNameLabel.text isEqualToString:@""]) {
				cell.userNameLabel.text = user[@"name"];
				[self.searchDisplayController.searchResultsTableView reloadData];
			}
			
			if ([cell.userInfoLabel.text isEqualToString:@""]) {
				cell.userInfoLabel.text = user[@"email"];
				[self.searchDisplayController.searchResultsTableView reloadData];
			}
			if (indexPath.row % 2 == 0) {
				cell.determinedFwdBgView.backgroundColor = [jvht getRedButtonNormalColor];
			} else {
				cell.determinedFwdBgView.backgroundColor = [jvht getOrangeButtonNormalColor];
			}
			if (user[@"photo_small"] != nil) {
				PFFile *imageFile = user[@"photo_small"];
				[imageFile getDataInBackgroundWithBlock:^(NSData *data, NSError *error) {
					if (!error) {
						NSLog(@"image!!");
						cell.userImageView.image = [UIImage imageWithData:data];
						if (cell.userImageView.image == nil) {
//							cell.userImageView.image = [UIImage imageWithData:data];
//							[self.searchDisplayController.searchResultsTableView reloadData];
						}
					}
				}];
			} else {
				cell.userImageView.image = [UIImage imageNamed:@"default_photo.png"];
			}
		}
//	});
    return cell;
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
	[self beginQueriesWithSearchText:searchText];
}

- (BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchString:(NSString *)searchString {
	[self beginQueriesWithSearchText:searchString];
	return YES;
}

- (void)searchBar:(UISearchBar *)searchBar selectedScopeButtonIndexDidChange:(NSInteger)selectedScope {
	NSLog(@"changed");
	queryCount = 0;
	[resultObjects removeAllObjects];
	if (selectedScope == 0) {
		chosenSearch = kUsersChosen;
		[self.searchDisplayController.searchBar setTintColor:[jvht getRedButtonNormalColor]];
		[self.searchDisplayController.searchBar setScopeBarButtonTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[jvht getRedButtonNormalColor], UITextAttributeTextColor, [UIFont fontWithName:@"GillSans-Light" size:15], UITextAttributeFont, [UIColor clearColor], UITextAttributeTextShadowColor, nil] forState:UIControlStateNormal];
		[self.searchDisplayController.searchResultsTableView reloadData];
	} else if (selectedScope == 1) {
		chosenSearch = kPlacesChosen;
		[self.searchDisplayController.searchBar setTintColor:[UIColor colorWithRed:40.0/255.0 green:40.0/255.0 blue:40.0/255.0 alpha:1.0]];
		[self.searchDisplayController.searchBar setScopeBarButtonTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor colorWithRed:40.0/255.0 green:40.0/255.0 blue:40.0/255.0 alpha:1.0], UITextAttributeTextColor, [UIFont fontWithName:@"GillSans-Light" size:15], UITextAttributeFont, [UIColor clearColor], UITextAttributeTextShadowColor, nil] forState:UIControlStateNormal];
		[self.searchDisplayController.searchResultsTableView reloadData];
	} else {
		chosenSearch = kTopicsChosen;
		[self.searchDisplayController.searchBar setTintColor:[jvht getOrangeButtonNormalColor]];
		[self.searchDisplayController.searchBar setScopeBarButtonTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[jvht getOrangeButtonNormalColor], UITextAttributeTextColor, [UIFont fontWithName:@"GillSans-Light" size:15], UITextAttributeFont, [UIColor clearColor], UITextAttributeTextShadowColor, nil] forState:UIControlStateNormal];
		[self.searchDisplayController.searchResultsTableView reloadData];
	}
	
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
	[self beginQueriesWithSearchText:searchBar.text];
}

- (BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchScope:(NSInteger)searchOption {
	[self beginQueriesWithSearchText:controller.searchBar.text];
	return YES;
}

- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar {
	if (SYSTEM_VERSION_LESS_THAN(@"7.0")) {
		if (searchBar.frame.origin.y == -44 && [searchBar.text isEqualToString:@""]) {
			searchBar.frame = CGRectMake(searchBar.frame.origin.x, 0, searchBar.frame.size.width, searchBar.frame.size.height);
		}
	}
	
}

- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar {
	if (SYSTEM_VERSION_LESS_THAN(@"7.0") && [searchBar.text isEqualToString:@""]) {
		searchBar.frame = CGRectMake(searchBar.frame.origin.x, -44, searchBar.frame.size.width, searchBar.frame.size.height);
	}
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar {
	if (SYSTEM_VERSION_LESS_THAN(@"7.0") && ![searchBar.text isEqualToString:@""]) {
		searchBar.frame = CGRectMake(searchBar.frame.origin.x, -44, searchBar.frame.size.width, searchBar.frame.size.height);
	}
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 60;
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath {
	return 60;
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
//	NSLog(@"%f", scrollView.contentOffset.y);
}

- (void)slowDownScrollWithGestureRecognizer: (UIPanGestureRecognizer *)recognizer {
	
}



@end
