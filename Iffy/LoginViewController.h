//
//  ViewController.h
//  Iffy
//
//  Created by Joseph Ayo-Vaughan on 1/5/14.
//  Copyright (c) 2014 Joseph Ayo-Vaughan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TextfieldLogin.h"
#import "JVHelperTools.h"

@class LoginViewController;

@interface LoginViewController : UIViewController <JVHelperToolsDelegate, UITextFieldDelegate, UIAlertViewDelegate>
@property (weak, nonatomic) IBOutlet UIButton *signInTwitter;
@property (weak, nonatomic) IBOutlet UIImageView *twitterBird;
@property (weak, nonatomic) IBOutlet UIButton *signUpButton;
@property (weak, nonatomic) IBOutlet UIButton *loginButton;
@property (weak, nonatomic) IBOutlet TextfieldLogin *nameField;
@property (weak, nonatomic) IBOutlet UIButton *iffyLogo;
@property (weak, nonatomic) IBOutlet UIButton *infoButton;
@property (weak, nonatomic) IBOutlet TextfieldLogin *emailField;
@property (weak, nonatomic) IBOutlet TextfieldLogin *passwordField;
@property (weak, nonatomic) IBOutlet UIButton *forgotPasswordButton;
@property (weak, nonatomic) IBOutlet UIButton *resignerButton;
@property (weak, nonatomic) IBOutlet UIImageView *mainBgImage;
@property (weak, nonatomic) IBOutlet UIImageView *faceLogo;

@property (strong, nonatomic) NSString *userName;

- (IBAction)loginUser:(id)sender;
- (IBAction)animateForLogin:(id)sender;
- (IBAction)resignKeyboard:(id)sender;
- (IBAction)signupUser:(id)sender;
- (IBAction)animateForSignup:(id)sender;
- (IBAction)signInWithTwitter:(id)sender;
- (IBAction)updateViewForEditing:(UITextField *)sender;
- (IBAction)updateViewForFinishedEditing:(UITextField *)sender;
- (IBAction)validateTextFieldInput:(UITextField *)sender;
- (IBAction)animateForgotPassword:(id)sender;
- (IBAction)sendPasswordResetEmail:(id)sender;
- (IBAction)cancelForgotPassword:(id)sender;

@end
