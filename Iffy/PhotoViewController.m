//
//  PhotoViewController.m
//  Iffy
//
//  Created by Joseph Ayo-Vaughan on 1/26/14.
//  Copyright (c) 2014 Joseph Ayo-Vaughan. All rights reserved.
//

#import "PhotoViewController.h"
#import "JVHelperTools.h"
#import <Parse/Parse.h>
#import <QuartzCore/QuartzCore.h>

@interface PhotoViewController () {
	JVHelperTools *jvht;
	UIImage *ogImage;
}

@end

@implementation PhotoViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	
	jvht = [JVHelperTools new];
	
	self.navigationController.view.backgroundColor = [UIColor blackColor];
	[[UIApplication sharedApplication]setStatusBarStyle:UIStatusBarStyleLightContent];
	
//	[UIView animateWithDuration:0.2 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
//		self.tabBarController.tabBar.frame = CGRectOffset(self.tabBarController.tabBar.frame, 0, 49.0);
//		self.navigationController.navigationBar.alpha = 0;
//	} completion:^(BOOL finished) {
//		NSLog(@"should've animated down now");
//	}];
	
	self.scrollView.minimumZoomScale=1;
    self.scrollView.maximumZoomScale=3.0;
//    self.scrollView.contentSize=CGSizeMake(1280, 960);
	self.scrollView.contentSize=CGSizeMake(250, 250);
    self.scrollView.delegate=self;
	
//	self.dottedLineView.layer.cornerRadius = self.dottedLineView.frame.size.height/2;
//	self.imageViewContainerView.layer.cornerRadius = self.imageViewContainerView.frame.size.height/2;
//	self.scrollView.layer.cornerRadius = self.scrollView.frame.size.height/2;
	
	NSString *docDir = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)[0];
	NSString *ogImagePath = [docDir stringByAppendingPathComponent:@"user_og.jpg"];
	
	if ([[NSFileManager defaultManager]fileExistsAtPath:ogImagePath]) {
		self.imageView.image = [UIImage imageWithContentsOfFile:ogImagePath];
	} else if([PFUser currentUser][@"ogPhoto"] != nil) {
		PFFile *imageFile = [PFUser currentUser][@"ogPhoto"];
		
		[imageFile getDataInBackgroundWithBlock:^(NSData *data, NSError *error) {
			if (!error) {
				self.imageView.image = [UIImage imageWithData:data];
			} else {
				self.imageView.image = [UIImage imageNamed:@"default_photo.png"];
			}
		}];
	} else {
		self.imageView.image = [UIImage imageNamed:@"default_photo.png"];
	}
	
	self.photoWrapView.layer.borderWidth = 1.0;
	self.photoWrapView.layer.borderColor = [UIColor colorWithWhite:1.0f alpha:0.75f].CGColor;
	
	if (is5thGen) {
		for (UIView *view in self.view.subviews) {
			view.frame = CGRectOffset(view.frame, 0, 36.0);
		}
	} else {
		for (UIView *view in self.view.subviews) {
			view.frame = CGRectOffset(view.frame, 0, -15.0);
		}
		self.uploadButton.frame = CGRectOffset(self.uploadButton.frame, 0, -5);
		self.finishedButton.frame = CGRectOffset(self.finishedButton.frame, 0, -5);
		self.cancelButton.frame = CGRectOffset(self.cancelButton.frame, 20, 20);
	}
	
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated {
	[super viewWillAppear:animated];
	[[UIApplication sharedApplication]setStatusBarStyle:UIStatusBarStyleLightContent];
}

- (void)viewWillDisappear:(BOOL)animated {
	[super viewWillDisappear:animated];
	
//	self.navigationController.view.backgroundColor = [UIColor whiteColor];
//	
//	[UIView animateWithDuration:0.2 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
//		self.tabBarController.tabBar.frame = CGRectOffset(self.tabBarController.tabBar.frame, 0, -49.0);
//		self.navigationController.navigationBar.alpha = 1.0;
//		[[UIApplication sharedApplication]setStatusBarHidden:NO];
//	} completion:^(BOOL finished) {
//		NSLog(@"should've animated UP now");
//	}];
}

- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView
{
    return self.imageView;
}

- (IBAction)saveImage:(UIButton *)sender {
//	UIImageWriteToSavedPhotosAlbum([jvht imageFromView:self.imageViewContainerView], nil, nil, nil);
	UIImage *pic = [jvht imageFromView:self.imageViewContainerView];
	
	NSData *imageData = UIImageJPEGRepresentation(pic, 1.0);
	NSData *ogImageData = UIImageJPEGRepresentation(self.imageView.image, 1.0);
	
	NSString *docDir = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)[0];
	NSString *smallImagePath = [docDir stringByAppendingPathComponent:@"user.jpg"];
	NSString *ogImagePath = [docDir stringByAppendingPathComponent:@"user_og.jpg"];
	
	if ([[NSFileManager defaultManager]fileExistsAtPath:docDir]) {
		NSLog(@"exists!");
		[imageData writeToFile:smallImagePath atomically:YES];
		[ogImageData writeToFile:ogImagePath atomically:YES];
	}
	
	PFFile *imageFile = [PFFile fileWithName:@"user.jpg" data:imageData];
	PFFile *ogImageFile = [PFFile fileWithName:@"ogphoto.jpg" data:ogImageData];
	
	[imageFile saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
		if (!error) {
			PFUser *user = [PFUser currentUser];
			user[@"photo_small"] = imageFile;
			[user saveInBackground];
			NSLog(@"saved user small_image");
		}
	}];
	
	[ogImageFile saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
		if (!error) {
			PFUser *user = [PFUser currentUser];
			user[@"ogPhoto"] = ogImageFile;
			[user saveEventually];
			NSLog(@"saved user ogimage");
		}
	}];
	
//	[self.navigationController popToRootViewControllerAnimated:YES];
	[self dismissViewControllerAnimated:YES completion:nil];
	
	NSLog(@"YEAAAAHHHH!!!...check");
}

- (IBAction)goBack:(UIButton *)sender {
//	[self.navigationController popToRootViewControllerAnimated:YES];
	[self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)changePhoto:(UIButton *)sender {
	UIActionSheet *as = [[UIActionSheet alloc] initWithTitle:@"Change your profile photo" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Take a photo", @"Choose from library", @"Use photo from Twitter", nil];
	as.accessibilityIdentifier = @"changePhoto";
	as.actionSheetStyle = UIActionSheetStyleBlackOpaque;
	[as showInView:self.view];
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
	if ([actionSheet.accessibilityIdentifier isEqualToString:@"changePhoto"]) {
		if (buttonIndex == 0) {
			UIImagePickerController *imp = [UIImagePickerController new];
			if ([UIImagePickerController isCameraDeviceAvailable:UIImagePickerControllerCameraDeviceRear]) {
				imp.sourceType = UIImagePickerControllerSourceTypeCamera;
				imp.allowsEditing = YES;
				imp.delegate = self;
				[self presentViewController:imp animated:YES completion:nil];
			} else {
				UIImagePickerController *imp = [UIImagePickerController new];
				imp.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
				imp.allowsEditing = YES;
				imp.delegate = self;
				[self presentViewController:imp animated:YES completion:nil];
			}
		} else if(buttonIndex == 1) {
			UIImagePickerController *imp = [UIImagePickerController new];
			imp.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
			imp.allowsEditing = YES;
			imp.delegate = self;
			[self presentViewController:imp animated:YES completion:nil];
		} else if(buttonIndex == 2) {
			// TODO: Get image from twitter account
			// if no twitter account,
			// prompt for log in and store account
		}
	}
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
	[self dismissViewControllerAnimated:YES completion:nil];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
	[self dismissViewControllerAnimated:YES completion:nil];
	[[UIApplication sharedApplication]setStatusBarStyle:UIStatusBarStyleLightContent];
	self.imageView.image = info[UIImagePickerControllerEditedImage];
	ogImage = info[UIImagePickerControllerOriginalImage];
}
@end
