//
//  JVHelperTools.h
//  Iffy
//
//  Created by Joseph Ayo-Vaughan on 1/11/14.
//  Copyright (c) 2014 Joseph Ayo-Vaughan. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UIERealTimeBlurView.h"
#import <Reachability.h>

extern UIColor *redButtonNormal;

@class JVHelperTools;

@protocol JVHelperToolsDelegate

- (void)animateSignUpFromAlert;
- (void)animateLoginFromAlert;
- (void)animateForgotPasswordFromAlert;

@end

@interface JVHelperTools : NSObject <UIAlertViewDelegate>

@property (weak, nonatomic) id<JVHelperToolsDelegate>delegate;
//@property (weak, nonatomic) UIERealTimeBlurView *blurView;
//@property (weak, nonatomic) UIView *transView;
//@property (weak, nonatomic) UIImageView *loadingImagesView;


-(BOOL) textIsValidEmail:(NSString *)checkString;
-(void) addProgressviewToSuperview: (UIView *)superView;
-(void) removeProgressViewFromView: (UIView *)superView;
-(UIImage *)imageFromColor:(UIColor *)color;
-(UIColor *)getRedButtonNormalColor;
-(UIColor *)getOrangeButtonNormalColor;
-(UIColor *)getRedButtonHighlightedColor;
-(UIColor *)getOrangeButtonHighlightedColor;
-(UIColor *)getButtonHighlightedTextColor;
-(BOOL) textFieldInputIsWhiteSpace: (NSString *)text;
-(BOOL) internetAccessIsAvailable;

enum UIAlertViewReason {
	JVHTNoInternetConnection,
	JVHTEmailTaken,
	JVHTEmailNotFound,
	JVHTInvalidLoginCredentials,
	JVHTPasswordResetEmailSent,
	JVHTEmailVerificationSent,
	JVHTGeneralError
};

-(void)showAlertViewForReason:(enum UIAlertViewReason) reason;
- (UIImage *)imageFromView: (UIView *)view;

@end
