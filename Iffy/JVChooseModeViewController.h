//
//  JVChooseModeViewController.h
//  Iffy
//
//  Created by Joseph Ayo-Vaughan on 2/9/14.
//  Copyright (c) 2014 Joseph Ayo-Vaughan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface JVChooseModeViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIButton *randomModeButton;
@property (weak, nonatomic) IBOutlet UIButton *speedyModeButton;
@property (weak, nonatomic) IBOutlet UIButton *exposeModeButton;
@property (weak, nonatomic) IBOutlet UIButton *openLinesModeButton;
@property (weak, nonatomic) IBOutlet UIButton *byRegionModeButton;
@property (weak, nonatomic) IBOutlet UIButton *byTopicModeButton;

@end
