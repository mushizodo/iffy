//
//  OutGoingCallViewController.m
//  Iffy
//
//  Created by Joseph Ayo-Vaughan on 2/3/14.
//  Copyright (c) 2014 Joseph Ayo-Vaughan. All rights reserved.
//

#import "OutgoingCallViewController.h"
#import <Parse/Parse.h>

@interface OutgoingCallViewController ()

@end

@implementation OutgoingCallViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
	PFQuery *query = [PFUser query];
	[query whereKey:@"objectId" equalTo:[self.call remoteUserId]];
	[query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
		if (!error) {
			PFUser *user = [objects objectAtIndex:0];
			if ([user[@"anonymous"]boolValue] == YES) {
				self.userCallingName.text = @"Iffy Caller";
			} else {
				self.userCallingName.text =  user[@"name"];
			}
		}
	}];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)hangUp:(UIButton *)sender {
	[self.call hangup];
	if (![self isBeingDismissed]) {
		[self dismissViewControllerAnimated:YES completion:nil];
	}
}

- (void)callEnded:(id<SINCall>)call {
	NSLog(@"call ended");
	if (![self isBeingDismissed]) {
		[self dismissViewControllerAnimated:YES completion:nil];
	}
}

@end
