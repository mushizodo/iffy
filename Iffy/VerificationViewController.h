//
//  StartupViewController.h
//  Iffy
//
//  Created by Joseph Ayo-Vaughan on 1/19/14.
//  Copyright (c) 2014 Joseph Ayo-Vaughan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VerificationViewController : UIViewController <UITextFieldDelegate, UIAlertViewDelegate>
@property (weak, nonatomic) IBOutlet UIImageView *facesImageView;
@property (weak, nonatomic) IBOutlet UIImageView *phoneImageView;
@property (weak, nonatomic) IBOutlet UIImageView *browsImageView;
@property (weak, nonatomic) IBOutlet UILabel *welcomeLabel;
@property (weak, nonatomic) IBOutlet UITextField *determinedLabel;
@property (weak, nonatomic) IBOutlet UIButton *resendEmailButton;
@property (weak, nonatomic) IBOutlet UIButton *loginButton;
@property (weak, nonatomic) IBOutlet UIButton *checkEmalButton;
@property (weak, nonatomic) IBOutlet UIButton *changeEmailButton;
@property (weak, nonatomic) IBOutlet UIButton *didNotGetEmailButton;

@property (strong, nonatomic) NSString *ogEmail;

- (IBAction)tryConnectingAgain:(UIButton *)sender;
- (IBAction)resendVerificationEmail:(UIButton *)sender;
- (IBAction)goToDidNotGetEmailView:(UIButton *)sender;
- (void)checkForUser;
- (IBAction)checkEmailVerification:(UIButton *)sender;
- (BOOL)recheckEmailVerified: (NSTimer *)timer;
- (IBAction)changeEmail:(UIButton *)sender;
- (IBAction)finishedChangeEmail:(UIButton *)sender;
- (IBAction)validateEmail:(UITextField *)sender;
- (IBAction)updateViewForEditing:(UITextField *)sender;
- (IBAction)updateViewForFinishedEditing:(UITextField *)sender;

@end
