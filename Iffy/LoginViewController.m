//
//  ViewController.m
//  Iffy
//
//  Created by Joseph Ayo-Vaughan on 1/5/14.
//  Copyright (c) 2014 Joseph Ayo-Vaughan. All rights reserved.
//

#import "LoginViewController.h"
#import "FinishTwitterViewController.h"
#import <Parse/Parse.h>
#import <Reachability.h>
#import "LoginSceneUnwindSegue.h"

@interface LoginViewController () {
	JVHelperTools *jvht;
	UILabel *validityLabel;
}

@end

@implementation LoginViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	
	jvht = [JVHelperTools new];
	jvht.delegate = self;
	
	UIColor *c_c6c6c6 = [jvht getButtonHighlightedTextColor];
	
	self.signInTwitter.backgroundColor = [UIColor colorWithRed:85.0/255.0 green:172.0/255.0 blue:238.0/255.0 alpha:1];
	UIImage *signupTwitterPressedImage = [[jvht imageFromColor:[UIColor colorWithRed:78.0/255.0 green:145.0/255.0 blue:196.0/255.0 alpha:1.0]] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 0, 0, 0)];
	[self.signInTwitter setBackgroundImage:signupTwitterPressedImage forState:UIControlStateHighlighted];
	[self.signInTwitter setTitleColor:c_c6c6c6 forState:UIControlStateHighlighted];
	
	self.signUpButton.backgroundColor = [jvht getOrangeButtonNormalColor];
	UIImage *signupPressedImage = [[jvht imageFromColor:[jvht getOrangeButtonHighlightedColor]] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 0, 0, 0)];
	[self.signUpButton setBackgroundImage:signupPressedImage forState:UIControlStateHighlighted];
	[self.signUpButton setTitleColor:c_c6c6c6 forState:UIControlStateHighlighted];
	
	self.loginButton.backgroundColor = [jvht getRedButtonNormalColor];
	UIImage *loginPressedImage = [[jvht imageFromColor:[jvht getRedButtonHighlightedColor]] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 0, 0, 0)];
	[self.loginButton setBackgroundImage:loginPressedImage forState:UIControlStateHighlighted];
	[self.loginButton setTitleColor:c_c6c6c6 forState:UIControlStateHighlighted];
	
	[self.forgotPasswordButton setTitleColor:c_c6c6c6 forState:UIControlStateHighlighted];
	
	[self.loginButton setTitleColor:[jvht getRedButtonHighlightedColor] forState:UIControlStateDisabled];
	[self.signUpButton setTitleColor:[jvht getOrangeButtonHighlightedColor] forState:UIControlStateDisabled];
	
	if (!is5thGen) {
		self.nameField.returnKeyType = UIReturnKeyDefault;
		self.emailField.returnKeyType = UIReturnKeyDefault;
		self.passwordField.returnKeyType = UIReturnKeyDefault;
	}
	
	if (is5thGen) {
		self.infoButton.frame = CGRectOffset(self.infoButton.frame, 0, 85);
	}
	
}

- (void)viewDidAppear:(BOOL)animated {
	[super viewDidAppear:animated];
	if (self.view.frame.origin.y == 0) {
		[UIView animateWithDuration:1.0 delay:1.0 options:UIViewAnimationOptionCurveEaseOut animations:^{
			CGRect faceLogoRectOG = self.faceLogo.frame;
			CGRect faceLogoNewRect;
			if (is5thGen) {
				faceLogoNewRect = CGRectMake(faceLogoRectOG.origin.x, 221.0, faceLogoRectOG.size.width, faceLogoRectOG.size.height);
			} else {
				faceLogoNewRect = CGRectMake(faceLogoRectOG.origin.x, 140.0, faceLogoRectOG.size.width, faceLogoRectOG.size.height);
			}
			self.faceLogo.frame = faceLogoNewRect;
		} completion:^(BOOL finished) {
			// code
		}];
	}
	
}

- (IBAction)loginUser:(id)sender {
	for (UITextField * tf in self.view.subviews) {
		[tf resignFirstResponder];
		[tf setEnabled:NO];
	}
	
	for (UIButton *bt in self.view.subviews) {
		[bt setEnabled:NO];
	}
	
	[self resignKeyboard:self.resignerButton];
	[self updateViewForFinishedEditing:self.emailField];
	
	[jvht addProgressviewToSuperview:self.view.superview];
	
	if ([jvht internetAccessIsAvailable]) {
		if ([jvht textIsValidEmail:self.emailField.text] && [self.passwordField.text length] != 0 && [self.passwordField.text length] >= 6) {
			[PFUser logInWithUsernameInBackground:self.emailField.text password:self.passwordField.text block:^(PFUser *user, NSError *error) {
				if (user) {
					NSLog(@"We're logged in!");
					[jvht removeProgressViewFromView:self.view.superview];
					
					if ([user[@"emailVerified"]boolValue] == YES) {
						NSLog(@"TO THE HOME PAGE!!!");
					} else {
						[self performSegueWithIdentifier:@"toVerification" sender:self];
					}
					
//					for (UIButton *bt in self.view.subviews) {
//						[bt setEnabled:YES];
//					}
				} else {
					NSLog(@"ERROR LOGGING IN WITH ERROR: %@", error);
					[jvht removeProgressViewFromView:self.view.superview];
					for (UIButton *bt in self.view.subviews) {
						[bt setEnabled:YES];
					}
					
					if (error.code == kParseErrorObjectNotFound) {
						[jvht showAlertViewForReason:JVHTInvalidLoginCredentials];
					}
				}
			}];
		} else {
			[jvht removeProgressViewFromView:self.view.superview];
			
			for (UIButton *bt in self.view.subviews) {
				[bt setEnabled:YES];
			}
			
			self.emailField.enabled = YES;
			self.passwordField.enabled = YES;
			
			if (![jvht textIsValidEmail:self.emailField.text]) {
				self.emailField.layer.borderWidth = 2.0;
				self.emailField.layer.borderColor = [jvht getOrangeButtonHighlightedColor].CGColor;
				NSLog(@"email blank");
			} else {
				self.emailField.layer.borderWidth = 0.0;
			}
			
			if ([self.passwordField.text length] == 0 || [self.passwordField.text length] < 6) {
				self.passwordField.layer.borderWidth = 2.0;
				self.passwordField.layer.borderColor = [jvht getOrangeButtonHighlightedColor].CGColor;
			} else {
				self.passwordField.layer.borderWidth = 0.0;
			}
		}
	} else {
		[jvht removeProgressViewFromView:self.view.superview];
		
		for (UIButton *bt in self.view.subviews) {
			[bt setEnabled:YES];
		}
		[jvht showAlertViewForReason:JVHTNoInternetConnection];
	}
	
}

- (IBAction)animateForLogin:(id)sender {
	NSLog(@"%f", self.loginButton.frame.size.width);
	for (UITextField *tf in self.view.subviews) {
		tf.layer.borderWidth = 0.0;
	}
	
	if (self.loginButton.frame.origin.y == 150) {
		self.mainBgImage.image = [UIImage imageNamed:@"red_login_bg.jpg"];
		
		self.signUpButton.enabled = NO;
		self.loginButton.enabled = NO;
		self.signInTwitter.enabled = NO;
		
		
		[self.signUpButton removeTarget:self action:@selector(signupUser:) forControlEvents:UIControlEventTouchUpInside];
		
		[UIView animateWithDuration:0.6 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
			
			self.iffyLogo.alpha = 0.3;
			self.signInTwitter.alpha = 0.3;
			self.signUpButton.alpha = 0.3;
			self.twitterBird.alpha = 0.3;
			self.loginButton.alpha = 0.3;
			self.forgotPasswordButton.enabled = YES;
			self.forgotPasswordButton.alpha = 1.0;
			
			if (!is5thGen) {
				self.infoButton.frame = CGRectMake(270, 35, 15, 15);
			}
			
			self.iffyLogo.frame = CGRectMake(self.iffyLogo.frame.origin.x, 80.0, self.iffyLogo.frame.size.width, self.iffyLogo.frame.size.height);
			
			self.signInTwitter.frame = CGRectMake(self.signInTwitter.frame.origin.x, 430, self.signInTwitter.frame.size.width, self.signInTwitter.frame.size.height);
			
			self.twitterBird.frame = CGRectMake(self.twitterBird.frame.origin.x, 447, self.twitterBird.frame.size.width, self.twitterBird.frame.size.height);
			
			self.signUpButton.frame = CGRectMake(self.signUpButton.frame.origin.x, 430, self.signUpButton.frame.size.width, self.signUpButton.frame.size.height);
			
			self.loginButton.frame = CGRectMake(self.loginButton.frame.origin.x, 295, self.loginButton.frame.size.width, self.loginButton.frame.size.height);
			
			self.nameField.layer.frame = CGRectMake(self.nameField.frame.origin.x, 500, self.nameField.frame.size.width, self.nameField.frame.size.height);
			self.nameField.alpha = 0;
			self.nameField.enabled = NO;
			
			self.emailField.alpha = 1.0;
			self.emailField.enabled = YES;
			
			self.passwordField.alpha = 1.0;
			self.passwordField.enabled = YES;
			
			self.forgotPasswordButton.alpha = 1.0;
			self.forgotPasswordButton.enabled = YES;
			
			self.iffyLogo.alpha = 1.0;
			self.signInTwitter.alpha = 1.0;
			self.signUpButton.alpha = 1.0;
			self.twitterBird.alpha = 1.0;
			self.loginButton.alpha = 1.0;
		} completion:^(BOOL finished) {
			if (finished) {
				[(UIButton *)sender addTarget:self action:@selector(loginUser:) forControlEvents:UIControlEventTouchUpInside];
				[self.signUpButton addTarget:self action:@selector(animateForSignup:) forControlEvents:UIControlEventTouchUpInside];
				
				self.signUpButton.enabled = YES;
				self.loginButton.enabled = YES;
				self.signInTwitter.enabled = YES;
			}
		}];
	} else if(self.loginButton.frame.origin.y == 430) {
		self.mainBgImage.image = [UIImage imageNamed:@"red_login_bg.jpg"];
		self.signUpButton.enabled = NO;
		self.loginButton.enabled = NO;
		self.forgotPasswordButton.enabled = YES;
		self.forgotPasswordButton.alpha = 1.0;
		
		[self.signUpButton removeTarget:self action:@selector(signupUser:) forControlEvents:UIControlEventTouchUpInside];
		
		[UIView animateWithDuration:0.25 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
			
			self.iffyLogo.frame = CGRectMake(self.iffyLogo.frame.origin.x, 80.0, self.iffyLogo.frame.size.width, self.iffyLogo.frame.size.height);
			
			self.nameField.layer.frame = CGRectMake(self.nameField.frame.origin.x, 500, self.nameField.frame.size.width, self.nameField.frame.size.height);
			self.nameField.alpha = 0;
			self.nameField.enabled = NO;
			
			CGRect newLoginFrame = CGRectMake(0, 295, 320, self.signUpButton.frame.size.height);
			self.loginButton.layer.frame = newLoginFrame;
			
			CGRect newSignupFrame = CGRectMake(160, 430, 160, self.loginButton.frame.size.height);
			self.signUpButton.layer.frame = newSignupFrame;
			
		} completion:^(BOOL finished) {
			[(UIButton *)sender addTarget:self action:@selector(loginUser:) forControlEvents:UIControlEventTouchUpInside];
			[self.signUpButton addTarget:self action:@selector(animateForSignup:) forControlEvents:UIControlEventTouchUpInside];
			
			self.signUpButton.enabled = YES;
			self.loginButton.enabled = YES;
		}];
	}
}

- (void)animateLoginFromAlert {
	[self animateForLogin:self.loginButton];
}

- (IBAction)signupUser:(id)sender {
	
	for (UITextField * tf in self.view.subviews) {
		[tf resignFirstResponder];
		[tf setEnabled:NO];
	}
	
	for (UIButton *bt in self.view.subviews) {
		[bt setEnabled:NO];
	}
	
	[self resignKeyboard:self.resignerButton];
	[self updateViewForFinishedEditing:self.emailField];
	
	[jvht addProgressviewToSuperview:self.view.superview];
	
	if ([jvht internetAccessIsAvailable]) {
		
		NSString *emailRaw = [self.emailField text];
		
		if (![jvht textFieldInputIsWhiteSpace:self.nameField.text] && ![jvht textFieldInputIsWhiteSpace:self.passwordField.text] && [jvht textIsValidEmail:emailRaw] && [self.passwordField.text length] >= 6) {
			PFUser *user = [PFUser user];
			user.username = self.emailField.text;
			user.password = self.passwordField.text;
			user.email = self.emailField.text;
			user[@"name"] = self.nameField.text;
			
			[user signUpInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
				if (!error) {
					[jvht removeProgressViewFromView:self.view.superview];
					
					// JUST FOR TESTING. REMOVE FOR PRODUCTION
					// SHOULD GO TO CHECK EMAIL VERIFIED PAGE
					NSLog(@"SIGNED UP! SHOULD GO TO CHECK EMAIL VERIFIED PAGE");
					[self performSegueWithIdentifier:@"toVerification" sender:self];
					
//					for (UITextField *tf in self.view.subviews) {
//						[tf setEnabled:YES];
//					}
//					
//					for (UIButton *bt in self.view.subviews) {
//						[bt setEnabled:YES];
//					}
					
				} else {
					[jvht removeProgressViewFromView:self.view.superview];
					
					for (UITextField *tf in self.view.subviews) {
						[tf setEnabled:YES];
					}
					
					for (UIButton *bt in self.view.subviews) {
						[bt setEnabled:YES];
					}
					
					if (error.code == kParseErrorUsernameTaken || error.code == kParseErrorEmailTaken) { // username/email taken
						[jvht showAlertViewForReason:JVHTEmailTaken];
					}
					NSLog(@"UNSUCCESSFUL SIGN IN WITH ERROR: %@", error);
				}
			}];
		} else {
			[jvht removeProgressViewFromView:self.view.superview];
			for (UITextField * tf in self.view.subviews) {
				[tf setEnabled:YES];
			}
			
			for (UIButton *bt in self.view.subviews) {
				[bt setEnabled:YES];
			}
			
			if ([jvht textFieldInputIsWhiteSpace:self.nameField.text]) {
				self.nameField.layer.borderWidth = 2.0;
				self.nameField.layer.borderColor = [jvht getRedButtonNormalColor].CGColor;
			} else {
				self.nameField.layer.borderWidth = 0.0;
			}
			
			if (![jvht textIsValidEmail:self.emailField.text]) {
				self.emailField.layer.borderWidth = 2.0;
				self.emailField.layer.borderColor = [jvht getRedButtonNormalColor].CGColor;
			} else {
				self.emailField.layer.borderWidth = 0.0;
			}
			
			if ([self.passwordField.text length] == 0 || [self.passwordField.text length] < 6) {
				self.passwordField.layer.borderWidth = 2.0;
				self.passwordField.layer.borderColor = [jvht getRedButtonNormalColor].CGColor;
			} else {
				self.passwordField.layer.borderWidth = 0.0;
			}
		}
	} else { // NO INTERNET CONNECTION ESTABLISHED
		[jvht removeProgressViewFromView:self.view.superview];
		
		for (UITextField *tf in self.view.subviews) {
			[tf setEnabled:YES];
		}
		
		for (UIButton *bt in self.view.subviews) {
			[bt setEnabled:YES];
		}
		[jvht showAlertViewForReason:JVHTNoInternetConnection];
	}
	
}

- (IBAction)animateForSignup:(id)sender {
	for (UITextField *tf in self.view.subviews) {
		tf.layer.borderWidth = 0.0;
	}
	
	if (self.signUpButton.frame.origin.y == 100) {
		self.mainBgImage.image = [UIImage imageNamed:@"orange_login_bg.jpg"];
		self.signUpButton.enabled = NO;
		self.loginButton.enabled = NO;
		self.signInTwitter.enabled = NO;
		
		[self.loginButton removeTarget:self action:@selector(loginUser:) forControlEvents:UIControlEventTouchUpInside];
		
		[UIView animateWithDuration:0.6 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
			
			self.iffyLogo.alpha = 0.3;
			self.signInTwitter.alpha = 0.3;
			self.signUpButton.alpha = 0.3;
			self.twitterBird.alpha = 0.3;
			self.loginButton.alpha = 0.3;
			self.forgotPasswordButton.enabled = NO;
			self.forgotPasswordButton.alpha = 0;
			
			if (!is5thGen) {
				self.infoButton.frame = CGRectMake(270, 35, 15, 15);
			}
			
			self.iffyLogo.frame = CGRectMake(self.iffyLogo.frame.origin.x, 20.0, self.iffyLogo.frame.size.width, self.iffyLogo.frame.size.height);
			
			self.signInTwitter.frame = CGRectMake(self.signInTwitter.frame.origin.x, 430, self.signInTwitter.frame.size.width, self.signInTwitter.frame.size.height);
			
			self.twitterBird.frame = CGRectMake(self.twitterBird.frame.origin.x, 447, self.twitterBird.frame.size.width, self.twitterBird.frame.size.height);
			
			CGRect newSignupFrame = CGRectMake(0, 295, 320, self.signUpButton.frame.size.height);
			self.signUpButton.layer.frame = newSignupFrame;
			
			CGRect newLoginFrame = CGRectMake(160, 430, 160, self.loginButton.frame.size.height);
			self.loginButton.layer.frame = newLoginFrame;
			
			self.nameField.layer.frame = CGRectMake(self.nameField.frame.origin.x, 123, self.nameField.frame.size.width, self.nameField.frame.size.height);
			self.nameField.alpha = 1.0;
			self.nameField.enabled = YES;
			
			self.emailField.alpha = 1.0;
			self.emailField.enabled = YES;
			
			self.passwordField.alpha = 1.0;
			self.passwordField.enabled = YES;
			
			self.iffyLogo.alpha = 1.0;
			self.signInTwitter.alpha = 1.0;
			self.signUpButton.alpha = 1.0;
			self.twitterBird.alpha = 1.0;
			self.loginButton.alpha = 1.0;
		} completion:^(BOOL finished) {
			[(UIButton *)sender addTarget:self action:@selector(signupUser:) forControlEvents:UIControlEventTouchUpInside];
			
			[self.loginButton addTarget:self action:@selector(animateForLogin:) forControlEvents:UIControlEventTouchUpInside];
			
			self.signUpButton.enabled = YES;
			self.loginButton.enabled = YES;
			self.signInTwitter.enabled = YES;
		}];
	} else if(self.signUpButton.frame.origin.y == 430) {
		
		if ([self.forgotPasswordButton.titleLabel.text isEqualToString:@"Cancel"]) {
			[self.forgotPasswordButton setTitle:@"Forgot Password?" forState:UIControlStateNormal];
			[self.forgotPasswordButton removeTarget:self action:@selector(cancelForgotPassword:) forControlEvents:UIControlEventTouchUpInside];
			[self.forgotPasswordButton addTarget:self action:@selector(animateForgotPassword:) forControlEvents:UIControlEventTouchUpInside];
			
			[self.loginButton setTitle:@"Login" forState:UIControlStateNormal];
			[self.loginButton removeTarget:self action:@selector(sendPasswordResetEmail:) forControlEvents:UIControlEventTouchUpInside];
			[self.loginButton addTarget:self action:@selector(animateForLogin:) forControlEvents:UIControlEventTouchUpInside];
			[UIView animateWithDuration:0.25 delay:0 options:UIViewAnimationOptionCurveLinear animations:^{
				self.passwordField.alpha = 1.0;
				self.passwordField.enabled = YES;
			} completion:^(BOOL finished) {
				// code
			}];
		}
		
		self.mainBgImage.image = [UIImage imageNamed:@"orange_login_bg.jpg"];
		self.signUpButton.enabled = NO;
		self.loginButton.enabled = NO;
		self.forgotPasswordButton.enabled = NO;
		self.forgotPasswordButton.alpha = 0;
		
		[self.loginButton removeTarget:self action:@selector(loginUser:) forControlEvents:UIControlEventTouchUpInside];
		
		[UIView animateWithDuration:0.25 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
			self.iffyLogo.frame = CGRectMake(self.iffyLogo.frame.origin.x, 20.0, self.iffyLogo.frame.size.width, self.iffyLogo.frame.size.height);
			
			self.nameField.layer.frame = CGRectMake(self.nameField.frame.origin.x, 123, self.nameField.frame.size.width, self.nameField.frame.size.height);
			self.nameField.alpha = 1.0;
			self.nameField.enabled = YES;
			
			CGRect newSignupFrame = CGRectMake(0, 295, 320, self.signUpButton.frame.size.height);
			self.signUpButton.layer.frame = newSignupFrame;
			
			CGRect newLoginFrame = CGRectMake(160, 430, 160, self.loginButton.frame.size.height);
			self.loginButton.layer.frame = newLoginFrame;
			
		} completion:^(BOOL finished) {
			[(UIButton *)sender addTarget:self action:@selector(signupUser:) forControlEvents:UIControlEventTouchUpInside];
			[self.loginButton addTarget:self action:@selector(animateForLogin:) forControlEvents:UIControlEventTouchUpInside];
			
			self.signUpButton.enabled = YES;
			self.loginButton.enabled = YES;
		}];
	}
}

- (void)animateSignUpFromAlert {
	[self animateForSignup:self.signUpButton];
}

- (IBAction)signInWithTwitter:(id)sender {
	self.signUpButton.enabled = NO;
	self.signInTwitter.enabled = NO;
	self.loginButton.enabled = NO;
	self.forgotPasswordButton.enabled = NO;
	self.infoButton.enabled = NO;
	
	[jvht addProgressviewToSuperview:self.view.superview];
	
	if (![jvht internetAccessIsAvailable]) {
		
		[jvht showAlertViewForReason:JVHTNoInternetConnection];
		
		[jvht removeProgressViewFromView:self.view.superview];
		
		self.signUpButton.enabled = YES;
		self.signInTwitter.enabled = YES;
		self.loginButton.enabled = YES;
		self.forgotPasswordButton.enabled = YES;
		self.infoButton.enabled = YES;
		
	} else {
		[PFTwitterUtils logInWithBlock:^(PFUser *user, NSError *error) {
			if (!user) {
				NSLog(@"Uh oh. The user cancelled the Twitter login.");
				
				[jvht removeProgressViewFromView:self.view.superview];
				
				self.signUpButton.enabled = YES;
				self.signInTwitter.enabled = YES;
				self.loginButton.enabled = YES;
				self.forgotPasswordButton.enabled = YES;
				self.infoButton.enabled = YES;
				
				return;
			} else if (user.isNew) {
				
				NSLog(@"User signed up and logged in with Twitter!");
				NSURL *verify = [NSURL URLWithString:@"https://api.twitter.com/1.1/account/verify_credentials.json"];
				NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:verify];
				[[PFTwitterUtils twitter] signRequest:request];
				NSURLResponse *response = nil;
				NSData *data = [NSURLConnection sendSynchronousRequest:request
													 returningResponse:&response
																 error:&error];
				NSDictionary *userInfo = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
				
				if (!(userInfo[@"name"] == nil)) {
					self.userName = userInfo[@"name"];
				}
				
				[jvht removeProgressViewFromView:self.view.superview];
				
				[self performSegueWithIdentifier:@"toGetEmail" sender:self];
			} else {
				NSLog(@"User logged in with Twitter!");
				NSLog(@"%@", user.email);
				
				NSURL *verify = [NSURL URLWithString:@"https://api.twitter.com/1.1/account/verify_credentials.json"];
				NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:verify];
				[[PFTwitterUtils twitter] signRequest:request];
				NSURLResponse *response = nil;
				NSData *data = [NSURLConnection sendSynchronousRequest:request
													 returningResponse:&response
																 error:&error];
				NSDictionary *userInfo = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
				
				if (!(userInfo[@"name"] == nil)) {
					self.userName = userInfo[@"name"];
				}
				
				if (user.email == nil) {
					[jvht removeProgressViewFromView:self.view.superview];
					[self performSegueWithIdentifier:@"toGetEmail" sender:self];
				} else {
					[jvht removeProgressViewFromView:self.view.superview];
					[PFUser currentUser].username = user.username;
					if ([user[@"emailVerified"]boolValue] == YES) {
						NSLog(@"VERIFIED! TO THE HOME PAGE! nananananana...");
					} else {
						[self performSegueWithIdentifier:@"toVerification" sender:self];
					}
				}
			}
		}];
	}
	
}

- (IBAction)sendPasswordResetEmail:(id)sender {
	[self.emailField resignFirstResponder];
	
	self.signUpButton.enabled = NO;
	self.forgotPasswordButton.enabled = NO;
	self.signInTwitter.enabled = NO;
	self.infoButton.enabled = NO;
	
	[jvht addProgressviewToSuperview:self.view.superview];
	NSLog(@"Keep on forgetting...");
	if ([jvht internetAccessIsAvailable]) {
		if ([jvht textIsValidEmail:self.emailField.text]) {
			[PFUser requestPasswordResetForEmailInBackground:self.emailField.text block:^(BOOL succeeded, NSError *error) {
				[self updateViewForFinishedEditing:self.emailField];
				if (!error) {
					[jvht removeProgressViewFromView:self.view.superview];
					self.signUpButton.enabled = YES;
					self.forgotPasswordButton.enabled = YES;
					self.signInTwitter.enabled = YES;
					self.infoButton.enabled = YES;
					[self cancelForgotPassword:self.forgotPasswordButton];
					[jvht showAlertViewForReason:JVHTPasswordResetEmailSent];
				} else if (error.code == kParseErrorEmailNotFound) {
					[jvht removeProgressViewFromView:self.view.superview];
					self.signUpButton.enabled = YES;
					self.forgotPasswordButton.enabled = YES;
					self.signInTwitter.enabled = YES;
					self.infoButton.enabled = YES;
					[jvht showAlertViewForReason:JVHTEmailNotFound];
				} else {
					[jvht removeProgressViewFromView:self.view.superview];
					self.signUpButton.enabled = YES;
					self.forgotPasswordButton.enabled = YES;
					self.signInTwitter.enabled = YES;
					self.infoButton.enabled = YES;
					[jvht showAlertViewForReason:JVHTGeneralError];
				}
			}];
		} else {
			[jvht removeProgressViewFromView:self.view.superview];
			self.signUpButton.enabled = YES;
			self.forgotPasswordButton.enabled = YES;
			self.signInTwitter.enabled = YES;
			self.infoButton.enabled = YES;
			self.emailField.layer.borderColor = [jvht getOrangeButtonNormalColor].CGColor;
			self.emailField.layer.borderWidth = 2.0;
		}
	} else {
		[jvht removeProgressViewFromView:self.view.superview];
		self.signUpButton.enabled = YES;
		self.forgotPasswordButton.enabled = YES;
		self.signInTwitter.enabled = YES;
		self.infoButton.enabled = YES;
		[jvht showAlertViewForReason:JVHTNoInternetConnection];
	}
	
}

- (IBAction)animateForgotPassword:(id)sender {
	for (UITextField *tf in self.view.subviews) {
		tf.layer.borderWidth = 0.0;
	}
	
	NSLog(@"%f", self.loginButton.frame.size.width);
	[sender setTitle:@"Cancel" forState:UIControlStateNormal];
	[sender removeTarget:self action:@selector(animateForgotPassword:) forControlEvents:UIControlEventTouchUpInside];
	[sender addTarget:self action:@selector(cancelForgotPassword:) forControlEvents:UIControlEventTouchUpInside];
	
	CGRect loginRect = self.loginButton.frame;
	[self.loginButton setTitle:@"Send Password Reset Email" forState:UIControlStateNormal];
	self.loginButton.frame = loginRect;
	[self.loginButton removeTarget:self action:@selector(loginUser:) forControlEvents:UIControlEventTouchUpInside];
	[self.loginButton addTarget:self action:@selector(sendPasswordResetEmail:) forControlEvents:UIControlEventTouchUpInside];
	
	[UIView animateWithDuration:0.25 delay:0 options:UIViewAnimationOptionCurveLinear animations:^{
		self.passwordField.alpha = 0.1;
		self.passwordField.enabled = NO;
	} completion:^(BOOL finished) {
		// code
	}];
}

- (IBAction)cancelForgotPassword:(id)sender {
	for (UITextField *tf in self.view.subviews) {
		tf.layer.borderWidth = 0.0;
	}
	
	[sender setTitle:@"Forgot Password?" forState:UIControlStateNormal];
	[sender removeTarget:self action:@selector(cancelForgotPassword:) forControlEvents:UIControlEventTouchUpInside];
	[sender addTarget:self action:@selector(animateForgotPassword:) forControlEvents:UIControlEventTouchUpInside];
	
	CGRect loginRect = self.loginButton.frame;
	[self.loginButton setTitle:@"Login" forState:UIControlStateNormal];
	self.loginButton.frame = loginRect;
	[self.loginButton removeTarget:self action:@selector(sendPasswordResetEmail:) forControlEvents:UIControlEventTouchUpInside];
	[self.loginButton addTarget:self action:@selector(loginUser:) forControlEvents:UIControlEventTouchUpInside];
	[UIView animateWithDuration:0.25 delay:0 options:UIViewAnimationOptionCurveLinear animations:^{
		self.passwordField.alpha = 1.0;
		self.passwordField.enabled = YES;
	} completion:^(BOOL finished) {
		// code
	}];

}

- (void)animateForgotPasswordFromAlert {
	[self animateForgotPassword:self.forgotPasswordButton];
}

- (IBAction)updateViewForEditing:(UITextField *)sender {
	sender.layer.borderWidth = 0.0;
	
	if ((!is5thGen) && self.iffyLogo.frame.origin.y > 0) {
		[UIView animateWithDuration:0.25 delay:0 options:UIViewAnimationOptionCurveLinear animations:^{
			for (UITextField *tf in self.view.subviews) {
				tf.frame = CGRectMake(tf.frame.origin.x, tf.frame.origin.y - 80, tf.frame.size.width, tf.frame.size.height);
			}
		} completion:^(BOOL finished) {
			if (finished) {
			}
		}];
	}
	
	if ([self.view.superview.subviews containsObject:validityLabel]) {
		if (sender == self.nameField) {
			validityLabel.text = @"Enter your name.";
			if ([sender.text length] > 0) {
				validityLabel.backgroundColor = [UIColor greenColor];
				validityLabel.text = @"Name looks good :D";
				validityLabel.textColor = [UIColor colorWithWhite:0.4 alpha:1];
			} else {
				validityLabel.backgroundColor = [UIColor redColor];
				validityLabel.text = @"Please enter your name";
				validityLabel.textColor = [UIColor whiteColor];
			}
		} else if(sender == self.emailField) {
			validityLabel.text = @"Enter your email";
			if ([jvht textIsValidEmail:sender.text]) {
				validityLabel.backgroundColor = [UIColor greenColor];
				validityLabel.text = @"Email looks good :D";
				validityLabel.textColor = [UIColor colorWithWhite:0.4 alpha:1];
			} else {
				validityLabel.backgroundColor = [UIColor redColor];
				validityLabel.text = @"A valid email is required.";
				validityLabel.textColor = [UIColor whiteColor];
			}
		} else if(sender == self.passwordField) {
			if ([sender.text length] < 6) {
				validityLabel.backgroundColor = [UIColor redColor];
				validityLabel.text = @"Password must be at least 6 characters.";
				validityLabel.textColor = [UIColor whiteColor];
			} else {
				validityLabel.backgroundColor = [UIColor greenColor];
				validityLabel.text = @"Password looks good :D";
				validityLabel.textColor = [UIColor colorWithWhite:0.4 alpha:1];
			}
		}
	} else {
		//  if (self.signUpButton.frame.size.width == [UIScreen mainScreen].bounds.size.width)
		CGRect validityFrame = CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 30);
		validityLabel = [[UILabel alloc]initWithFrame:validityFrame];
		[validityLabel setFont:[UIFont fontWithName:@"GillSans-Light" size:15]];
		validityLabel.backgroundColor = [UIColor redColor];
		validityLabel.textColor = [UIColor whiteColor];
		validityLabel.textAlignment = NSTextAlignmentCenter;
		validityLabel.alpha = 0.0;
		if (sender == self.nameField) {
			validityLabel.text = @"Enter your name.";
			if ([sender.text length] > 0) {
				validityLabel.backgroundColor = [UIColor greenColor];
				validityLabel.text = @"Name looks good :D";
				validityLabel.textColor = [UIColor colorWithWhite:0.4 alpha:1];
			} else {
				validityLabel.backgroundColor = [UIColor redColor];
				validityLabel.text = @"Please enter your name";
				validityLabel.textColor = [UIColor whiteColor];
			}
		} else if(sender == self.emailField) {
			validityLabel.text = @"Enter your email";
			if ([jvht textIsValidEmail:sender.text]) {
				validityLabel.backgroundColor = [UIColor greenColor];
				validityLabel.text = @"Email looks good :D";
				validityLabel.textColor = [UIColor colorWithWhite:0.4 alpha:1];
			} else {
				validityLabel.backgroundColor = [UIColor redColor];
				validityLabel.text = @"A valid email is required.";
				validityLabel.textColor = [UIColor whiteColor];
			}
		} else if(sender == self.passwordField) {
			if ([sender.text length] < 6) {
				validityLabel.backgroundColor = [UIColor redColor];
				validityLabel.text = @"Password must be at least 6 characters.";
				validityLabel.textColor = [UIColor whiteColor];
			} else {
				validityLabel.backgroundColor = [UIColor greenColor];
				validityLabel.text = @"Password looks good :D";
				validityLabel.textColor = [UIColor colorWithWhite:0.4 alpha:1];
			}
		}
		
		[self.view.superview addSubview:validityLabel];
		
		[UIView animateWithDuration:0.25 delay:0 options:UIViewAnimationOptionCurveLinear animations:^{
			validityLabel.alpha = 1.0;
		} completion:^(BOOL finished) {
			// nilch
		}];
	}
	
}

- (IBAction)updateViewForFinishedEditing:(UITextField *)sender {
	if ((!is5thGen) && self.iffyLogo.frame.origin.y <= 0) {
		[UIView animateWithDuration:0.25 delay:0 options:UIViewAnimationOptionCurveLinear animations:^{
			for (UITextField *tf in self.view.subviews) {
				tf.frame = CGRectMake(tf.frame.origin.x, tf.frame.origin.y + 80, tf.frame.size.width, tf.frame.size.height);
			}
		} completion:^(BOOL finished) {
			if (finished) {
			}
		}];
	}
	
	if ([self.view.superview.subviews containsObject:validityLabel]) {
		[UIView animateWithDuration:0.25 delay:0 options:UIViewAnimationOptionCurveLinear animations:^{
			validityLabel.alpha = 0.0;
		} completion:^(BOOL finished) {
			if (finished) {
				[validityLabel removeFromSuperview];
			}
		}];
	}
}

- (IBAction)validateTextFieldInput:(UITextField *)sender {
	if (sender == self.nameField) {
		validityLabel.text = @"Enter your name.";
		if ([sender.text length] > 0) {
			validityLabel.backgroundColor = [UIColor greenColor];
			validityLabel.text = @"Name looks good :D";
			validityLabel.textColor = [UIColor colorWithWhite:0.4 alpha:1];
		} else {
			validityLabel.backgroundColor = [UIColor redColor];
			validityLabel.text = @"Please enter your name";
			validityLabel.textColor = [UIColor whiteColor];
		}
	} else if(sender == self.emailField) {
		validityLabel.text = @"Enter your email";
		if ([jvht textIsValidEmail:sender.text]) {
			validityLabel.backgroundColor = [UIColor greenColor];
			validityLabel.text = @"Email looks good :D";
			validityLabel.textColor = [UIColor colorWithWhite:0.4 alpha:1];
		} else {
			validityLabel.backgroundColor = [UIColor redColor];
			validityLabel.text = @"A valid email is required.";
			validityLabel.textColor = [UIColor whiteColor];
		}
	} else if(sender == self.passwordField) {
		if ([sender.text length] < 6) {
			validityLabel.backgroundColor = [UIColor redColor];
			validityLabel.text = @"Password must be at least 6 characters.";
			validityLabel.textColor = [UIColor whiteColor];
		} else {
			validityLabel.backgroundColor = [UIColor greenColor];
			validityLabel.text = @"Password looks good :D";
			validityLabel.textColor = [UIColor colorWithWhite:0.4 alpha:1];
		}
	}
}

- (IBAction)resignKeyboard:(id)sender {
	NSLog(@"%f", self.faceLogo.frame.origin.y);
	for (UITextField * tf in self.view.subviews) {
		[tf resignFirstResponder];
	}
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField {
	if ((!is5thGen) && self.iffyLogo.frame.origin.y <= 0) {
		[UIView animateWithDuration:0.25 delay:0 options:UIViewAnimationOptionCurveLinear animations:^{
			for (UITextField *tf in self.view.subviews) {
				tf.frame = CGRectMake(tf.frame.origin.x, tf.frame.origin.y + 80, tf.frame.size.width, tf.frame.size.height);
			}
		} completion:^(BOOL finished) {
			if (finished) {
			}
		}];
	}
	
	if ([self.view.superview.subviews containsObject:validityLabel]) {
		[UIView animateWithDuration:0.25 delay:0 options:UIViewAnimationOptionCurveLinear animations:^{
			validityLabel.alpha = 0.0;
		} completion:^(BOOL finished) {
			if (finished) {
				[validityLabel removeFromSuperview];
			}
		}];
	}
	
	if (is5thGen) {
		if (textField == self.nameField) {
			[self.emailField becomeFirstResponder];
		} else if(textField == self.emailField) {
			[self.passwordField becomeFirstResponder];
		} else if (textField == self.passwordField) {
			[self signupUser:self.signUpButton];
		}
	}else {
		[textField resignFirstResponder];
	}
	return YES;
}

-(IBAction)unwindFromViewController:(UIStoryboardSegue *)sender {
	
}

-(UIStoryboardSegue *)segueForUnwindingToViewController:(UIViewController *)toViewController fromViewController:(UIViewController *)fromViewController identifier:(NSString *)identifier {
	LoginSceneUnwindSegue *loginUnwind = [[LoginSceneUnwindSegue alloc]initWithIdentifier:identifier source:fromViewController destination:toViewController];
	return loginUnwind;
}

-(BOOL)prefersStatusBarHidden {
	return YES;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
@end
