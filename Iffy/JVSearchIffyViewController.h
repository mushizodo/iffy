//
//  JVSearchIffyViewController.h
//  Iffy
//
//  Created by Joseph Ayo-Vaughan on 3/7/14.
//  Copyright (c) 2014 Joseph Ayo-Vaughan. All rights reserved.
//

#import <Parse/Parse.h>
#import "TextfieldLogin.h"

@interface JVSearchIffyViewController : PFQueryTableViewController <UITextFieldDelegate>
- (IBAction)dismissController:(id)sender;
@property (weak, nonatomic) IBOutlet TextfieldLogin *searchTextField;
@property (weak, nonatomic) IBOutlet UIButton *cancelSearchButton;
- (IBAction)dismissKeyboard:(id)sender;
@property (weak, nonatomic) IBOutlet UISegmentedControl *searchSegControl;
@property (weak, nonatomic) IBOutlet UIView *bgHeaderView;
- (IBAction)updateSearchResults:(TextfieldLogin *)sender;

@end
