//
//  JVChooseModeViewController.m
//  Iffy
//
//  Created by Joseph Ayo-Vaughan on 2/9/14.
//  Copyright (c) 2014 Joseph Ayo-Vaughan. All rights reserved.
//

#import "JVChooseModeViewController.h"

@interface JVChooseModeViewController ()

@end

@implementation JVChooseModeViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	
	if (!is5thGen) {
		CGRect randomFrame = self.randomModeButton.frame;
		CGRect speedyFrame = self.speedyModeButton.frame;
		CGRect exposeFrame = self.exposeModeButton.frame;
		CGRect openlinesFrame = self.openLinesModeButton.frame;
		CGRect byRegionFrame = self.byRegionModeButton.frame;
		CGRect byTopicFrame = self.byTopicModeButton.frame;
		
		static float heightDiff = 36.0;
		
		[self.randomModeButton setFrame:CGRectMake(randomFrame.origin.x, randomFrame.origin.y, randomFrame.size.width, 121)];
		[self.randomModeButton setBackgroundImage:[UIImage imageNamed:@"cm_random_3_5.png"] forState:UIControlStateNormal];
		
		[self.speedyModeButton setFrame:CGRectMake(speedyFrame.origin.x, speedyFrame.origin.y, speedyFrame.size.width, 121)];
		[self.speedyModeButton setBackgroundImage:[UIImage imageNamed:@"cm_speedy_3_5.png"] forState:UIControlStateNormal];
		
		[self.exposeModeButton setFrame:CGRectMake(exposeFrame.origin.x, exposeFrame.origin.y-heightDiff, exposeFrame.size.width, 121)];
		[self.exposeModeButton setBackgroundImage:[UIImage imageNamed:@"cm_expose_3_5.png"] forState:UIControlStateNormal];
		
		[self.openLinesModeButton setFrame:CGRectMake(openlinesFrame.origin.x, openlinesFrame.origin.y-heightDiff, openlinesFrame.size.width, 121)];
		[self.openLinesModeButton setBackgroundImage:[UIImage imageNamed:@"cm_open_lines_3_5.png"] forState:UIControlStateNormal];
		
		[self.byRegionModeButton setFrame:CGRectMake(byRegionFrame.origin.x, byRegionFrame.origin.y-(heightDiff*2), byRegionFrame.size.width, 121)];
		[self.byRegionModeButton setBackgroundImage:[UIImage imageNamed:@"cm_region_3_5.png"] forState:UIControlStateNormal];
		
		[self.byTopicModeButton setFrame:CGRectMake(byTopicFrame.origin.x, byTopicFrame.origin.y-(heightDiff*2), byTopicFrame.size.width, 121)];
		[self.byTopicModeButton setBackgroundImage:[UIImage imageNamed:@"cm_topic_3_5.png"] forState:UIControlStateNormal];
	}
		
}

- (void)viewDidAppear:(BOOL)animated {
	[super viewDidAppear:animated];
	
	if (is5thGen) {
		[[UIApplication sharedApplication]setStatusBarHidden:YES withAnimation:UIStatusBarAnimationFade];
	}
}

- (void)viewWillDisappear:(BOOL)animated {
	[super viewWillDisappear:animated];
	[[UIApplication sharedApplication]setStatusBarHidden:NO withAnimation:UIStatusBarAnimationFade];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
