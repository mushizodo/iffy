//
//  OutGoingCallViewController.h
//  Iffy
//
//  Created by Joseph Ayo-Vaughan on 2/3/14.
//  Copyright (c) 2014 Joseph Ayo-Vaughan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Sinch/Sinch.h>

@interface OutgoingCallViewController : UIViewController <SINCallDelegate>

@property (weak, nonatomic) IBOutlet UILabel *userCallingName;
@property(nonatomic, readwrite, strong) id<SINCall> call;

- (IBAction)hangUp:(UIButton *)sender;

@end
