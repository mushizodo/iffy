//
//  JVMakeCallViewController.m
//  Iffy
//
//  Created by Joseph Ayo-Vaughan on 2/2/14.
//  Copyright (c) 2014 Joseph Ayo-Vaughan. All rights reserved.
//

#import "JVMakeCallViewController.h"
#import "JVMainTabBarController.h"
#import <Parse/Parse.h>
#import "OutgoingCallViewController.h"
#import "IncomingCallViewController.h"

@interface JVMakeCallViewController () {
}

@end

@implementation JVMakeCallViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
//	[[UIApplication sharedApplication]setStatusBarStyle:UIStatusBarStyleDefault animated:YES];
	
	[self.startCallButton setImage:[UIImage imageNamed:@"call_button_s.png"] forState:UIControlStateHighlighted];
	
	self.client = [((JVMainTabBarController *)self.tabBarController).client callClient];
//	[self.client setDelegate:self];
}

- (void)viewWillAppear:(BOOL)animated {
	[super viewWillAppear:animated];
	[[UIApplication sharedApplication]setStatusBarStyle:UIStatusBarStyleDefault animated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//- (id<REBClient>)client {
//    return ((MainTabBarController *)self.tabBarController).client;
//}

- (IBAction)startCall:(UIButton *)sender {
	NSLog(@"Go to call view");
	 id<SINCall>call = [self.client callUserWithId:@"kpRiGNVf1B"];
	[self performSegueWithIdentifier:@"toMakeCall" sender:call];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
	if ([segue.identifier isEqualToString:@"toMakeCall"]) {
		OutgoingCallViewController *og = segue.destinationViewController;
		og.call = sender;
		og.call.delegate = og;
	}
	
}

@end
