//
//  TextfieldLogin.h
//  Iffy
//
//  Created by Joseph Ayo-Vaughan on 1/9/14.
//  Copyright (c) 2014 Joseph Ayo-Vaughan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TextfieldLogin : UITextField

@property (nonatomic) float dxOffset;

@end
