//
//  ProfileTableViewController.m
//  Iffy
//
//  Created by Joseph Ayo-Vaughan on 1/31/14.
//  Copyright (c) 2014 Joseph Ayo-Vaughan. All rights reserved.
//

#import "ProfileTableViewController.h"
#import <Parse/Parse.h>
#import <QuartzCore/QuartzCore.h>
#import "JVHelperTools.h"
#import "SearchTableViewCell.h"
#import "ProfileUserInfoSpaceCell.h"

#define kUsersChosen @"users"
#define kPlacesChosen @"places"
#define kTopicsChosen @"topics"

@interface ProfileTableViewController () {
	NSMutableArray *resultObjects;
	NSMutableArray *cellHeights;
	__block int queryCount;
	UIView *tableLoadingView;
	JVHelperTools *jvht;
	NSString *chosenSearch;
	UITableViewController *tvc;
	UISearchBar *sb;
	UISearchDisplayController *sdc;
}

@end

@implementation ProfileTableViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
//	[[UIApplication sharedApplication]setStatusBarStyle:UIStatusBarStyleLightContent animated:YES];
	jvht = [JVHelperTools new];
	
	sb = [[UISearchBar alloc]initWithFrame:CGRectMake(0, -44, 320, 44)];
	sb.delegate = self;
	sb.showsScopeBar = NO;
	[sb setScopeButtonTitles:@[@"People", @"Places", @"Topics"]];
	[sb setKeyboardType:UIKeyboardTypeDefault];
	[sb setPlaceholder:@"Search Iffy..."];
	[sb setAutocapitalizationType:UITextAutocapitalizationTypeNone];
	[sb setAutocorrectionType:UITextAutocorrectionTypeNo];
	[self.navigationController.view insertSubview:sb belowSubview:self.navigationController.navigationBar];
	
	sdc = [[UISearchDisplayController alloc]initWithSearchBar:sb contentsController:self];
	[sdc setSearchResultsDataSource:self];
	[sdc setSearchResultsDelegate:self];
	
	tvc = [UITableViewController new];
	tvc.tableView = sdc.searchResultsTableView;
	UIRefreshControl *refreshControl = [[UIRefreshControl alloc]init];
	[refreshControl setTintColor:[jvht getOrangeButtonNormalColor]];
	[refreshControl addTarget:self action:@selector(refreshSearchTableView) forControlEvents:UIControlEventValueChanged];
	tvc.refreshControl = refreshControl;
	
	UIRefreshControl *mainRefreshControl = [[UIRefreshControl alloc]init];
	[mainRefreshControl setTintColor:[jvht getOrangeButtonNormalColor]];
	[mainRefreshControl addTarget:self action:@selector(refreshMainTableView) forControlEvents:UIControlEventValueChanged];
	self.refreshControl = mainRefreshControl;
		
	queryCount = 0;
	resultObjects = [[NSMutableArray alloc]init];
	cellHeights = [[NSMutableArray alloc]init];
	
	if (is5thGen) {
		tableLoadingView = [[UIView alloc]initWithFrame:CGRectMake(0, 200, 320, 75)];
	} else {
		tableLoadingView = [[UIView alloc]initWithFrame:CGRectMake(0, 100, 320, 75)];
	}
	
	[jvht addProgressviewToSuperview:tableLoadingView];
	
	chosenSearch = @"users";
	
//	if (SYSTEM_VERSION_LESS_THAN(@"7.0")) {
//		sdc.searchBar.frame = CGRectMake(sdc.searchBar.frame.origin.x, -44, sdc.searchBar.frame.size.width, sdc.searchBar.frame.size.height);
//		NSLog(@"%f", sdc.searchBar.frame.origin.y);
//	}
	
	[sdc.searchResultsTableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
	
	[sdc.searchBar setScopeBarButtonTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[jvht getRedButtonNormalColor], UITextAttributeTextColor, [UIFont fontWithName:@"GillSans-Light" size:15], UITextAttributeFont, [UIColor clearColor], UITextAttributeTextShadowColor, nil] forState:UIControlStateNormal];
	
	[sdc.searchBar setScopeBarButtonTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[jvht getButtonHighlightedTextColor], UITextAttributeTextColor, [UIFont fontWithName:@"GillSans-Light" size:15], UITextAttributeFont, [UIColor clearColor], UITextAttributeTextShadowColor, nil] forState:UIControlStateHighlighted];
	
	[sdc.searchBar setScopeBarButtonTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor whiteColor], UITextAttributeTextColor, [UIFont fontWithName:@"GillSans-Light" size:15], UITextAttributeFont, [UIColor clearColor], UITextAttributeTextShadowColor, nil] forState:UIControlStateSelected];
	
	[sdc.searchBar setTintColor:[jvht getRedButtonHighlightedColor]];
	
	UIButton *addButton = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 30, 20)];
	if (!is5thGen) {
		[addButton setImageEdgeInsets:UIEdgeInsetsMake(0, 10, 0, -10)];
	}
	[addButton setImage:[UIImage imageNamed:@"search_alien.png"] forState:UIControlStateNormal];
	[addButton setShowsTouchWhenHighlighted:YES];
	[addButton addTarget:self action:@selector(goToSearch) forControlEvents:UIControlEventTouchUpInside];
	UIBarButtonItem *addBarButton = [[UIBarButtonItem alloc]initWithCustomView:addButton];
	self.navigationItem.leftBarButtonItem = addBarButton;
	
	UIButton *editProfileButton = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 30, 20)];
	if (!is5thGen) {
		[editProfileButton setImageEdgeInsets:UIEdgeInsetsMake(0, -10, 0, 10)];
	}
	[editProfileButton setImage:[UIImage imageNamed:@"edit_prof.png"] forState:UIControlStateNormal];
	[editProfileButton setShowsTouchWhenHighlighted:YES];
	UIBarButtonItem *editProfileBarButton = [[UIBarButtonItem alloc]initWithCustomView:editProfileButton];
	self.navigationItem.rightBarButtonItem = editProfileBarButton;
	
//	UIView *barItems = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 50, 20)];
//	UIButton *settings = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 20, 20)];
//	[settings setBackgroundImage:[UIImage imageNamed:@"settings.png"] forState:UIControlStateNormal];
//	UIButton *contacts = [[UIButton alloc] initWithFrame:CGRectMake(10, 0, 20, 20)];
//	[settings setBackgroundImage:[UIImage imageNamed:@"contacts.png"] forState:UIControlStateNormal];
//	
//	[barItems addSubview:settings];
//	[barItems addSubview:contacts];
//	UIBarButtonItem *rightButtons = [[UIBarButtonItem alloc]initWithCustomView:barItems];
//	self.navigationItem.rightBarButtonItem = rightButtons;
	
	[sdc.searchResultsTableView registerNib:[UINib nibWithNibName:@"SearchTableViewCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"JVCell"];
	
//	[self.mainTableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"UserSpace"];
	
	[self.mainTableView registerNib:[UINib nibWithNibName:@"ProfileUserInfoSpaceCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"UserInfoSpace"];
	
//	sdc.searchBar.frame = CGRectOffset(sdc.searchBar.frame, 0, -44.0);
	
}

- (void)viewDidAppear:(BOOL)animated {
	[super viewDidAppear:animated];
	[self.mainTableView reloadData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)viewProfilePic:(UIButton *)sender {
	[self performSegueWithIdentifier:@"toPhotoView" sender:self];
	CGRect frame = sender.frame;
	NSLog(@"button width: %f, height: %f", frame.size.width, frame.size.height);
}

- (void)goToSearch {
//	[sdc.searchBar becomeFirstResponder];
	[self performSegueWithIdentifier:@"toSearch" sender:self];
}

- (void)viewFollowers:(id)sender {
	NSLog(@"View Followers");
}

- (void) viewFollowing:(id)sender {
	NSLog(@"View Following");
}

- (void)refreshSearchTableView {
	[self beginQueriesWithSearchText:sdc.searchBar.text];
}

- (void)refreshMainTableView {
	[self.mainTableView reloadData];
}

- (void)beginQueriesWithSearchText: (NSString *)searchText {
	[resultObjects removeAllObjects];
	
	queryCount = 0;
	[sdc.searchResultsTableView reloadData];
	
	[self.view.superview addSubview:tableLoadingView];
	
	if (![searchText isEqualToString:@""]) {
		PFQuery *countQuery;
		if ([chosenSearch isEqualToString:kUsersChosen]) {
			countQuery = [PFUser query];
		} else if([chosenSearch isEqualToString:kPlacesChosen]) {
			countQuery = [PFQuery queryWithClassName:@"Places"];
		} else {
			countQuery = [PFQuery queryWithClassName:@"Topics"];
		}
		//		countQuery  = [PFUser query];
		countQuery.limit = 10;
		[countQuery whereKey:@"name" hasPrefix:searchText];
		[countQuery whereKey:@"canIndex" equalTo:[NSNumber numberWithBool:YES]];
		[countQuery countObjectsInBackgroundWithBlock:^(int number, NSError *error) {
			if (!error) {
				queryCount = number;
				if (queryCount == 0) {
					[tableLoadingView removeFromSuperview];
				}
				[sdc.searchResultsTableView reloadData];
			} else {
				queryCount = 0;
				[tableLoadingView removeFromSuperview];
				[sdc.searchResultsTableView reloadData];
			}
		}];
		
		PFQuery *findQuery;
		if ([chosenSearch isEqualToString:kUsersChosen]) {
			findQuery = [PFUser query];
		} else if([chosenSearch isEqualToString:kPlacesChosen]) {
			findQuery = [PFQuery queryWithClassName:@"Places"];
		} else {
			findQuery = [PFQuery queryWithClassName:@"Topics"];
		}
		//		findQuery = [PFUser query];
		findQuery.limit = 10;
		[findQuery whereKey:@"name" hasPrefix:searchText];
		[findQuery whereKey:@"canIndex" equalTo:[NSNumber numberWithBool:YES]];
		[findQuery findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
			if (!error) {
				for (PFObject *obj in objects) {
					[resultObjects addObject:obj];
				}
				
				[sdc.searchResultsTableView reloadData];
			}
		}];
	} else {
		[tableLoadingView removeFromSuperview];
	}
	
	[sdc.searchResultsTableView reloadData];
	
	[tvc.refreshControl endRefreshing];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	if (tableView == sdc.searchResultsTableView) {
		return queryCount;
	} else {
		return 1;
	}
	
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	if (tableView == sdc.searchResultsTableView) {
		NSString *CellIdentifier = @"JVCell";
		
		SearchTableViewCell *cell = (SearchTableViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
		
		if (cell == nil) {
			NSLog(@"cell is nil");
			NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"SearchTableViewCell" owner:self options:nil];
			cell = [nib objectAtIndex:0];
		}
		
		cell.userImageView.layer.masksToBounds = YES;
		cell.userImageView.layer.cornerRadius = cell.userImageView.frame.size.height/2;
		
		//	double delayInSeconds = 2.0;
		//	dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
		//	dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
		[tableLoadingView removeFromSuperview];
		if ([resultObjects count] != 0 && [resultObjects count] == queryCount && indexPath.row <= [resultObjects count]) {
			
			PFUser *user = [resultObjects objectAtIndex:indexPath.row];
			cell.userNameLabel.text = user[@"name"];
			cell.userInfoLabel.text = user[@"email"];
			if ([cell.userNameLabel.text isEqualToString:@""]) {
				cell.userNameLabel.text = user[@"name"];
				[sdc.searchResultsTableView reloadData];
			}
			
			if ([cell.userInfoLabel.text isEqualToString:@""]) {
				cell.userInfoLabel.text = user[@"email"];
				[sdc.searchResultsTableView reloadData];
			}
			if (indexPath.row % 2 == 0) {
				cell.determinedFwdBgView.backgroundColor = [jvht getRedButtonNormalColor];
			} else {
				cell.determinedFwdBgView.backgroundColor = [jvht getOrangeButtonNormalColor];
			}
			if (user[@"photo_small"] != nil) {
				PFFile *imageFile = user[@"photo_small"];
				[imageFile getDataInBackgroundWithBlock:^(NSData *data, NSError *error) {
					if (!error) {
						NSLog(@"image!!");
						cell.userImageView.image = [UIImage imageWithData:data];
						if (cell.userImageView.image == nil) {
//							cell.userImageView.image = [UIImage imageWithData:data];
//							[sdc.searchResultsTableView reloadData];
						}
					}
				}];
			} else {
				cell.userImageView.image = [UIImage imageNamed:@"default_photo.png"];
			}
		}
		//	});
		return cell;
	} else {
		NSString *CellIdentifier = @"UserInfoSpace";
		
		ProfileUserInfoSpaceCell *cell = (ProfileUserInfoSpaceCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
		
		if (cell == nil) {
			NSLog(@"cell is nil");
			NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"ProfileUserInfoSpaceCell" owner:self options:nil];
			cell = [nib objectAtIndex:0];
		}
				
		cell.profilePhotoView.layer.masksToBounds = YES;
		cell.profilePhotoView.layer.cornerRadius = cell.profilePhotoView.frame.size.height/2;
		
		cell.profilePhotoViewBG.alpha = 0.5;
		cell.profilePhotoViewBG.layer.masksToBounds = YES;
		cell.profilePhotoViewBG.layer.cornerRadius = cell.profilePhotoViewBG.frame.size.height/2;
		
		cell.viewPhotoButton.layer.masksToBounds = YES;
		cell.viewPhotoButton.layer.cornerRadius = cell.viewPhotoButton.frame.size.height/2;
		
		cell.whiteCutout.layer.cornerRadius = cell.whiteCutout.frame.size.height/2;
		
		cell.nameLabel.text = [PFUser currentUser][@"name"];
		
		NSString *docDir = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)[0];
		NSString *smallImagePath = [docDir stringByAppendingPathComponent:@"user.jpg"];
		
		if ([[NSFileManager defaultManager]fileExistsAtPath:smallImagePath]) {
			UIImage *profImage = [UIImage imageWithContentsOfFile:smallImagePath];
			
			cell.profilePhotoView.image = profImage;
			
		}else if ([PFUser currentUser][@"photo_small"] != nil) {
			
			PFFile *imageFile = [PFUser currentUser][@"photo_small"];
			
			[imageFile getDataInBackgroundWithBlock:^(NSData *data, NSError *error) {
				if (!error) {
					cell.profilePhotoView.image = [UIImage imageWithData:data];
				} else {
					cell.profilePhotoView.image = [UIImage imageNamed:@"default_photo.png"];
				}
			}];
			
		}else {
			cell.profilePhotoView.image = [UIImage imageNamed:@"default_photo.png"];
		}
		
		[cell.followersButton setBackgroundImage:[jvht imageFromColor:[jvht getRedButtonHighlightedColor]] forState:UIControlStateHighlighted];
		[cell.followingButton setBackgroundImage:[jvht imageFromColor:[jvht getOrangeButtonHighlightedColor]] forState:UIControlStateHighlighted];
		
		[cell.viewPhotoButton addTarget:self action:@selector(viewProfilePic:) forControlEvents:UIControlEventTouchUpInside];
		[cell.followersButton addTarget:self action:@selector(viewFollowers:) forControlEvents:UIControlEventTouchUpInside];
		[cell.followingButton addTarget:self action:@selector(viewFollowing:) forControlEvents:UIControlEventTouchUpInside];
		
		[self.refreshControl endRefreshing];
		return cell;
	}
	
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
	[self beginQueriesWithSearchText:searchText];
}

- (BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchString:(NSString *)searchString {
	[self beginQueriesWithSearchText:searchString];
	return YES;
}

- (void)searchBar:(UISearchBar *)searchBar selectedScopeButtonIndexDidChange:(NSInteger)selectedScope {
	NSLog(@"changed");
	queryCount = 0;
	[resultObjects removeAllObjects];
	if (selectedScope == 0) {
		chosenSearch = kUsersChosen;
		[sdc.searchBar setTintColor:[jvht getRedButtonNormalColor]];
		[sdc.searchBar setScopeBarButtonTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[jvht getRedButtonNormalColor], UITextAttributeTextColor, [UIFont fontWithName:@"GillSans-Light" size:15], UITextAttributeFont, [UIColor clearColor], UITextAttributeTextShadowColor, nil] forState:UIControlStateNormal];
		[sdc.searchResultsTableView reloadData];
	} else if (selectedScope == 1) {
		chosenSearch = kPlacesChosen;
		[sdc.searchBar setTintColor:[UIColor colorWithRed:40.0/255.0 green:40.0/255.0 blue:40.0/255.0 alpha:1.0]];
		[sdc.searchBar setScopeBarButtonTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor colorWithRed:40.0/255.0 green:40.0/255.0 blue:40.0/255.0 alpha:1.0], UITextAttributeTextColor, [UIFont fontWithName:@"GillSans-Light" size:15], UITextAttributeFont, [UIColor clearColor], UITextAttributeTextShadowColor, nil] forState:UIControlStateNormal];
		[sdc.searchResultsTableView reloadData];
	} else {
		chosenSearch = kTopicsChosen;
		[sdc.searchBar setTintColor:[jvht getOrangeButtonNormalColor]];
		[sdc.searchBar setScopeBarButtonTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[jvht getOrangeButtonNormalColor], UITextAttributeTextColor, [UIFont fontWithName:@"GillSans-Light" size:15], UITextAttributeFont, [UIColor clearColor], UITextAttributeTextShadowColor, nil] forState:UIControlStateNormal];
		[sdc.searchResultsTableView reloadData];
	}
	
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
	[self beginQueriesWithSearchText:searchBar.text];
}

- (BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchScope:(NSInteger)searchOption {
	[self beginQueriesWithSearchText:controller.searchBar.text];
	return YES;
}

- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar {
//	if (SYSTEM_VERSION_LESS_THAN(@"7.0")) {
//		if (searchBar.frame.origin.y == -44 && [searchBar.text isEqualToString:@""]) {
//			searchBar.frame = CGRectMake(searchBar.frame.origin.x, 0, searchBar.frame.size.width, searchBar.frame.size.height);
//		}
//	}
	
}

- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar {
//	if (SYSTEM_VERSION_LESS_THAN(@"7.0") && [searchBar.text isEqualToString:@""]) {
//		searchBar.frame = CGRectMake(searchBar.frame.origin.x, -44, searchBar.frame.size.width, searchBar.frame.size.height);
//	}
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar {
//	if (SYSTEM_VERSION_LESS_THAN(@"7.0") && ![searchBar.text isEqualToString:@""]) {
//		searchBar.frame = CGRectMake(searchBar.frame.origin.x, -44, searchBar.frame.size.width, searchBar.frame.size.height);
//	}
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == self.mainTableView) {
		if (indexPath.row == 0) {
			return 290;
		} else {
			return 60;
		}
	} else {
		return 60;
	}
}

//- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath {
//	return 60;
//}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
//	NSLog(@"%f", scrollView.contentOffset.y);
	
	if (scrollView.contentOffset.y < -240) {
		[scrollView setContentOffset:CGPointMake(scrollView.contentOffset.x, -240)];
	}
	
}

- (void)slowDownScrollWithGestureRecognizer: (UIPanGestureRecognizer *)recognizer {
	
}

@end
