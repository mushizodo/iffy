//
//  PhotoViewController.h
//  Iffy
//
//  Created by Joseph Ayo-Vaughan on 1/26/14.
//  Copyright (c) 2014 Joseph Ayo-Vaughan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DottedLineView.h"

@interface PhotoViewController : UIViewController <UIScrollViewDelegate, UIActionSheetDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate>
@property (weak, nonatomic) IBOutlet UIView *finalFrame;
@property (weak, nonatomic) IBOutlet UIScrollView *initialFrame;
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIView *imageViewContainerView;
@property (weak, nonatomic) IBOutlet UIView *photoWrapView;
@property (weak, nonatomic) IBOutlet UIButton *cancelButton;
@property (weak, nonatomic) IBOutlet UIButton *uploadButton;
@property (weak, nonatomic) IBOutlet UIButton *finishedButton;
@property (weak, nonatomic) IBOutlet UILabel *messageLabel;

- (IBAction)saveImage:(UIButton *)sender;
- (IBAction)goBack:(UIButton *)sender;
- (IBAction)changePhoto:(UIButton *)sender;
@end
