//
//  ProfileNavigationController.m
//  Iffy
//
//  Created by Joseph Ayo-Vaughan on 1/25/14.
//  Copyright (c) 2014 Joseph Ayo-Vaughan. All rights reserved.
//

#import "ProfileNavigationController.h"
#import "JVHelperTools.h"
#import <Parse/Parse.h>

@interface ProfileNavigationController () {
	JVHelperTools *jvht;
}

@end

@implementation ProfileNavigationController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	
	jvht = [JVHelperTools new];
	[[PFUser currentUser]refreshInBackgroundWithBlock:^(PFObject *object, NSError *error) {
		NSLog(@"User refreshed");
	}];
		
//	NSString *docDir = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)[0];
//	NSString *smallImagePath = [docDir stringByAppendingPathComponent:@"user.jpg"];
//	[[PFUser currentUser]refresh];
//	
//	if ([[NSFileManager defaultManager]fileExistsAtPath:smallImagePath]) {
//		NSLog(@"exists!!!");
//		UIImage *profImage = [UIImage imageWithContentsOfFile:smallImagePath];
//		
//		self.profilePhoto = profImage;
//		
//	}else if ([PFUser currentUser][@"photo_small"] != nil) {
//		
//		PFFile *imageFile = [PFUser currentUser][@"photo_small"];
//		
//		[imageFile getDataInBackgroundWithBlock:^(NSData *data, NSError *error) {
//			if (!error) {
//				self.profilePhoto = [UIImage imageWithData:data];
//			} else if ([[NSFileManager defaultManager]fileExistsAtPath:smallImagePath]) {
//				NSLog(@"exists!!!");
//				UIImage *profImage = [UIImage imageWithContentsOfFile:smallImagePath];
//				
//				self.profilePhoto = profImage;
//			}
//		}];
//		
//	}else {
//		self.profilePhoto = [UIImage imageNamed:@"default_photo.png"];
//		NSLog(@"no user photo for user: %@", [PFUser currentUser][@"name"]);
//	}
}

- (void)viewWillAppear:(BOOL)animated {
	[super viewWillAppear:animated];
	[[UIApplication sharedApplication]setStatusBarStyle:UIStatusBarStyleLightContent animated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)prefersStatusBarHidden {
	return NO;
}

@end
