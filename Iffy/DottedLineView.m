//
//  DottedLineView.m
//  Iffy
//
//  Created by Joseph Ayo-Vaughan on 1/27/14.
//  Copyright (c) 2014 Joseph Ayo-Vaughan. All rights reserved.
//

#import "DottedLineView.h"

@implementation DottedLineView

static CGFloat const kDashedBorderWidth     = (2.0f);
static CGFloat const kDashedPhase           = (0.0f);
static CGFloat const kDashedLinesLength[]   = {4.0f, 2.0f};
static size_t const kDashedCount            = (2.0f);

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
		// initialization...
    }
    return self;
}


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    [super drawRect:rect];
	
	CGContextRef context = UIGraphicsGetCurrentContext();
	
    CGContextSetLineWidth(context, kDashedBorderWidth);
    CGContextSetStrokeColorWithColor(context, [UIColor redColor].CGColor);
	
    CGContextSetLineDash(context, kDashedPhase, kDashedLinesLength, kDashedCount) ;
	
    CGContextAddRect(context, rect);
    CGContextStrokePath(context);
}


@end
