//
//  FinishTwitterViewController.m
//  Iffy
//
//  Created by Joseph Ayo-Vaughan on 1/10/14.
//  Copyright (c) 2014 Joseph Ayo-Vaughan. All rights reserved.
//

#import "FinishTwitterViewController.h"
#import "LoginViewController.h"
#import <Parse/Parse.h>
#import <QuartzCore/QuartzCore.h>
#import "JVHelperTools.h"

@interface FinishTwitterViewController () {
	JVHelperTools *jvht;
	UIStoryboard *sb;
	LoginViewController *vc;
	UILabel *validityLabel;
}

@end

@implementation FinishTwitterViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	
	jvht = [JVHelperTools new];
	
	UIColor *c_c6c6c6 = [jvht getButtonHighlightedTextColor];
	
	self.finishTwitterButton.backgroundColor = [jvht getRedButtonNormalColor];
	UIImage *finishPressedImage = [[jvht imageFromColor:[jvht getRedButtonHighlightedColor]] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 0, 0, 0)];
	[self.finishTwitterButton setBackgroundImage:finishPressedImage forState:UIControlStateHighlighted];
	[self.finishTwitterButton setTitleColor:c_c6c6c6 forState:UIControlStateHighlighted];
	
	UIImage *backPressedImage = [[UIImage imageNamed:@"orange_up_arrow_pressed.png"] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 0, 0, 0)];
	[self.backButton setBackgroundImage:backPressedImage forState:UIControlStateHighlighted];
	
	if (!is5thGen) {
		self.tNameField.returnKeyType = UIReturnKeyDefault;
		self.tEmailField.returnKeyType = UIReturnKeyDefault;
	}
	
	UIScreen *screen = [UIScreen mainScreen];
	
	sb = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
	vc = [sb instantiateViewControllerWithIdentifier:@"loginViewController"];
	vc.view.frame = CGRectMake(0, -(self.view.frame.size.height), self.view.frame.size.width, screen.bounds.size.height);
	vc.view.alpha = 0.0;
	
	[self.view addSubview:vc.view];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)finishTwitterSignup:(id)sender {
	
	[self.tNameField resignFirstResponder];
	[self.tEmailField resignFirstResponder];
	[self updateViewForFinishedEditing:self.resignerButton];
	
	[jvht addProgressviewToSuperview:self.view.superview];
	
	self.tNameField.enabled = NO;
	self.tEmailField.enabled = NO;
	self.finishTwitterButton.enabled = NO;
	self.infoButton.enabled = NO;
	self.backButton.enabled = NO;
	
	if ([jvht internetAccessIsAvailable]) {
		NSString *nameRaw = [self.tNameField text];
		NSString *emailRaw = [self.tEmailField text];
		
		if (![jvht textFieldInputIsWhiteSpace:nameRaw] && [jvht textIsValidEmail:emailRaw]) {
			
			[PFUser currentUser][@"name"] = self.tNameField.text;
			[PFUser currentUser].email = self.tEmailField.text;
			[PFUser currentUser].username = self.tEmailField.text;
			[[PFUser currentUser] saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
				[jvht removeProgressViewFromView:self.view.superview];
				
				// JUST FOR TESTING PURPOSES
				// SHOULD GO TO HOME PAGE
				self.tNameField.enabled = YES;
				self.tEmailField.enabled = YES;
				self.finishTwitterButton.enabled = YES;
				self.infoButton.enabled = YES;
				self.backButton.enabled = YES;
				
				if (error) {
					if (error.code == kParseErrorEmailTaken || error.code == kParseErrorUsernameTaken) {
						UIAlertView *emailTakenAlert = [[UIAlertView alloc]
														initWithTitle:@"Email Already Used"
														message:@"That email address is already registered. Did you want to login?"
														delegate:self
														cancelButtonTitle:@"Cancel"
														otherButtonTitles:@"Login", nil];
						emailTakenAlert.accessibilityLabel = @"emailTaken";
						[emailTakenAlert show];
					} else {
						[jvht showAlertViewForReason:JVHTGeneralError];
					}
					NSLog(@"%@", error);
				} else {
					NSLog(@"Success!");
					[self performSegueWithIdentifier:@"toVerification" sender:self];
				}
			}];
			
		} else {
			self.tNameField.enabled = YES;
			self.tEmailField.enabled = YES;
			self.finishTwitterButton.enabled = YES;
			self.infoButton.enabled = YES;
			self.backButton.enabled = YES;
			
			[jvht removeProgressViewFromView:self.view.superview];
			
			if ([jvht textFieldInputIsWhiteSpace:nameRaw]) {
				self.tNameField.layer.borderColor = [jvht getOrangeButtonNormalColor].CGColor;
				self.tNameField.layer.borderWidth = 2.0;
			}else {
				self.tNameField.layer.borderWidth = 0;
			}
			
			if (![jvht textIsValidEmail:emailRaw]) {
				self.tEmailField.layer.borderColor = [jvht getOrangeButtonNormalColor].CGColor;
				self.tEmailField.layer.borderWidth = 2.0;
			} else {
				self.tEmailField.layer.borderWidth = 0;
			}
		}
	} else {
		[jvht removeProgressViewFromView:self.view.superview];
		[jvht showAlertViewForReason:JVHTNoInternetConnection];
		self.tNameField.enabled = YES;
		self.tEmailField.enabled = YES;
		self.finishTwitterButton.enabled = YES;
		self.infoButton.enabled = YES;
		self.backButton.enabled = YES;
	}
	
}

- (IBAction)resignKeyboard:(id)sender {
	[self.tNameField resignFirstResponder];
	[self.tEmailField resignFirstResponder];
}

- (IBAction)updateViewForEditing:(UITextField *)sender {
	self.backButton.enabled = NO;
	sender.layer.borderWidth = 0;
	
	if ((!is5thGen) && self.tNameField.frame.origin.y == 102) {
		[UIView animateWithDuration:0.25 delay:0 options:UIViewAnimationOptionCurveLinear animations:^{
			self.hiLabel.alpha = 0.0;
			self.emailLabel.alpha = 0.0;
			
			self.tNameField.frame = CGRectMake(self.tNameField.frame.origin.x, self.tNameField.frame.origin.y - 40, self.tNameField.frame.size.width, self.tNameField.frame.size.height);
			self.tEmailField.frame = CGRectMake(self.tEmailField.frame.origin.x, self.tEmailField.frame.origin.y - 60, self.tEmailField.frame.size.width, self.tEmailField.frame.size.height);
			self.finishTwitterButton.frame = CGRectMake(self.finishTwitterButton.frame.origin.x, self.finishTwitterButton.frame.origin.y - 60, self.finishTwitterButton.frame.size.width, self.finishTwitterButton.frame.size.height);
		} completion:^(BOOL finished) {
			if (finished) {
			}
		}];
	}
	
	if ([self.view.superview.subviews containsObject:validityLabel]) {
		if (sender == self.tNameField) {
			validityLabel.text = @"Enter your name.";
			if ([sender.text length] > 0) {
				validityLabel.backgroundColor = [UIColor greenColor];
				validityLabel.text = @"Name looks good :D";
				validityLabel.textColor = [UIColor colorWithWhite:0.4 alpha:1];
			} else {
				validityLabel.backgroundColor = [UIColor redColor];
				validityLabel.text = @"Please enter your name";
				validityLabel.textColor = [UIColor whiteColor];
			}
		} else if(sender == self.tEmailField) {
			validityLabel.text = @"Enter your email";
			if ([jvht textIsValidEmail:sender.text]) {
				validityLabel.backgroundColor = [UIColor greenColor];
				validityLabel.text = @"Email looks good :D";
				validityLabel.textColor = [UIColor colorWithWhite:0.4 alpha:1];
			} else {
				validityLabel.backgroundColor = [UIColor redColor];
				validityLabel.text = @"A valid email is required.";
				validityLabel.textColor = [UIColor whiteColor];
			}
		}
	} else {
		//  if (self.signUpButton.frame.size.width == [UIScreen mainScreen].bounds.size.width)
		CGRect validityFrame = CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 30);
		validityLabel = [[UILabel alloc]initWithFrame:validityFrame];
		[validityLabel setFont:[UIFont fontWithName:@"GillSans-Light" size:15]];
		validityLabel.backgroundColor = [UIColor redColor];
		validityLabel.textColor = [UIColor whiteColor];
		validityLabel.textAlignment = NSTextAlignmentCenter;
		validityLabel.alpha = 0.0;
		if (sender == self.tNameField) {
			validityLabel.text = @"Enter your name.";
			if ([sender.text length] > 0) {
				validityLabel.backgroundColor = [UIColor greenColor];
				validityLabel.text = @"Name looks good :D";
				validityLabel.textColor = [UIColor colorWithWhite:0.4 alpha:1];
			} else {
				validityLabel.backgroundColor = [UIColor redColor];
				validityLabel.text = @"Please enter your name";
				validityLabel.textColor = [UIColor whiteColor];
			}
		} else if(sender == self.tEmailField) {
			validityLabel.text = @"Enter your email";
			if ([jvht textIsValidEmail:sender.text]) {
				validityLabel.backgroundColor = [UIColor greenColor];
				validityLabel.text = @"Email looks good :D";
				validityLabel.textColor = [UIColor colorWithWhite:0.4 alpha:1];
			} else {
				validityLabel.backgroundColor = [UIColor redColor];
				validityLabel.text = @"A valid email is required.";
				validityLabel.textColor = [UIColor whiteColor];
			}
		}
		
		[self.view.superview addSubview:validityLabel];
		
		[UIView animateWithDuration:0.25 delay:0 options:UIViewAnimationOptionCurveLinear animations:^{
			validityLabel.alpha = 1.0;
		} completion:^(BOOL finished) {
			// nilch
		}];
	}
}

- (IBAction)updateViewForFinishedEditing:(id)sender {
	self.backButton.enabled = YES;
	if ((!is5thGen) && self.tNameField.frame.origin.y == 62) {
		[UIView animateWithDuration:0.25 delay:0 options:UIViewAnimationOptionCurveLinear animations:^{
			self.hiLabel.alpha = 1.0;
			self.emailLabel.alpha = 1.0;
			
			self.tNameField.frame = CGRectMake(self.tNameField.frame.origin.x, self.tNameField.frame.origin.y + 40, self.tNameField.frame.size.width, self.tNameField.frame.size.height);
			self.tEmailField.frame = CGRectMake(self.tEmailField.frame.origin.x, self.tEmailField.frame.origin.y + 60, self.tEmailField.frame.size.width, self.tEmailField.frame.size.height);
			self.finishTwitterButton.frame = CGRectMake(self.finishTwitterButton.frame.origin.x, self.finishTwitterButton.frame.origin.y + 60, self.finishTwitterButton.frame.size.width, self.finishTwitterButton.frame.size.height);
		} completion:^(BOOL finished) {
			if (finished) {
			}
		}];
	}
	
	if ([self.view.superview.subviews containsObject:validityLabel]) {
		[UIView animateWithDuration:0.25 delay:0 options:UIViewAnimationOptionCurveLinear animations:^{
			validityLabel.alpha = 0.0;
		} completion:^(BOOL finished) {
			if (finished) {
				[validityLabel removeFromSuperview];
			}
		}];
	}
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField {
	if (is5thGen) {
		if (textField == self.tNameField) {
			[self.tEmailField becomeFirstResponder];
		} else {
			[self.tEmailField resignFirstResponder];
			[self finishTwitterSignup:self.finishTwitterButton];
		}
	} else {
		
		[textField resignFirstResponder];
	}
	
	return YES;
}

- (IBAction)slidePageView:(UIPanGestureRecognizer *)recognizer {
	CGPoint velocity = [recognizer velocityInView:self.view];
	
	if (vc.view.alpha != 1.0) {
		vc.view.alpha = 1.0;
	}
	
	if (recognizer.view.frame.origin.y > 0) {
		for (UITextField *tf in self.view.subviews) {
			if ([tf isFirstResponder]) {
				[tf resignFirstResponder];
			}
		}
		
		[self updateViewForFinishedEditing:self.resignerButton];
		
		CGPoint translation = [recognizer translationInView:self.view];
		
		if (recognizer.view.frame.origin.y + translation.y >= 0 && vc.view.frame.origin.y + vc.view.frame.size.height <= vc.view.frame.size.height/2) {
			recognizer.view.center = CGPointMake(recognizer.view.center.x, recognizer.view.center.y + translation.y);
			
			vc.view.frame = CGRectMake(0, recognizer.view.frame.origin.y - recognizer.view.frame.size.height, recognizer.view.frame.size.width, recognizer.view.frame.size.height);
		}
	}else if (velocity.y > 300 && velocity.y < 1000) {
		for (UITextField *tf in self.view.subviews) {
			if ([tf isFirstResponder]) {
				[tf resignFirstResponder];
			}
		}
		
		[self updateViewForFinishedEditing:self.resignerButton];
		
		CGPoint translation = [recognizer translationInView:self.view];
		
		if (recognizer.view.frame.origin.y + translation.y >= 0 && vc.view.frame.origin.y + vc.view.frame.size.height <= vc.view.frame.size.height/2) {
			recognizer.view.center = CGPointMake(recognizer.view.center.x, recognizer.view.center.y + translation.y);
			
			vc.view.frame = CGRectMake(0, recognizer.view.frame.origin.y - recognizer.view.frame.size.height, recognizer.view.frame.size.width, recognizer.view.frame.size.height);
		}
		
	} else if(velocity.y <= 0) {
		for (UITextField *tf in self.view.subviews) {
			if ([tf isFirstResponder]) {
				[tf resignFirstResponder];
			}
		}
		
		[self updateViewForFinishedEditing:self.resignerButton];
		
		CGPoint translation = [recognizer translationInView:self.view];
		
		if (recognizer.view.frame.origin.y + translation.y >= 0 && vc.view.frame.origin.y + vc.view.frame.size.height <= vc.view.frame.size.height/2) {
			recognizer.view.center = CGPointMake(recognizer.view.center.x, recognizer.view.center.y + translation.y);
			
			vc.view.frame = CGRectMake(0, recognizer.view.frame.origin.y - recognizer.view.frame.size.height, recognizer.view.frame.size.width, recognizer.view.frame.size.height);
		}
	}
	
	if(recognizer.state == UIGestureRecognizerStateEnded) {
		if (vc.view.frame.origin.y + vc.view.frame.size.height >= vc.view.frame.size.height/4) {
			[UIView animateWithDuration:0.25 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
				recognizer.view.frame = CGRectMake(0, recognizer.view.frame.size.height, recognizer.view.frame.size.width, recognizer.view.frame.size.height);
				vc.view.frame = CGRectMake(0, -(self.view.frame.size.height), vc.view.frame.size.width, vc.view.frame.size.height);
			} completion:^(BOOL finished) {
				
				[vc.view removeFromSuperview];
				[self presentViewController:vc animated:NO completion:nil];
				
			}];
		}else {
			[UIView animateWithDuration:0.25 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
				recognizer.view.frame = CGRectMake(0, 0, recognizer.view.frame.size.width, recognizer.view.frame.size.height);
				vc.view.frame = CGRectMake(0, -(self.view.frame.size.height), self.view.frame.size.width, self.view.frame.size.height);
			} completion:^(BOOL finished) {
			}];
		}
	}
		
	[recognizer setTranslation:CGPointMake(0, 0) inView:self.view];
	
}

- (IBAction)validateTextFieldInput:(UITextField *)sender {
	if (sender == self.tNameField) {
		validityLabel.text = @"Enter your name.";
		if ([sender.text length] > 0) {
			validityLabel.backgroundColor = [UIColor greenColor];
			validityLabel.text = @"Name looks good :D";
			validityLabel.textColor = [UIColor colorWithWhite:0.4 alpha:1];
		} else {
			validityLabel.backgroundColor = [UIColor redColor];
			validityLabel.text = @"Please enter your name";
			validityLabel.textColor = [UIColor whiteColor];
		}
	} else if(sender == self.tEmailField) {
		validityLabel.text = @"Enter your email";
		if ([jvht textIsValidEmail:sender.text]) {
			validityLabel.backgroundColor = [UIColor greenColor];
			validityLabel.text = @"Email looks good :D";
			validityLabel.textColor = [UIColor colorWithWhite:0.4 alpha:1];
		} else {
			validityLabel.backgroundColor = [UIColor redColor];
			validityLabel.text = @"A valid email is required.";
			validityLabel.textColor = [UIColor whiteColor];
		}
	}
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
	if (buttonIndex == 1) {
		[self.backButton sendActionsForControlEvents:UIControlEventAllEvents];
	}
}


@end
