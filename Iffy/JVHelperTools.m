//
//  JVHelperTools.m
//  Iffy
//
//  Created by Joseph Ayo-Vaughan on 1/11/14.
//  Copyright (c) 2014 Joseph Ayo-Vaughan. All rights reserved.
//

#import "JVHelperTools.h"
#import <QuartzCore/QuartzCore.h>

#define kLoadingBGHeight 100.0
#define kLoadingImageWidth 200.0
#define kLoadinImageHeight 75.0

@interface JVHelperTools () {
	UIERealTimeBlurView *blurView;
	UIView *transView;
//	UILabel *loadingMessage;
	UIImageView *loadingImagesView;
}

@end

@implementation JVHelperTools

-(BOOL) textIsValidEmail:(NSString *)checkString
{
	BOOL stricterFilter = YES; // Discussion http://blog.logichigh.com/2010/09/02/validating-an-e-mail-address/
	NSString *stricterFilterString = @"[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}";
	NSString *laxString = @".+@([A-Za-z0-9]+\\.)+[A-Za-z]{2}[A-Za-z]*";
	NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
	NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
	return [emailTest evaluateWithObject:checkString] && ![self textFieldInputIsWhiteSpace:checkString];
}

-(void) addProgressviewToSuperview: (UIView *)superView;	{
	CGRect transBGFrame = CGRectMake(0, (superView.bounds.size.height/2) - (kLoadingBGHeight/2), superView.bounds.size.width, kLoadingBGHeight);
	
	NSMutableArray *loadingImages = [NSMutableArray new];
	[loadingImages addObject:[UIImage imageNamed:@"general_loading_01.png"]];
	[loadingImages addObject:[UIImage imageNamed:@"general_loading_02.png"]];
	
	CGRect loadingImagesFrame = CGRectMake((transBGFrame.size.width/2) - (kLoadingImageWidth/2), (kLoadingBGHeight/2) - (kLoadinImageHeight/2) , kLoadingImageWidth, kLoadinImageHeight);
	
	loadingImagesView = [[UIImageView alloc]initWithFrame:loadingImagesFrame];
	loadingImagesView.animationImages = loadingImages;
	loadingImagesView.animationDuration = 1.0f;
	loadingImagesView.alpha = 0.0f;
	
	if (SYSTEM_VERSION_LESS_THAN(@"7.0")) {
		transView = [[UIView alloc]initWithFrame:transBGFrame];
		transView.backgroundColor = [UIColor whiteColor];
		transView.alpha = 0.0f;
		[transView addSubview:loadingImagesView];
	} else {
		blurView = [[UIERealTimeBlurView alloc]initWithFrame:transBGFrame];
		blurView.tintColor = [UIColor colorWithRed:200.0/255.0 green:200.0/255.0 blue:200.0/255.0 alpha:1];
		blurView.alpha = 0.0f;
		[blurView addSubview:loadingImagesView];
	}
	
//	CGRect loadingMessageFrame = CGRectMake(0, loadingImagesView.frame.origin.y + 90.0, screen.bounds.size.width, 20.0);
//	loadingMessage = [[UILabel alloc]initWithFrame:loadingMessageFrame];
//	loadingMessage.text = @"One Moment Please";
//	loadingMessage.backgroundColor = [UIColor clearColor];
//	[loadingMessage setFont:[UIFont fontWithName:@"GillSans" size:14.0]];
//	[loadingMessage setTextColor:[UIColor colorWithRed:155.0/255.0 green:50.0/255.0 blue:50.0/255.0 alpha:1]];
//	[loadingMessage setTextAlignment:NSTextAlignmentCenter];
//	loadingMessage.alpha = 0.0;
	
	if (SYSTEM_VERSION_LESS_THAN(@"7.0")) {
		[superView addSubview:transView];
		[UIView animateWithDuration:0.15 delay:0 options:UIViewAnimationOptionCurveLinear animations:^{
			transView.alpha = 0.9f;
			loadingImagesView.alpha = 1.0f;
		} completion:nil];
	} else {
		[superView addSubview:blurView];
		[UIView animateWithDuration:0.15 delay:0 options:UIViewAnimationOptionCurveLinear animations:^{
			blurView.alpha = 1.0f;
			loadingImagesView.alpha = 1.0f;
		} completion:nil];
	}
//	[superView addSubview:loadingImagesView];
	[loadingImagesView startAnimating];
	
}

-(void) removeProgressViewFromView: (UIView *)superView {
	
	if (SYSTEM_VERSION_LESS_THAN(@"7.0")) {
		[UIView animateWithDuration:0.15 delay:0 options:UIViewAnimationOptionCurveLinear animations:^{
//			loadingImagesView.alpha = 0.0f;
			transView.alpha = 0.0f;
		} completion:^(BOOL finished) {
			if (finished) {
//				for (UIImageView *im in superView.subviews) {
//					if (im == loadingImagesView) {
//						NSLog(@"removed loadingImagesView");
//						[loadingImagesView removeFromSuperview];
//					}
//				}
				
				for (UIView *v in superView.subviews) {
					if (v == transView) {
						[transView removeFromSuperview];
					}
				}
			}
		}];
		
	} else {
		[UIView animateWithDuration:0.15 delay:0 options:UIViewAnimationOptionCurveLinear animations:^{
//			loadingImagesView.alpha = 0.0f;
			blurView.alpha = 0.0f;
		} completion:^(BOOL finished) {
			if (finished) {
//				for (UIImageView *im in superView.subviews) {
//					if (im == loadingImagesView) {
//						NSLog(@"removed loadingImagesView");
//						[loadingImagesView removeFromSuperview];
//					}
//				}
				
				for (UIView *v in superView.subviews) {
					if (v == blurView) {
						[blurView removeFromSuperview];
					}
				}
			}
		}];
		
	}
		
}

- (UIImage *)imageFromColor:(UIColor *)color {
    CGRect rect = CGRectMake(0, 0, 1, 1);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return [img stretchableImageWithLeftCapWidth:0 topCapHeight:0];
}

- (UIColor *)getRedButtonNormalColor {
	return [UIColor colorWithRed:155.0/255.0 green:50.0/255.0 blue:50.0/255.0 alpha:1];
}

- (UIColor *)getOrangeButtonNormalColor {
	return [UIColor colorWithRed:1.0 green:114.0/255.0 blue:45.0/255.0 alpha:1];
}

- (UIColor *)getRedButtonHighlightedColor {
	return [UIColor colorWithRed:129.0/255.0 green:42.0/255.0 blue:42.0/255.0 alpha:1.0];
}

- (UIColor *)getOrangeButtonHighlightedColor {
	return [UIColor colorWithRed:230.0/255.0 green:103.0/255.0 blue:41.0/255.0 alpha:1.0];
}

- (UIColor *)getButtonHighlightedTextColor {
	return [UIColor colorWithRed:198.0/255.0 green:198.0/255.0 blue:198.0/255.0 alpha:1];
}

-(BOOL) textFieldInputIsWhiteSpace: (NSString *)text {
	NSString *rawText = text;
	NSCharacterSet *whitespace = [NSCharacterSet whitespaceAndNewlineCharacterSet];
	
	NSString *rawTrimmed = [rawText stringByTrimmingCharactersInSet:whitespace];
	
	if ([rawTrimmed length] == 0) {
		return true;
	} else {
		return false;
	}
}

-(BOOL) internetAccessIsAvailable {
	Reachability *internetChecker = [Reachability reachabilityForInternetConnection];
	NetworkStatus status = [internetChecker currentReachabilityStatus];
	
	if (status == ReachableViaWiFi || status == ReachableViaWWAN) {
		return true;
	} else {
		return false;
	}
}

- (UIImage *)imageFromView: (UIView *)view {
	//Get the size of the screen
//	CGRect screenRect = [[UIScreen mainScreen] bounds];
	
	//Create a bitmap-based graphics context and make
	//it the current context passing in the view size
	UIGraphicsBeginImageContext(view.bounds.size);
	
	CGContextRef ctx = UIGraphicsGetCurrentContext();
	[[UIColor blackColor] set];
	CGContextFillRect(ctx, view.bounds);
	
	//render the receiver and its sublayers into the specified context
	//choose a view or use the window to get a screenshot of the
	//entire device
	[view.layer renderInContext:ctx];
	
	UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
	
	//End the bitmap-based graphics context
	UIGraphicsEndImageContext();
	
	//Save UIImage to camera roll
//	UIImageWriteToSavedPhotosAlbum(newImage, nil, nil, nil);
	return newImage;
}

-(void)showAlertViewForReason:(enum UIAlertViewReason) reason {
	UIAlertView *JVHTAlert;
	switch (reason) {
		case JVHTNoInternetConnection:
			JVHTAlert = [[UIAlertView alloc]
						  initWithTitle:@"No Internet Connection"
						  message:@"You need an internet connection to do that."
						  delegate:self
						  cancelButtonTitle:@"Ok"
						  otherButtonTitles: nil];
			
			JVHTAlert.accessibilityLabel = @"noInternetConnection";
			[JVHTAlert show];
			break;
		
		case JVHTEmailTaken:
			JVHTAlert = [[UIAlertView alloc]
						  initWithTitle:@"Email Already Used"
						  message:@"That email address is already registered. Did you want to login?"
						  delegate:self
						  cancelButtonTitle:@"Cancel"
						  otherButtonTitles:@"Login", nil];
			
			JVHTAlert.accessibilityLabel = @"loginEmailTaken";
			[JVHTAlert show];
			break;
			
		case JVHTInvalidLoginCredentials:
			JVHTAlert = [[UIAlertView alloc]
						  initWithTitle:@"Login Unsuccessful"
						  message:@"The login credentials you entered were invalid. Did you forget your password?"
						  delegate:self cancelButtonTitle:@"Retry"
						  otherButtonTitles:@"Yes", nil];
			
			JVHTAlert.accessibilityLabel = @"loginInvalid";
			[JVHTAlert show];
			break;
			
		case JVHTPasswordResetEmailSent:
			JVHTAlert = [[UIAlertView alloc]
						  initWithTitle:@"Email Sent"
						  message:@"Thanks! Check your email, click the link, and change your password. Then come back and sign in."
						  delegate:self cancelButtonTitle:@"Ok"
						  otherButtonTitles:nil];
			
			JVHTAlert.accessibilityLabel = @"passwordResetEmailSent";
			[JVHTAlert show];
			break;
			
		case JVHTEmailVerificationSent:
			JVHTAlert = [[UIAlertView alloc]
						 initWithTitle:@"Email Sent"
						 message:@"Check your email and click the link. Then come back make your first call!"
						 delegate:self cancelButtonTitle:@"Ok"
						 otherButtonTitles:nil];
			
			JVHTAlert.accessibilityLabel = @"emailVerificationSent";
			[JVHTAlert show];
			break;
			
		case JVHTEmailNotFound:
			JVHTAlert = [[UIAlertView alloc]
						  initWithTitle:@"Email Not Found"
						  message:@"Sorry that email is not registered. Did you want to Sign Up?"
						  delegate:self cancelButtonTitle:@"Retry"
						  otherButtonTitles:@"Sign Up", nil];
			
			JVHTAlert.accessibilityLabel = @"emailNotFound";
			[JVHTAlert show];
			break;
		
		case JVHTGeneralError:
			JVHTAlert = [[UIAlertView alloc]
						  initWithTitle:@"Error!"
						  message:@"Iffy is experiencing some problems back home. Please try again in a little bit."
						  delegate:self cancelButtonTitle:@"Ok"
						  otherButtonTitles:nil];
			
			JVHTAlert.accessibilityLabel = @"generalError";
			[JVHTAlert show];
			break;
			
		default:
			break;
	}
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
	if ([alertView.accessibilityLabel isEqualToString:@"loginEmailTaken"]) {
		if (buttonIndex == 0) {
			NSLog(@"Cancel PRESSED!");
		} else {
			NSLog(@"Login!");
			[self.delegate animateLoginFromAlert];
		}
	}
	
	if ([alertView.accessibilityLabel isEqualToString:@"loginInvalid"]) {
		if (buttonIndex == 0) {
			NSLog(@"RETRY/OK PRESSED!");
		} else {
			NSLog(@"Forgot Password huh?!");
			[self.delegate animateForgotPasswordFromAlert];
		}
	}
	
	if ([alertView.accessibilityLabel isEqualToString:@"emailNotFound"]) {
		NSLog(@"emailNotFound");
		if (buttonIndex == 1) {
			NSLog(@"Sign Up!");
			
			[self.delegate animateSignUpFromAlert];
		}
	}
	
}

@end
