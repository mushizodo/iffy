//
//  ProfileNavigationController.h
//  Iffy
//
//  Created by Joseph Ayo-Vaughan on 1/25/14.
//  Copyright (c) 2014 Joseph Ayo-Vaughan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProfileNavigationController : UINavigationController

@property (strong, nonatomic) UIImage *profilePhoto;

@end
