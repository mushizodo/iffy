//
//  JVProfileViewController.m
//  Iffy
//
//  Created by Joseph Ayo-Vaughan on 3/5/14.
//  Copyright (c) 2014 Joseph Ayo-Vaughan. All rights reserved.
//

#import "JVProfileViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "JVHelperTools.h"
#import "SearchTableViewCell.h"
#import "ProfileUserInfoSpaceCell.h"

#define kUsersChosen @"User"
#define kPlacesChosen @"Places"
#define kTopicsChosen @"Topics"

@interface JVProfileViewController () {
	JVHelperTools *jvht;
	UISearchBar *sb;
	UISearchDisplayController *sdc;
	UITableViewController *tvc;
	UIView *tableLoadingView;
}

@end

@implementation JVProfileViewController

- (id)initWithCoder:(NSCoder *)aDecoder {
	self = [super initWithCoder:aDecoder];
    if (self) {
        // Custom the table
        
        // The className to query on
		self.parseClassName = kUsersChosen;
        
        // The key of the PFObject to display in the label of the default cell style
//        self.textKey = @"text";
        
        // Uncomment the following line to specify the key of a PFFile on the PFObject to display in the imageView of the default cell style
//        self.imageKey = @"image";
        
        // Whether the built-in pull-to-refresh is enabled
        self.pullToRefreshEnabled = YES;
        
        // Whether the built-in pagination is enabled
        self.paginationEnabled = YES;
        
        // The number of objects to show per page
        self.objectsPerPage = 25;
		
    }
    return self;
}

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}


#pragma mark - UIViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
	
	jvht = [JVHelperTools new];
	
	sb = [[UISearchBar alloc]initWithFrame:CGRectMake(0, -44, 320, 44)];
	sb.delegate = self;
	sb.showsScopeBar = NO;
	[sb setScopeButtonTitles:@[@"People", @"Places", @"Topics"]];
	[sb setKeyboardType:UIKeyboardTypeDefault];
	[sb setPlaceholder:@"Search Iffy..."];
	[sb setAutocapitalizationType:UITextAutocapitalizationTypeNone];
	[sb setAutocorrectionType:UITextAutocorrectionTypeNo];
	[self.navigationController.view insertSubview:sb belowSubview:self.navigationController.navigationBar];
	
	sdc = [[UISearchDisplayController alloc]initWithSearchBar:sb contentsController:self];
	[sdc setSearchResultsDataSource:self];
	[sdc setSearchResultsDelegate:self];
	
	tvc = [UITableViewController new];
	tvc.tableView = sdc.searchResultsTableView;
	UIRefreshControl *refreshControl = [[UIRefreshControl alloc]init];
	[refreshControl setTintColor:[jvht getOrangeButtonNormalColor]];
	[refreshControl addTarget:self action:@selector(refreshSearchTableView) forControlEvents:UIControlEventValueChanged];
	tvc.refreshControl = refreshControl;
	
	if (is5thGen) {
		tableLoadingView = [[UIView alloc]initWithFrame:CGRectMake(0, 200, 320, 75)];
	} else {
		tableLoadingView = [[UIView alloc]initWithFrame:CGRectMake(0, 100, 320, 75)];
	}
	
	[jvht addProgressviewToSuperview:tableLoadingView];
	
	
	[sdc.searchResultsTableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
	
	[sdc.searchBar setScopeBarButtonTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[jvht getRedButtonNormalColor], UITextAttributeTextColor, [UIFont fontWithName:@"GillSans-Light" size:15], UITextAttributeFont, [UIColor clearColor], UITextAttributeTextShadowColor, nil] forState:UIControlStateNormal];
	
	[sdc.searchBar setScopeBarButtonTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[jvht getButtonHighlightedTextColor], UITextAttributeTextColor, [UIFont fontWithName:@"GillSans-Light" size:15], UITextAttributeFont, [UIColor clearColor], UITextAttributeTextShadowColor, nil] forState:UIControlStateHighlighted];
	
	[sdc.searchBar setScopeBarButtonTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor whiteColor], UITextAttributeTextColor, [UIFont fontWithName:@"GillSans-Light" size:15], UITextAttributeFont, [UIColor clearColor], UITextAttributeTextShadowColor, nil] forState:UIControlStateSelected];
	
	[sdc.searchBar setTintColor:[jvht getRedButtonHighlightedColor]];
	
	UIBarButtonItem *settingsButton = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"settings.png"] style:UIBarButtonItemStylePlain target:self action:nil];
	UIBarButtonItem *contactsButton = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"contacts.png"] style:UIBarButtonItemStylePlain target:self action:nil];
	
	self.navigationItem.leftBarButtonItem = contactsButton;
	self.navigationItem.rightBarButtonItem = settingsButton;
	
	[sdc.searchResultsTableView registerNib:[UINib nibWithNibName:@"SearchTableViewCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"JVCell"];
		
	[self.tableView registerNib:[UINib nibWithNibName:@"ProfileUserInfoSpaceCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"UserInfoSpace"];
}

- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
	[self.tableView reloadData];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}


#pragma mark - PFQueryTableViewController

- (void)objectsWillLoad {
    [super objectsWillLoad];
    
    // This method is called before a PFQuery is fired to get more objects
}

- (void)objectsDidLoad:(NSError *)error {
    [super objectsDidLoad:error];
    
    // This method is called every time objects are loaded from Parse via the PFQuery
}


// Override to customize what kind of query to perform on the class. The default is to query for
// all objects ordered by createdAt descending.
- (PFQuery *)queryForTable {
	NSLog(@"loppitylop");
	PFQuery *query = [PFUser query];

//	If Pull To Refresh is enabled, query against the network by default.
	if (self.pullToRefreshEnabled) {
		query.cachePolicy = kPFCachePolicyNetworkOnly;
	}

//	If no objects are loaded in memory, we look to the cache first to fill the table
//	and then subsequently do a query against the network.
	if (self.objects.count == 0) {
		query.cachePolicy = kPFCachePolicyCacheThenNetwork;
	}
	
//	query.limit = 7;
//	[query whereKey:@"name" hasPrefix:sb.text];
//	[query whereKey:@"canIndex" equalTo:[NSNumber numberWithBool:YES]];
	[query orderByDescending:@"createdAt"];
	[query whereKey:@"objectId" equalTo:[PFUser currentUser].objectId];
	
//	[query countObjectsInBackgroundWithBlock:^(int number, NSError *error) {
//		if (!error) {
//			NSLog(@"number of objects: %d", number);
//		} else {
//			NSLog(@"Error!");
//		}
//	}];

	return query;
}



 // Override to customize the look of a cell representing an object. The default is to display
 // a UITableViewCellStyleDefault style cell with the label being the textKey in the object,
 // and the imageView being the imageKey in the object.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath object:(PFObject *)object {
	
	NSLog(@"object: %@", object);
	
	if (tableView == sdc.searchResultsTableView) {
		static NSString *CellIdentifier = @"JVCell";
		
		SearchTableViewCell *cell = (SearchTableViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
		
		if (!cell) {
			NSLog(@"cell is nil");
			NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"SearchTableViewCell" owner:self options:nil];
			cell = [nib objectAtIndex:0];
		}
		
		cell.userImageView.layer.masksToBounds = YES;
		cell.userImageView.layer.cornerRadius = cell.userImageView.frame.size.height/2;
		
		[tableLoadingView removeFromSuperview];
		
		PFUser *user = (PFUser *)object;
		cell.userNameLabel.text = user[@"name"];
		cell.userInfoLabel.text = user[@"email"];
		
		if (indexPath.row % 2 == 0) {
			cell.determinedFwdBgView.backgroundColor = [jvht getRedButtonNormalColor];
		} else {
			cell.determinedFwdBgView.backgroundColor = [jvht getOrangeButtonNormalColor];
		}
		
		if (user[@"photo_small"] != nil) {
			PFFile *imageFile = user[@"photo_small"];
			[imageFile getDataInBackgroundWithBlock:^(NSData *data, NSError *error) {
				if (!error) {
					NSLog(@"image!!");
					cell.userImageView.image = [UIImage imageWithData:data];
				}
			}];
		} else {
			cell.userImageView.image = [UIImage imageNamed:@"default_photo.png"];
		}
		[tvc.refreshControl endRefreshing];
//		cell.textLabel.text = [object objectForKey:self.textKey];
//		cell.imageView.file = [object objectForKey:self.imageKey];
		return cell;
	} else {
		NSLog(@"should use custom cell");
		static NSString *CellIdentifier = @"UserInfoSpace";
		
		ProfileUserInfoSpaceCell *cell = (ProfileUserInfoSpaceCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
		
		if (!cell) {
			NSLog(@"cell is nil");
			NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"ProfileUserInfoSpaceCell" owner:self options:nil];
			cell = [nib objectAtIndex:0];
		}
		
		cell.profilePhotoView.layer.masksToBounds = YES;
		cell.profilePhotoView.layer.cornerRadius = cell.profilePhotoView.frame.size.height/2;
		
		cell.profilePhotoViewBG.alpha = 0.5;
		cell.profilePhotoViewBG.layer.borderColor = [UIColor colorWithRed:155.0/255.0 green:50.0/255.0 blue:50.0/255.0 alpha:0.8].CGColor;
		//	cell.profilePhotoViewBG.layer.borderWidth = 1.0;
		cell.profilePhotoViewBG.layer.cornerRadius = cell.profilePhotoViewBG.frame.size.height/2;
		
		cell.viewPhotoButton.layer.borderColor = [UIColor colorWithRed:40.0/255.0 green:40.0/255.0 blue:40.0/255.0 alpha:0.2].CGColor;
		//	cell.viewPhotoButton.layer.borderWidth = 1.0;
		cell.viewPhotoButton.layer.cornerRadius = cell.viewPhotoButton.frame.size.height/2;
		
		cell.nameLabel.text = [PFUser currentUser][@"name"];
		
		NSString *docDir = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)[0];
		NSString *smallImagePath = [docDir stringByAppendingPathComponent:@"user.jpg"];
		
		if ([[NSFileManager defaultManager]fileExistsAtPath:smallImagePath]) {
			UIImage *profImage = [UIImage imageWithContentsOfFile:smallImagePath];
			
			cell.profilePhotoView.image = profImage;
			
		}else if ([PFUser currentUser][@"photo_small"] != nil) {
			
			PFFile *imageFile = [PFUser currentUser][@"photo_small"];
			
			[imageFile getDataInBackgroundWithBlock:^(NSData *data, NSError *error) {
				if (!error) {
					cell.profilePhotoView.image = [UIImage imageWithData:data];
				} else {
					cell.profilePhotoView.image = [UIImage imageNamed:@"default_photo.png"];
				}
			}];
			
		}else {
			cell.profilePhotoView.image = [UIImage imageNamed:@"default_photo.png"];
		}
		
		[cell.viewPhotoButton addTarget:self action:@selector(viewProfilePic:) forControlEvents:UIControlEventTouchUpInside];
		[self.refreshControl endRefreshing];
		return cell;
	}
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
//	if (tableView == sdc.searchResultsTableView) {
//		return 1;
//	} else {
//		NSLog(@"gogogo: %d", self.objects.count);
//		return self.objects.count;
//	}
	
	if (tableView == self.tableView) {
		NSLog(@"gogogo: %lu", (unsigned long)self.objects.count);
		return self.objects.count;
	} else {
		return 1;
	}
	
}

/*
 // Override if you need to change the ordering of objects in the table.
 - (PFObject *)objectAtIndex:(NSIndexPath *)indexPath {
 return [self.objects objectAtIndex:indexPath.row];
 }
 */

/*
 // Override to customize the look of the cell that allows the user to load the next page of objects.
 // The default implementation is a UITableViewCellStyleDefault cell with simple labels.
 - (UITableViewCell *)tableView:(UITableView *)tableView cellForNextPageAtIndexPath:(NSIndexPath *)indexPath {
 static NSString *CellIdentifier = @"NextPage";
 
 UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
 
 if (cell == nil) {
 cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
 }
 
 cell.selectionStyle = UITableViewCellSelectionStyleNone;
 cell.textLabel.text = @"Load more...";
 
 return cell;
 }
 */

#pragma mark - UITableViewDataSource

/*
 // Override to support conditional editing of the table view.
 - (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
 // Return NO if you do not want the specified item to be editable.
 return YES;
 }
 */

/*
 // Override to support editing the table view.
 - (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
 if (editingStyle == UITableViewCellEditingStyleDelete) {
 // Delete the object from Parse and reload the table view
 } else if (editingStyle == UITableViewCellEditingStyleInsert) {
 // Create a new instance of the appropriate class, and save it to Parse
 }
 }
 */

/*
 // Override to support rearranging the table view.
 - (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
 }
 */

/*
 // Override to support conditional rearranging of the table view.
 - (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
 // Return NO if you do not want the item to be re-orderable.
 return YES;
 }
 */

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [super tableView:tableView didSelectRowAtIndexPath:indexPath];
}

- (IBAction)viewProfilePic:(UIButton *)sender {
	[self performSegueWithIdentifier:@"toPhotoView" sender:self];
}

- (void)refreshSearchTableView {
	[self beginQueriesWithSearchText:sdc.searchBar.text];
}

- (void)refreshMainTableView {
	[self.tableView reloadData];
}

- (void)beginQueriesWithSearchText: (NSString *)searchText {
	
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
	[self beginQueriesWithSearchText:searchText];
}

- (BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchString:(NSString *)searchString {
	return YES;
}

- (void)searchBar:(UISearchBar *)searchBar selectedScopeButtonIndexDidChange:(NSInteger)selectedScope {
	NSLog(@"changed");
	if (selectedScope == 0) {
		self.parseClassName = kUsersChosen;
		[sdc.searchBar setTintColor:[jvht getRedButtonNormalColor]];
		[sdc.searchBar setScopeBarButtonTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[jvht getRedButtonNormalColor], UITextAttributeTextColor, [UIFont fontWithName:@"GillSans-Light" size:15], UITextAttributeFont, [UIColor clearColor], UITextAttributeTextShadowColor, nil] forState:UIControlStateNormal];
	} else if (selectedScope == 1) {
		self.parseClassName = kPlacesChosen;
		[sdc.searchBar setTintColor:[UIColor colorWithRed:40.0/255.0 green:40.0/255.0 blue:40.0/255.0 alpha:1.0]];
		[sdc.searchBar setScopeBarButtonTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor colorWithRed:40.0/255.0 green:40.0/255.0 blue:40.0/255.0 alpha:1.0], UITextAttributeTextColor, [UIFont fontWithName:@"GillSans-Light" size:15], UITextAttributeFont, [UIColor clearColor], UITextAttributeTextShadowColor, nil] forState:UIControlStateNormal];
	} else {
		self.parseClassName = kTopicsChosen;
		[sdc.searchBar setTintColor:[jvht getOrangeButtonNormalColor]];
		[sdc.searchBar setScopeBarButtonTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[jvht getOrangeButtonNormalColor], UITextAttributeTextColor, [UIFont fontWithName:@"GillSans-Light" size:15], UITextAttributeFont, [UIColor clearColor], UITextAttributeTextShadowColor, nil] forState:UIControlStateNormal];
	}
	
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
	[self beginQueriesWithSearchText:searchBar.text];
}

- (BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchScope:(NSInteger)searchOption {
	return YES;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == self.tableView) {
		if (indexPath.row == 0) {
			return 190;
		} else {
			return 60;
		}
	} else {
		return 60;
	}
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
	//	NSLog(@"%f", scrollView.contentOffset.y);
	
	if (scrollView.contentOffset.y < -240) {
		[scrollView setContentOffset:CGPointMake(scrollView.contentOffset.x, -240)];
	}
}

@end
