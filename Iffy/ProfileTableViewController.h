//
//  ProfileTableViewController.h
//  Iffy
//
//  Created by Joseph Ayo-Vaughan on 1/31/14.
//  Copyright (c) 2014 Joseph Ayo-Vaughan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProfileTableViewController : UITableViewController <UISearchBarDelegate, UISearchDisplayDelegate, UITableViewDelegate, UITableViewDataSource, UIScrollViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *mainTableView;

- (IBAction)viewProfilePic:(UIButton *)sender;
- (void)goToSearch;
- (void)beginQueriesWithSearchText: (NSString *)searchText;
- (void)slowDownScrollWithGestureRecognizer: (UIPanGestureRecognizer *)recognizer;
- (void)refreshSearchTableView;
- (void)refreshMainTableView;
- (void)viewFollowers:(id)sender;
- (void)viewFollowing:(id)sender;
@end
