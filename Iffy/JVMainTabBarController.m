//
//  JVMainTabBarController.m
//  Iffy
//
//  Created by Joseph Ayo-Vaughan on 4/25/14.
//  Copyright (c) 2014 Joseph Ayo-Vaughan. All rights reserved.
//

#import "JVMainTabBarController.h"
#import "IncomingCallViewController.h"
#import "JVHelperTools.h"
#import <Parse/Parse.h>

@interface JVMainTabBarController () {
	JVHelperTools *jvht;
}

@end

@implementation JVMainTabBarController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	jvht = [JVHelperTools new];
	
	[[UIApplication sharedApplication]setStatusBarStyle:UIStatusBarStyleLightContent animated:YES];
	[[UIApplication sharedApplication]setStatusBarHidden:NO];
	
	[[UINavigationBar appearance] setBackgroundImage:[jvht imageFromColor:[jvht getRedButtonNormalColor]] forBarMetrics:UIBarMetricsDefault];
	[[UINavigationBar appearance] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor whiteColor], UITextAttributeTextColor, [UIFont fontWithName:@"GillSans-Light" size:20], UITextAttributeFont, [UIColor clearColor], UITextAttributeTextShadowColor, nil]];
	
	[[UINavigationBar appearance] setShadowImage:[[UIImage alloc]init]];
	
	[[UIBarButtonItem appearance] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor whiteColor], UITextAttributeTextColor, [UIFont fontWithName:@"GillSans-Light" size:20], UITextAttributeFont, [UIColor clearColor], UITextAttributeTextShadowColor, nil] forState:UIControlStateNormal];
	
	[[UIBarButtonItem appearance] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor colorWithWhite:0.8 alpha:0.8], UITextAttributeTextColor, [UIFont fontWithName:@"GillSans-Light" size:20], UITextAttributeFont, [UIColor clearColor], UITextAttributeTextShadowColor, nil] forState:UIControlStateHighlighted];
	
	if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) {
		[[UINavigationBar appearance] setTintColor:[UIColor whiteColor]];
	} else {
		[[UINavigationBar appearance] setTintColor:[jvht getRedButtonNormalColor]];
		[[UIBarButtonItem appearance] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor whiteColor], UITextAttributeTextColor, [UIFont fontWithName:@"GillSans-Light" size:15], UITextAttributeFont, [UIColor clearColor], UITextAttributeTextShadowColor, nil] forState:UIControlStateNormal];
		
		[[UIBarButtonItem appearance] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor colorWithWhite:0.8 alpha:0.8], UITextAttributeTextColor, [UIFont fontWithName:@"GillSans-Light" size:15], UITextAttributeFont, [UIColor clearColor], UITextAttributeTextShadowColor, nil] forState:UIControlStateHighlighted];
	}
	
	//	[[UIBarButtonItem appearance] setTintColor:[UIColor whiteColor]];
	[self updateTabBarForWhiteBackground];
	
	NSString *userId = [[PFUser currentUser]objectId];
	
	self.client = [Sinch clientWithApplicationKey:kRebtelApplicationKey
                                    applicationSecret:kRebtelApplicationSecret
                                      environmentHost:kRebtelApplicationEnviromentHost
                                               userId:userId];
	
	[self.client setDelegate:self];
	[self.client setSupportActiveConnectionInBackground:YES];
	[self.client setSupportPushNotifications:YES];
	// Start the Rebtel Client
	[self.client start];
	// Start listening for incoming calls on the active connection.
	[self.client startListeningOnActiveConnection];
	
	NSString *docDir = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)[0];
	NSString *smallImagePath = [docDir stringByAppendingPathComponent:@"user.jpg"];
	NSString *ogImagePath = [docDir stringByAppendingPathComponent:@"user_og.jpg"];
	
	if ([[NSFileManager defaultManager]fileExistsAtPath:docDir]) {
		NSLog(@"existaaaaaa!");
		NSData *imageData = [NSData dataWithContentsOfFile:smallImagePath];
		NSData *ogImageData = [NSData dataWithContentsOfFile:ogImagePath];
		
		PFFile *imageFile = [PFFile fileWithName:@"user.jpg" data:imageData];
		PFFile *ogImageFile = [PFFile fileWithName:@"ogphoto.jpg" data:ogImageData];
		
		[imageFile saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
			if (!error) {
				PFUser *user = [PFUser currentUser];
				user[@"photo_small"] = imageFile;
				[user saveInBackground];
				NSLog(@"saved user small_image");
			}
		}];
		
		[ogImageFile saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
			if (!error) {
				PFUser *user = [PFUser currentUser];
				user[@"ogPhoto"] = ogImageFile;
				[user saveEventually];
				NSLog(@"saved user ogimage");
			}
		}];
		
	}
	
}

- (void)updateTabBarForWhiteBackground {
	[self.tabBar setBackgroundImage:[UIImage imageNamed:@"tabbar_bg.png"]];
	[self.tabBar setSelectionIndicatorImage:[UIImage imageNamed:@"brows_01.png"]];
	[self.tabBar setShadowImage:[UIImage imageNamed:@"brows_01.png"]];
	
	[[UITabBarItem appearance] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor colorWithWhite:0.4 alpha:1.0], UITextAttributeTextColor, [UIFont fontWithName:@"GillSans" size:10.0], UITextAttributeFont, nil] forState:UIControlStateNormal];
	
	[[UITabBarItem appearance] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor colorWithRed:40.0/255.0 green:40.0/255.0 blue:40.0/255.0 alpha:1.0], UITextAttributeTextColor, nil] forState:UIControlStateSelected];
	
	UIImage *youImage = [UIImage imageNamed:@"tb_profile.png"];
	UIImage *youImageSelected = [UIImage imageNamed:@"tb_profile_s.png"];
	UITabBarItem *youItem = [self.tabBar.items objectAtIndex:0];
	[youItem setFinishedSelectedImage:youImageSelected withFinishedUnselectedImage:youImage];
	
	UIImage *notiImage = [UIImage imageNamed:@"tb_notif.png"];
	UIImage *notiImageSelected = [UIImage imageNamed:@"tb_notif_s.png"];
	UITabBarItem *notiItem = [self.tabBar.items objectAtIndex:1];
	[notiItem setFinishedSelectedImage:notiImageSelected withFinishedUnselectedImage:notiImage];
	
	UIImage *iffyTabImage = [UIImage imageNamed:@"tb_modes_s.png"];
	UIImage *iffyTabImageSelected = [UIImage imageNamed:@"tb_modes.png"];
	UITabBarItem *iffyTabItem = [self.tabBar.items objectAtIndex:2];
	[iffyTabItem setFinishedSelectedImage:iffyTabImageSelected withFinishedUnselectedImage:iffyTabImage];
	iffyTabItem.imageInsets = UIEdgeInsetsMake(6, 0, -6, 0);
	
	UIImage *openLinesImage = [UIImage imageNamed:@"tb_feed.png"];
	UIImage *openLinesImageSelected = [UIImage imageNamed:@"tb_feed_s.png"];
	UITabBarItem *openLinesItem = [self.tabBar.items objectAtIndex:3];
	[openLinesItem setFinishedSelectedImage:openLinesImageSelected withFinishedUnselectedImage:openLinesImage];
	
	UIImage *moreImage = [UIImage imageNamed:@"tb_messages.png"];
	UIImage *moreImageSelected = [UIImage imageNamed:@"tb_messages_s.png"];
	UITabBarItem *moreItem = [self.tabBar.items objectAtIndex:4];
	[moreItem setFinishedSelectedImage:moreImageSelected withFinishedUnselectedImage:moreImage];
}

#pragma mark - REBClientDelegate

- (void)client:(id<SINClient>)client didReceiveIncomingCall:(id<SINCall>)call {
	
    [self performSegueWithIdentifier:@"toReceiveCall" sender:call];
}

- (SINLocalNotification *)client:(id<SINClient>)client localNotificationForIncomingCallFromUser:(NSString *)remoteUserId {
    SINLocalNotification *notification = [[SINLocalNotification alloc] init];
    notification.alertAction = @"Answer";
    notification.alertBody = [NSString stringWithFormat:@"Incoming call from %@", remoteUserId];
    return notification;
}

- (void)clientDidStart:(id<SINClient>)client {
    NSLog(@"Rebtel SDK client started successfully (version: %@)", [Sinch version]);
}

- (void)clientDidStop:(id<SINClient>)client {
    NSLog(@"Rebtel SDK client stopped");
}

- (void)clientDidFail:(id<SINClient>)client error:(NSError *)error {
    NSLog(@"Error: %@", error.localizedDescription);
}

- (void)client:(id<SINClient>)client logMessage:(NSString *)message area:(NSString *)area severity:(SINLogSeverity)severity timestamp:(NSDate *)timestamp {
	
    if (severity == SINLogSeverityCritical) {
        NSLog(@"%@", message);
    }
}


- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
	
	if ([segue.identifier isEqualToString:@"toReceiveCall"]) {
		IncomingCallViewController *ic = segue.destinationViewController;
		ic.call = sender;
		ic.call.delegate = ic;
	}
	
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)prefersStatusBarHidden {
	return NO;
}

- (UIStatusBarStyle)preferredStatusBarStyle {
	return UIStatusBarStyleLightContent;
}

- (void)handleLocalNotification:(UILocalNotification *)notification {
	
    if (notification) {
		
        id<SINNotificationResult> result = [self.client relayLocalNotification:notification];
		
        if ([result isCall]) {
			UIAlertView *alert =
			[[UIAlertView alloc]
			 initWithTitle:@"Missed call"
			 message:[NSString stringWithFormat:@"Missed call from %@", [[result callResult] remoteUserId]]
			 delegate:nil
			 cancelButtonTitle:nil
			 otherButtonTitles:@"OK",
			 nil];
            [alert show];
        }
    }
}

@end
