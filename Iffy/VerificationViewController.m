//
//  StartupViewController.m
//  Iffy
//
//  Created by Joseph Ayo-Vaughan on 1/19/14.
//  Copyright (c) 2014 Joseph Ayo-Vaughan. All rights reserved.
//

#import "VerificationViewController.h"
#import <Parse/Parse.h>
#import "JVHelperTools.h"
#import "LoginViewController.h"

#define kScreenWidth [UIScreen mainScreen].bounds.size.width

@interface VerificationViewController () {
	JVHelperTools *jvht;
	PFUser *user;
	NSTimer *checkUser;
	UILabel *validityLabel;
	NSTimer *checkEmailVerifiedTimer;
}

@end

@implementation VerificationViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
	
	jvht = [JVHelperTools new];
	user = [PFUser currentUser];
	
	if ([PFUser currentUser] != nil && [jvht internetAccessIsAvailable]) {
		[[PFUser currentUser]refreshInBackgroundWithBlock:^(PFObject *object, NSError *error) {
			if (!error) {
				user = [PFUser currentUser];
			}
		}];
	}
	
	if (!is5thGen) {
//		self.facesImageView.frame = CGRectOffset(self.facesImageView.frame, 0, -44.0);
//		self.phoneImageView.frame = CGRectOffset(self.phoneImageView.frame, 0, -44.0);
//		self.browsImageView.frame = CGRectOffset(self.browsImageView.frame, 0, -44.0);
//		self.welcomeLabel.frame = CGRectOffset(self.welcomeLabel.frame, 0, -44.0);
//		self.determinedLabel.frame = CGRectOffset(self.determinedLabel.frame, 0, -44.0);
		
		for (UIView *view in self.view.subviews) {
			view.frame = CGRectOffset(view.frame, 0, -44.0);
		}
	}
	
	self.determinedLabel.alpha = 0.0;
	self.resendEmailButton.alpha = 0.0;
	self.checkEmalButton.alpha = 0.0;
	self.loginButton.alpha = 0.0;
	self.changeEmailButton.alpha = 0.0;
	self.didNotGetEmailButton.alpha = 0.0;
	
	[self.resendEmailButton setBackgroundImage:[jvht imageFromColor:[UIColor colorWithRed:94.0/255.0 green:94.0/255.0 blue:94.0/255.0 alpha:1.0]] forState:UIControlStateHighlighted];
	[self.resendEmailButton setBackgroundImage:[jvht imageFromColor:[UIColor colorWithRed:94.0/255.0 green:94.0/255.0 blue:94.0/255.0 alpha:1.0]] forState:UIControlStateDisabled];
	[self.resendEmailButton setTitleColor:[UIColor colorWithRed:155.0/255.0 green:155.0/255.0 blue:155.0/255.0 alpha:1.0] forState:UIControlStateHighlighted];
	[self.resendEmailButton setTitleColor:[UIColor colorWithRed:155.0/255.0 green:155.0/255.0 blue:155.0/255.0 alpha:1.0] forState:UIControlStateDisabled];
	[self.resendEmailButton setTitle:@"Resend Verification Email" forState:UIControlStateNormal];
	[self.resendEmailButton setTitle:@"Checking..." forState:UIControlStateDisabled];
	
	[self.checkEmalButton setBackgroundImage:[jvht imageFromColor:[jvht getRedButtonHighlightedColor]] forState:UIControlStateHighlighted];
	[self.checkEmalButton setBackgroundImage:[jvht imageFromColor:[jvht getRedButtonHighlightedColor]] forState:UIControlStateDisabled];
	[self.checkEmalButton setTitleColor:[jvht getButtonHighlightedTextColor] forState:UIControlStateHighlighted];
	[self.checkEmalButton setTitleColor:[jvht getButtonHighlightedTextColor] forState:UIControlStateDisabled];
	[self.checkEmalButton setTitle:@"Checking" forState:UIControlStateDisabled];
	
	[self.loginButton setBackgroundImage:[jvht imageFromColor:[jvht getOrangeButtonHighlightedColor]] forState:UIControlStateHighlighted];
	[self.loginButton setTitleColor:[jvht getButtonHighlightedTextColor] forState:UIControlStateHighlighted];
	[self.loginButton setTitleColor:[jvht getButtonHighlightedTextColor] forState:UIControlStateDisabled];
	
	[self.changeEmailButton setTitleColor:[UIColor blackColor] forState:UIControlStateHighlighted];
	[self.changeEmailButton setBackgroundImage:[jvht imageFromColor:[UIColor colorWithWhite:0.9 alpha:1.0]] forState:UIControlStateHighlighted];
	
	[self.didNotGetEmailButton setTitleColor:[jvht getButtonHighlightedTextColor] forState:UIControlStateHighlighted];
}

- (void)viewDidAppear:(BOOL)animated {
	[super viewDidAppear:animated];
	
//	[[UIApplication sharedApplication]setStatusBarStyle:UIStatusBarStyleDefault];
//	[[UIApplication sharedApplication]setStatusBarHidden:NO withAnimation:UIStatusBarAnimationSlide];
	
	NSMutableArray *browsImages = [NSMutableArray new];
	[browsImages addObject:[UIImage imageNamed:@"brows_01.png"]];
	[browsImages addObject:[UIImage imageNamed:@"brows_02.png"]];
	[browsImages addObject:[UIImage imageNamed:@"brows_03.png"]];
	[browsImages addObject:[UIImage imageNamed:@"brows_04.png"]];
	
	NSMutableArray *faceImages = [NSMutableArray new];
	[faceImages addObject:[UIImage imageNamed:@"startup_smileys_01.png"]];
	[faceImages addObject:[UIImage imageNamed:@"startup_smileys_02.png"]];
	
	NSMutableArray *phoneImages = [NSMutableArray new];
	[phoneImages addObject:[UIImage imageNamed:@"startup_phone_01.png"]];
	[phoneImages addObject:[UIImage imageNamed:@"startup_phone.png"]];
	[phoneImages addObject:[UIImage imageNamed:@"startup_phone_02.png"]];
	[phoneImages addObject:[UIImage imageNamed:@"startup_phone.png"]];
	
	self.browsImageView.animationImages = browsImages;
	self.browsImageView.animationDuration = 1.0;
	
	self.facesImageView.animationImages = faceImages;
	self.facesImageView.animationDuration = 1.0;
	
	self.phoneImageView.animationImages = phoneImages;
	self.phoneImageView.animationDuration = 1.0;
	
	[self.browsImageView startAnimating];
	[self.facesImageView startAnimating];
	[self.phoneImageView startAnimating];
	
	if ([jvht internetAccessIsAvailable]) {
		checkUser = [NSTimer scheduledTimerWithTimeInterval:4.0 target:self selector:@selector(checkForUser) userInfo:nil repeats:NO];
	} else {
		self.welcomeLabel.text = @"Please connect to the internet";
		CGFloat buttonWidth = 300.0;
		CGRect connectButtonFrame = CGRectMake(kScreenWidth/2 - buttonWidth/2, self.welcomeLabel.frame.origin.y + 40.0, buttonWidth, 40.0);
		UIButton *connectButton = [[UIButton alloc]initWithFrame:connectButtonFrame];
		[connectButton.titleLabel setFont:[UIFont fontWithName:@"GillSans-Light" size:15]];
		[connectButton setBackgroundColor:[jvht getRedButtonNormalColor]];
		
		[connectButton setTitle:@"Try Again" forState:UIControlStateNormal];
		[connectButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
		
		[connectButton setTitleColor:[UIColor colorWithWhite:0.6 alpha:1.0] forState: UIControlStateHighlighted];
		[connectButton setBackgroundImage:[jvht imageFromColor:[jvht getRedButtonHighlightedColor]] forState:UIControlStateHighlighted];
		
		[connectButton setTitleColor:[UIColor colorWithWhite:0.6 alpha:1.0] forState: UIControlStateDisabled];
		[connectButton setBackgroundImage:[jvht imageFromColor:[jvht getRedButtonHighlightedColor]] forState:UIControlStateDisabled];
		[connectButton setTitle:@"Trying.." forState:UIControlStateDisabled];
		
		[connectButton addTarget:self action:@selector(tryConnectingAgain:) forControlEvents:UIControlEventTouchUpInside];
		
		[connectButton setAdjustsImageWhenDisabled:YES];
		[connectButton setAdjustsImageWhenHighlighted:YES];
		[self.view addSubview:connectButton];
	}
}

- (IBAction)tryConnectingAgain:(UIButton *)sender {
	sender.enabled = NO;
	if ([jvht internetAccessIsAvailable]) {
		self.welcomeLabel.text = @"One Moment Please";
		sender.alpha = 0.0;
		sender.enabled = NO;
		[sender removeFromSuperview];
		[self checkForUser];
	} else {
		sender.enabled = YES;
		[jvht showAlertViewForReason:JVHTNoInternetConnection];
	}
}

#pragma mark -
#pragma mark Check For User
- (void)checkForUser {
	[checkUser invalidate];
	checkUser = nil;
	
	[user refresh];
	
	if (user != nil && user.email != nil) { //TODO: check if user or user.email can be compared if nil
		PFQuery *query = [PFUser query];
		[query whereKey:@"email" equalTo:user.email];
		
		if ([[query findObjects]count] != 0) {
			NSLog(@"%@. %@", user[@"name"], user.email);
			NSLog(@"%lu", (unsigned long)[[query findObjects] count]);
			if ([user[@"emailVerified"]boolValue] == YES) {
				self.welcomeLabel.text = @"Welcome back";
				self.determinedLabel.text = user[@"name"];
				self.determinedLabel.alpha = 1.0;
				
				double delayInSeconds = 1.5;
				dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
				dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
					NSLog(@"TO THE HOME SCREEN...mobile?...nanananana");
					[self performSegueWithIdentifier:@"toHomeView" sender:self];
				});
			} else {
				self.welcomeLabel.text = @"Please verify your email address:";
				self.determinedLabel.text = user.email;
				self.determinedLabel.alpha = 1.0;
				
				self.resendEmailButton.enabled = YES;
				self.loginButton.enabled = YES;
				self.checkEmalButton.enabled = YES;
				self.changeEmailButton.enabled = YES;
				self.didNotGetEmailButton.enabled = YES;
				
				for (UIView *view in self.view.subviews) {
					[UIView animateWithDuration:1.0 delay:1.0 options:UIViewAnimationOptionCurveEaseOut animations:^{
						view.frame = CGRectOffset(view.frame, 0, -70.0);
						self.resendEmailButton.alpha = 1.0;
						self.checkEmalButton.alpha = 1.0;
						self.loginButton.alpha = 1.0;
						self.changeEmailButton.alpha = 1.0;
						self.didNotGetEmailButton.alpha = 1.0;
					} completion:^(BOOL finished) {
						//						if (finished) {
						//							[UIView animateWithDuration:1.0 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
						//								self.didNotGetEmailButton.frame = CGRectOffset(self.didNotGetEmailButton.frame, 0, -70.0);
						//								self.changeEmailButton.frame = CGRectOffset(self.changeEmailButton.frame, 0, -70.0);
						//
						//							} completion:^(BOOL finished) {
						//
						//							}];
						//						}
					}];
				}
				
			}
		}else {
			NSLog(@"nope. no user info");
			self.welcomeLabel.text = @"Welcome!";
			
			double delayInSeconds = 1.5;
			dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
			dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
				[self performSegueWithIdentifier:@"toLoginView" sender:self];
			});
			
		}
	} else {
		NSLog(@"Nope. No current user");
		self.welcomeLabel.text = @"Welcome!";
		
		double delayInSeconds = 1.5;
		dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
		dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
			[self performSegueWithIdentifier:@"toLoginView" sender:self];
		});
	}
}

- (IBAction)resendVerificationEmail:(UIButton *)sender {
	NSLog(@"will resend verification email");
	sender.enabled = NO;
	
	[self.checkEmalButton setTitle:@"Done" forState:UIControlStateDisabled];
	self.checkEmalButton.enabled = NO;
	
	if ([jvht internetAccessIsAvailable]) {
		user.email = user.email;
		[user saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
			if (!error) {
				NSLog(@"should get email");
				[jvht showAlertViewForReason:JVHTEmailVerificationSent];
				checkEmailVerifiedTimer = [NSTimer scheduledTimerWithTimeInterval:5.0 target:self selector:@selector(recheckEmailVerified:) userInfo:nil repeats:YES];
			} else {
				[jvht showAlertViewForReason:JVHTGeneralError];
				sender.enabled = YES;
			}
		}];
	} else {
		[jvht showAlertViewForReason:JVHTNoInternetConnection];
		sender.enabled = YES;
	}
}

- (IBAction)goToDidNotGetEmailView:(UIButton *)sender {
}

- (IBAction)checkEmailVerification:(UIButton *)sender {
	[sender setTitle:@"Checking" forState:UIControlStateDisabled];
	
	sender.enabled = NO;
	[user refresh];
	NSLog(@"%@", user[@"name"]);
	
	if ([user[@"emailVerified"]boolValue] == YES) {
		NSLog(@"verified!");
		sender.enabled = YES;
		
		double delayInSeconds = 1.5;
		dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
		dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
			NSLog(@"TO THE HOME SCREEN...mobile?...nanananana");
			[self performSegueWithIdentifier:@"toHomeView" sender:self];
		});
	} else {
		sender.enabled = YES;
		self.welcomeLabel.text = @"Still haven't verfied:";
		NSLog(@"NOT verified");
	}
}

- (BOOL)recheckEmailVerified: (NSTimer *)timer {
	[user refresh];
	
	if ([user[@"emailVerified"]boolValue] == YES) {
		[timer invalidate];
		timer = nil;
		self.welcomeLabel.text = @"Verified! Hi,";
		self.determinedLabel.text = user[@"name"];
		self.loginButton.enabled = NO;
		self.changeEmailButton.enabled = NO;
		self.didNotGetEmailButton.enabled = NO;
		
		[UIView animateWithDuration:0.5 delay:0 options:UIViewAnimationOptionCurveLinear animations:^{
			self.resendEmailButton.alpha = 0;
			self.checkEmalButton.alpha = 0;
			self.loginButton.alpha = 0;
			self.changeEmailButton.alpha = 0;
			self.didNotGetEmailButton.alpha = 0;
			
			self.resendEmailButton.frame = CGRectOffset(self.resendEmailButton.frame, 0, 70.0);
			self.checkEmalButton.frame = CGRectOffset(self.checkEmalButton.frame, 0, 70.0);
			self.loginButton.frame = CGRectOffset(self.loginButton.frame, 0, 70.0);
			self.changeEmailButton.frame = CGRectOffset(self.changeEmailButton.frame, 0, 70.0);
			self.didNotGetEmailButton.frame = CGRectOffset(self.didNotGetEmailButton.frame, 0, 70.0);
		} completion:^(BOOL finished) {
			double delayInSeconds = 1.5;
			dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
			dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
				NSLog(@"TO THE HOME SCREEN...mobile?...nanananana");
				[self performSegueWithIdentifier:@"toHomeView" sender:self];
			});
		}];
		return YES;
	} else {
		NSLog(@"NOT verified");
		return NO;
	}
}

- (IBAction)changeEmail:(UIButton *)sender {
	// if user signed up with twitter,
	// have them sign in again first
	// check if username is equal to already signed in username
	
	[checkEmailVerifiedTimer invalidate];
	self.resendEmailButton.enabled = YES;
//	[self.resendEmailButton setTitle:@"Resend Email Verification" forState:UIControlStateNormal];
	[self.checkEmalButton setEnabled:YES];
	self.changeEmailButton.enabled = NO;
	[self.changeEmailButton setTitle:@"Done" forState:UIControlStateNormal];
	self.determinedLabel.enabled = YES;
	self.determinedLabel.backgroundColor = [UIColor colorWithWhite:1.0 alpha:0.9];
	self.determinedLabel.layer.borderWidth = 0.0;
	self.determinedLabel.layer.borderColor = [jvht getButtonHighlightedTextColor].CGColor;
	[self.determinedLabel becomeFirstResponder];
	[UIView animateWithDuration:0.25 delay:0 options:UIViewAnimationOptionCurveLinear animations:^{
		self.determinedLabel.frame = CGRectOffset(self.determinedLabel.frame, 0, -200);
		self.changeEmailButton.frame = CGRectMake(0, self.changeEmailButton.frame.origin.y - 240, [UIScreen mainScreen].bounds.size.width, self.changeEmailButton.frame.size.height + 10);
		[self.changeEmailButton.titleLabel setFont:[UIFont fontWithName:@"GillSans-Light" size:20]];
		self.determinedLabel.layer.borderWidth = 1.0;
	} completion:^(BOOL finished) {
		[self.changeEmailButton removeTarget:self action:@selector(changeEmail:) forControlEvents:UIControlEventTouchUpInside];
		[self.changeEmailButton addTarget:self action:@selector(finishedChangeEmail:) forControlEvents:UIControlEventTouchUpInside];
		self.changeEmailButton.enabled = YES;
	}];
}

- (IBAction)finishedChangeEmail:(UIButton *)sender {
	if ([jvht textIsValidEmail:self.determinedLabel.text]) {
		[self.determinedLabel resignFirstResponder];
		[self.determinedLabel setBackgroundColor:[UIColor clearColor]];
		self.determinedLabel.enabled = NO;
		self.changeEmailButton.enabled = NO;
		[self.changeEmailButton setTitle:@"Change Email" forState:UIControlStateNormal];
		self.welcomeLabel.text = @"One Moment...";
		[UIView animateWithDuration:0.25 delay:0 options:UIViewAnimationOptionCurveLinear animations:^{
			self.determinedLabel.frame = CGRectOffset(self.determinedLabel.frame, 0, 200);
			self.changeEmailButton.frame = CGRectMake(105, self.changeEmailButton.frame.origin.y + 240, 215, self.changeEmailButton.frame.size.height - 10);
			[self.changeEmailButton.titleLabel setFont:[UIFont fontWithName:@"GillSans-Light" size:15]];
			self.determinedLabel.layer.borderWidth = 0.0;
		} completion:^(BOOL finished) {
			[self.changeEmailButton removeTarget:self action:@selector(finishedChangeEmail:) forControlEvents:UIControlEventTouchUpInside];
			[self.changeEmailButton addTarget:self action:@selector(changeEmail:) forControlEvents:UIControlEventTouchUpInside];
			self.changeEmailButton.enabled = YES;
			
			self.ogEmail = [PFUser currentUser].email;
			if ([jvht internetAccessIsAvailable]) {
				if (![self.determinedLabel.text isEqualToString:[PFUser currentUser].email]) {
					[PFUser currentUser].email = self.determinedLabel.text;
					[PFUser currentUser].username = self.determinedLabel.text;
					[[PFUser currentUser] saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
						if (!error) {
							UIAlertView *checkEmail = [[UIAlertView alloc]initWithTitle:@"Verify Email" message:@"Please check your new email for a verification email. Then come back and hit \"Done\"" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
							[checkEmail show];
						} else if (error.code == kParseErrorUsernameTaken || error.code == kParseErrorEmailTaken) {
							UIAlertView *emailTaken = [[UIAlertView alloc]initWithTitle:@"Email Taken" message:@"That email is already registered. Did you mean to login?" delegate:self cancelButtonTitle:@"Retry" otherButtonTitles:@"Login", nil];
							emailTaken.accessibilityLabel = @"emailTaken";
							[emailTaken show];
							[PFUser currentUser].email = self.ogEmail;
							[PFUser currentUser].username = self.ogEmail;
							
							NSString *tempEmail = self.determinedLabel.text;
							self.determinedLabel.text = self.ogEmail;
							self.ogEmail = tempEmail;
							NSLog(@"%@", tempEmail);
						} else {
							[jvht showAlertViewForReason:JVHTGeneralError];
							[PFUser currentUser].email = self.ogEmail;
							[PFUser currentUser].username = self.ogEmail;
							
							self.determinedLabel.text = self.ogEmail;
						}
					}];
				}
				self.welcomeLabel.text = @"Please verify your email address:";
			} else {
				[jvht showAlertViewForReason:JVHTNoInternetConnection];
				self.welcomeLabel.text = @"Please verify your email address:";
				self.determinedLabel.text = self.ogEmail;
			}
			
		}];
	}
	
}

- (IBAction)validateEmail:(UITextField *)sender {
	validityLabel.text = @"Enter your email";
	if ([jvht textIsValidEmail:sender.text]) {
		validityLabel.backgroundColor = [UIColor greenColor];
		validityLabel.text = @"Email looks good :D";
		validityLabel.textColor = [UIColor colorWithWhite:0.4 alpha:1];
	} else {
		validityLabel.backgroundColor = [UIColor redColor];
		validityLabel.text = @"A valid email is required.";
		validityLabel.textColor = [UIColor whiteColor];
	}
}

- (IBAction)updateViewForEditing:(UITextField *)sender {
	if ([self.view.superview.subviews containsObject:validityLabel]) {
		validityLabel.text = @"Enter your email";
		if ([jvht textIsValidEmail:sender.text]) {
			validityLabel.backgroundColor = [UIColor greenColor];
			validityLabel.text = @"Email looks good :D";
			validityLabel.textColor = [UIColor colorWithWhite:0.4 alpha:1];
		} else {
			validityLabel.backgroundColor = [UIColor redColor];
			validityLabel.text = @"A valid email is required.";
			validityLabel.textColor = [UIColor whiteColor];
		}
	} else {
		CGRect validityFrame = CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 30);
		validityLabel = [[UILabel alloc]initWithFrame:validityFrame];
		[validityLabel setFont:[UIFont fontWithName:@"GillSans-Light" size:15]];
		validityLabel.backgroundColor = [UIColor redColor];
		validityLabel.textColor = [UIColor whiteColor];
		validityLabel.textAlignment = NSTextAlignmentCenter;
		validityLabel.alpha = 0.0;
		validityLabel.text = @"Enter your email";
		if ([jvht textIsValidEmail:sender.text]) {
			validityLabel.backgroundColor = [UIColor greenColor];
			validityLabel.text = @"Email looks good :D";
			validityLabel.textColor = [UIColor colorWithWhite:0.4 alpha:1];
		} else {
			validityLabel.backgroundColor = [UIColor redColor];
			validityLabel.text = @"A valid email is required.";
			validityLabel.textColor = [UIColor whiteColor];
		}
		
		[self.view.superview addSubview:validityLabel];
		
		[UIView animateWithDuration:0.25 delay:0 options:UIViewAnimationOptionCurveLinear animations:^{
			validityLabel.alpha = 1.0;
		} completion:^(BOOL finished) {
			// nilch
		}];
	}
}

- (IBAction)updateViewForFinishedEditing:(UITextField *)sender {
	if ([self.view.superview.subviews containsObject:validityLabel]) {
		[UIView animateWithDuration:0.25 delay:0 options:UIViewAnimationOptionCurveLinear animations:^{
			validityLabel.alpha = 0.0;
		} completion:^(BOOL finished) {
			if (finished) {
				[validityLabel removeFromSuperview];
			}
		}];
	}
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
	if (textField == self.determinedLabel) {
		[self finishedChangeEmail:self.changeEmailButton];
	}
	return YES;
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
	if ([alertView.accessibilityLabel isEqualToString:@"emailTaken"]) {
		if (buttonIndex == 1) {
			[self performSegueWithIdentifier:@"toLoginView" sender:self];
		}
	}
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
