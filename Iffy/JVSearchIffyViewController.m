//
//  JVSearchIffyViewController.m
//  Iffy
//
//  Created by Joseph Ayo-Vaughan on 3/7/14.
//  Copyright (c) 2014 Joseph Ayo-Vaughan. All rights reserved.
//

#import "JVSearchIffyViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "TextfieldLogin.h"
#import "SearchTableViewCell.h"
#import "JVHelperTools.h"

#define kLoadingBGHeight 100.0
#define kLoadingImageWidth 200.0
#define kLoadinImageHeight 75.0

@interface JVSearchIffyViewController () {
	JVHelperTools *jvht;
	UIView *headerView;
	BOOL isLoading;
	SearchTableViewCell *lastCellSelected;
}

@end

@implementation JVSearchIffyViewController

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        // Custom the table
        
        // The className to query on
        self.parseClassName = @"Foo";
        
        // The key of the PFObject to display in the label of the default cell style
        self.textKey = @"text";
        
        // Uncomment the following line to specify the key of a PFFile on the PFObject to display in the imageView of the default cell style
        // self.imageKey = @"image";
        
        // Whether the built-in pull-to-refresh is enabled
        self.pullToRefreshEnabled = YES;
        
        // Whether the built-in pagination is enabled
        self.paginationEnabled = YES;
        
        // The number of objects to show per page
        self.objectsPerPage = 25;
    }
    return self;
}

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}


#pragma mark - UIViewController

- (void)viewDidLoad {
    [super viewDidLoad];
	
	jvht = [JVHelperTools new];
	isLoading = NO;
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
	
	//	self.navigationController.hidesBottomBarWhenPushed = YES;
	//	self.navigationItem.hidesBackButton = YES;
	
	//	CGRect searchBarFrame = CGRectMake(10.0f, 100.0f, 100.0f, 31.0f);
	CGRect searchBarFrame = self.searchTextField.frame;
	//    self.searchTextField = [[TextfieldLogin alloc] initWithFrame:searchBarFrame];
	self.searchTextField.frame = CGRectMake(searchBarFrame.origin.x, searchBarFrame.origin.y, 100.0f, searchBarFrame.size.height);
	self.searchTextField.dxOffset = 10.0f;
	
    self.searchTextField.backgroundColor = UIColorFromRGB(0x772828);
    self.searchTextField.textColor = [UIColor colorWithWhite:0.85 alpha:1.0];
	
	//    self.searchTextField.borderStyle = UITextBorderStyleRoundedRect;
    self.searchTextField.clearButtonMode = UITextFieldViewModeWhileEditing;
    self.searchTextField.returnKeyType = UIReturnKeySearch;
	
	[self.searchTextField setAutocapitalizationType:UITextAutocapitalizationTypeNone];
	[self.searchTextField setAutocorrectionType:UITextAutocorrectionTypeNo];
	
    self.searchTextField.delegate = self;
	self.searchTextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Search Iffy..." attributes:@{NSForegroundColorAttributeName: [UIColor colorWithWhite:0.85 alpha: 0.7]}];
	[self.searchTextField setFont:[UIFont fontWithName:@"GillSans" size:16]];
	[self.searchTextField setContentVerticalAlignment:UIControlContentVerticalAlignmentCenter];
	
	self.searchTextField.borderStyle = UITextBorderStyleNone;
	self.searchTextField.layer.borderWidth = 1.0f;
	self.searchTextField.layer.borderColor = [UIColorFromRGB(0x6a2424) CGColor];
	self.searchTextField.layer.cornerRadius = 5.0f;
	
	if (!is5thGen) {
		[self.cancelSearchButton setContentEdgeInsets:UIEdgeInsetsMake(0, 0, 0, 13)];
	}
	
	//	if (is5thGen) {
	//		self.cancelSearchButton = [[UIButton alloc]initWithFrame:CGRectMake(20.0f, 100.0f, 50.0f, 31.0f)];
	//	} else {
	//		self.cancelSearchButton = [[UIButton alloc]initWithFrame:CGRectMake(20.0f, 150.0f, 50.0f, 31.0f)];
	//	}
	
	//	[self.cancelSearchButton setTitle:@"Cancel" forState:UIControlStateNormal];
	//	[self.cancelSearchButton setTitleColor:UIColorFromRGB(0xFFFFFF) forState:UIControlStateNormal];
	//	[self.cancelSearchButton setTitleColor:[UIColor colorWithWhite:0.7 alpha:1.0] forState:UIControlStateHighlighted];
	//	[self.cancelSearchButton addTarget:self action:@selector(dismissController:) forControlEvents:UIControlEventTouchUpInside];
	
	//	if (is5thGen) {
	//		[self.cancelSearchButton.titleLabel setFont:[UIFont fontWithName:@"GillSans" size:16]];
	//	} else {
	//		[self.cancelSearchButton.titleLabel setFont:[UIFont fontWithName:@"GillSans" size:18]];
	//	}
	
	//	[self.cancelSearchButton setContentHorizontalAlignment:UIControlContentHorizontalAlignmentCenter];
	
	//	UIBarButtonItem *searchBarItem = [[UIBarButtonItem alloc]initWithCustomView:self.searchTextField];
	//	UIBarButtonItem *cancelButtonBarItem = [[UIBarButtonItem alloc]initWithCustomView:self.cancelSearchButton];
	
	//	NSArray *searchItems = @[searchBarItem, cancelButtonBarItem];
	
	//	self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:self.searchTextField];
	//	self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithCustomView:self.cancelSearchButton];
	//	self.navigationItem.leftBarButtonItems = searchItems;
	
	//	self.tableView.hidden = YES;
	//	self.tableView.alpha = 0.3;
	
	// Set divider images
	//	[self.searchSegControl setDividerImage:[UIImage imageNamed:@"searchSeg-noneSel.png"]
	//	  forLeftSegmentState:UIControlStateNormal
	//		rightSegmentState:UIControlStateNormal
	//			   barMetrics:UIBarMetricsDefault];
	//	[self.searchSegControl setDividerImage:[UIImage imageNamed:@"searchSeg-leftSel.png"]
	//	  forLeftSegmentState:UIControlStateSelected
	//		rightSegmentState:UIControlStateNormal
	//			   barMetrics:UIBarMetricsDefault];
	//	[self.searchSegControl setDividerImage:[UIImage imageNamed:@"searchSeg-rightSel.png"]
	//	  forLeftSegmentState:UIControlStateNormal
	//		rightSegmentState:UIControlStateSelected
	//			   barMetrics:UIBarMetricsDefault];
	
	// Set background images
	//	UIImage *normalBackgroundImage = [UIImage imageNamed:@"searchSeg-normal"];
	//	[self.searchSegControl setBackgroundImage:normalBackgroundImage
	//					forState:UIControlStateNormal
	//				  barMetrics:UIBarMetricsDefault];
	//	UIImage *selectedBackgroundImage = [UIImage imageNamed:@"searchSeg-selected"];
	//	[self.searchSegControl setBackgroundImage:selectedBackgroundImage
	//					forState:UIControlStateSelected
	//				  barMetrics:UIBarMetricsDefault];
	
	NSDictionary *attr = @{UITextAttributeFont: [UIFont fontWithName:@"GillSans" size:16.0f], UITextAttributeTextShadowColor: [UIColor clearColor], UITextAttributeTextColor: [jvht getRedButtonNormalColor]};
	[self.searchSegControl setTitleTextAttributes:attr
										 forState:UIControlStateNormal];
	
	[self.searchSegControl setTitleTextAttributes:@{UITextAttributeTextColor: [UIColor whiteColor]} forState:UIControlStateSelected];
	
	[self.searchSegControl setBackgroundImage:[jvht imageFromColor:[UIColor colorWithWhite:0.95 alpha:1.0]] forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
	[self.searchSegControl setBackgroundImage:[jvht imageFromColor:[jvht getOrangeButtonNormalColor]] forState: UIControlStateHighlighted barMetrics:UIBarMetricsDefault];
	[self.searchSegControl setBackgroundImage:[jvht imageFromColor:[jvht getOrangeButtonNormalColor]] forState: UIControlStateSelected barMetrics:UIBarMetricsDefault];
	
	headerView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, self.bgHeaderView.bounds.size.height)];
	headerView.backgroundColor = [UIColor clearColor];
	for (UIView *view in self.bgHeaderView.subviews) {
		[headerView addSubview:view];
	}
	
	[self.tableView addSubview:headerView];
	
	
	self.refreshControl.tintColor = [jvht getOrangeButtonNormalColor];
	
	[self.tableView registerNib:[UINib nibWithNibName:@"SearchTableViewCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"JVCell"];
}

- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
	[[UIApplication sharedApplication]setStatusBarStyle:UIStatusBarStyleLightContent animated:YES];
	if (is5thGen) {
		[[UIApplication sharedApplication]setStatusBarHidden:YES withAnimation:UIStatusBarAnimationSlide];
	}
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
	
	[UIView animateWithDuration:0.5 delay:0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
		CGRect searchBarFrame = self.searchTextField.frame;
		[self.searchTextField setFrame:CGRectMake(searchBarFrame.origin.x, searchBarFrame.origin.y, 225.0f, searchBarFrame.size.height)];
	} completion:^(BOOL finished) {
		[self.searchTextField becomeFirstResponder];
	}];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
	if (is5thGen) {
		[[UIApplication sharedApplication]setStatusBarHidden:NO withAnimation:UIStatusBarAnimationSlide];
	}
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (IBAction)dismissController:(id)sender {
	[self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)dismissKeyboard:(id)sender {
	for (UITextField *tf in self.view.subviews) {
		[tf resignFirstResponder];
	}
	[self.searchTextField resignFirstResponder];
}

- (IBAction)updateSearchResults:(TextfieldLogin *)sender {
	[self loadObjects];
}

#pragma mark - PFQueryTableViewController

- (void)objectsWillLoad {
    [super objectsWillLoad];
    
	// This method is called before a PFQuery is fired to get more objects
	if (isLoading == NO) {
		[jvht addProgressviewToSuperview:self.view.superview];
		isLoading = YES;
	}
}

- (void)objectsDidLoad:(NSError *)error {
    [super objectsDidLoad:error];
    
	//	This method is called every time objects are loaded from Parse via the PFQuery
	[jvht removeProgressViewFromView: self.view.superview];
	isLoading = NO;
}

// Override to customize what kind of query to perform on the class. The default is to query for
// all objects ordered by createdAt descending.
- (PFQuery *)queryForTable {
	//	PFQuery *query = [PFQuery queryWithClassName:self.parseClassName];
	PFQuery *query = [PFUser query];
	
	[query whereKey:@"name" hasPrefix:self.searchTextField.text];
	
	// If Pull To Refresh is enabled, query against the network by default.
	if (self.pullToRefreshEnabled) {
		query.cachePolicy = kPFCachePolicyNetworkOnly;
	}
	
	// If no objects are loaded in memory, we look to the cache first to fill the table
	// and then subsequently do a query against the network.
	if (self.objects.count == 0) {
		query.cachePolicy = kPFCachePolicyCacheThenNetwork;
	}
	
	[query orderByDescending:@"createdAt"];
	
	return query;
}

// Override to customize the look of a cell representing an object. The default is to display
// a UITableViewCellStyleDefault style cell with the label being the textKey in the object,
// and the imageView being the imageKey in the object.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath object:(PFObject *)object {
	static NSString *CellIdentifier = @"JVCell";
	
	SearchTableViewCell *cell = (SearchTableViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
	
	if (!cell) {
		NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"SearchTableViewCell" owner:self options:nil];
		cell = [nib objectAtIndex:0];
	}
	
	cell.userImageView.layer.masksToBounds = YES;
	cell.userImageView.layer.cornerRadius = cell.userImageView.frame.size.height/2;
	
	PFUser *user = (PFUser *)object;
	cell.userNameLabel.text = user[@"name"];
	cell.userInfoLabel.text = user.email;
	
	if (indexPath.row % 2 == 0) {
		cell.arrowImageView.image = [UIImage imageNamed:@"forward-red.png"];
	} else {
		cell.arrowImageView.image = [UIImage imageNamed:@"forward-orange.png"];
	}
	
	if (user[@"photo_small"] != nil) {
		PFFile *imageFile = user[@"photo_small"];
		[imageFile getDataInBackgroundWithBlock:^(NSData *data, NSError *error) {
			if (!error) {
				cell.userImageView.image = [UIImage imageWithData:data];
			}
		}];
	} else {
		cell.userImageView.image = [UIImage imageNamed:@"default_photo.png"];
	}
	return cell;
}

/*
 // Override if you need to change the ordering of objects in the table.
 - (PFObject *)objectAtIndex:(NSIndexPath *)indexPath {
 return [self.objects objectAtIndex:indexPath.row];
 }
 */


// Override to customize the look of the cell that allows the user to load the next page of objects.
// The default implementation is a UITableViewCellStyleDefault cell with simple labels.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForNextPageAtIndexPath:(NSIndexPath *)indexPath {
	static NSString *CellIdentifier = @"NextPage";
	
	UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
	
	if (cell == nil) {
		cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
	}
	
	return cell;
}


#pragma mark - UITableViewDataSource

/*
 // Override to support conditional editing of the table view.
 - (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
 // Return NO if you do not want the specified item to be editable.
 return YES;
 }
 */

/*
 // Override to support editing the table view.
 - (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
 if (editingStyle == UITableViewCellEditingStyleDelete) {
 // Delete the object from Parse and reload the table view
 } else if (editingStyle == UITableViewCellEditingStyleInsert) {
 // Create a new instance of the appropriate class, and save it to Parse
 }
 }
 */

/*
 // Override to support rearranging the table view.
 - (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
 }
 */

/*
 // Override to support conditional rearranging of the table view.
 - (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
 // Return NO if you do not want the item to be re-orderable.
 return YES;
 }
 */

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [super tableView:tableView didSelectRowAtIndexPath:indexPath];
	
	lastCellSelected.userInteractionEnabled = YES;
	[UIView animateWithDuration:1.0 delay:0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
		lastCellSelected.selectedView.frame = CGRectMake(0, 0, 5, 60);
		lastCellSelected.selectedView.alpha = 0.0;
	} completion:nil];
		
	SearchTableViewCell *cell = (SearchTableViewCell *)[tableView cellForRowAtIndexPath:indexPath];
	lastCellSelected = cell;
	
	cell.selectedView.backgroundColor = [UIColor colorWithWhite:0.9 alpha:1.0];
	cell.userInteractionEnabled = NO;
	
	[UIView animateWithDuration:1.0 delay:0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
		cell.selectedView.alpha = 1.0;
		cell.selectedView.frame = CGRectMake(0, 0, cell.frame.size.width, cell.frame.size.height);
	} completion:nil];
	
	if (indexPath.row + 1 < [self.objects count]) {
		PFObject *obj = [self.objects objectAtIndex:indexPath.row];
		PFUser *user = (PFUser *)obj;
		NSLog(@"%@", [user objectId]);
	}
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 60;
}

#pragma mark UIScrollViewDelegate

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
	[self dismissKeyboard:self.searchTextField];
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
	float dyOffset;
	if (is5thGen) {
		dyOffset = 44.0f;
	} else {
		dyOffset = 0.0f;
	}
	
	CGRect headerViewFrame = headerView.frame;
	headerView.frame = CGRectMake(headerViewFrame.origin.x, self.tableView.contentOffset.y + dyOffset, headerViewFrame.size.width, headerViewFrame.size.height);
	if (self.tableView.contentOffset.y + dyOffset >= 0.0f) {
		headerView.alpha = 1.0f;
		//		self.searchSegControl.enabled = YES;
	} else {
		headerView.alpha = 0.4f;
		//		self.searchSegControl.enabled = NO;
	}
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
	[self dismissKeyboard:textField];
	[self loadObjects];
	return YES;
}

@end
