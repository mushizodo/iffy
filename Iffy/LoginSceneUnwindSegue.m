//
//  LoginSceneUnwindSegue.m
//  Iffy
//
//  Created by Joseph Ayo-Vaughan on 1/14/14.
//  Copyright (c) 2014 Joseph Ayo-Vaughan. All rights reserved.
//

#import "LoginSceneUnwindSegue.h"
#import "LoginViewController.h"

@implementation LoginSceneUnwindSegue

-(void)perform {
	UIViewController *src = self.sourceViewController;
	
	if ([self.destinationViewController isKindOfClass:[LoginViewController class]]) {
		LoginViewController *dest = self.destinationViewController;
		dest.mainBgImage.image = [UIImage imageNamed:@"red_login_bg.jpg"];
		dest.signUpButton.enabled = YES;
		dest.signInTwitter.enabled = YES;
		dest.loginButton.enabled = YES;
		dest.infoButton.enabled = YES;
		dest.forgotPasswordButton.enabled = NO;
		
		dest.forgotPasswordButton.alpha = 0.0;
		dest.nameField.alpha = 0.0;
		dest.emailField.alpha = 0.0;
		dest.passwordField.alpha = 0.0;
		
		dest.faceLogo.frame = CGRectMake(dest.faceLogo.frame.origin.x, 525.0, dest.faceLogo.frame.size.width, dest.faceLogo.frame.size.height);
		
		[src.view.superview addSubview:dest.view];
		
		UIScreen *screen = [UIScreen mainScreen];
		
		dest.view.frame = CGRectMake(0, -(screen.bounds.size.height), dest.view.frame.size.width, dest.view.frame.size.height);
		
		[UIView animateWithDuration:0.5 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
			src.view.layer.frame = CGRectMake(0, screen.bounds.size.height, src.view.frame.size.width, src.view.frame.size.height);
			dest.view.layer.frame = CGRectMake(0, 0, dest.view.frame.size.width, dest.view.frame.size.height);
		} completion:^(BOOL finished) {
			if (finished) {
				[dest.view removeFromSuperview];
				[src dismissViewControllerAnimated:NO completion:nil];
			}
		}];
	} else {
		UIViewController *dest = self.destinationViewController;
		
		[src.view.superview addSubview:dest.view];
		
		UIScreen *screen = [UIScreen mainScreen];
		
		dest.view.frame = CGRectMake(0, -(screen.bounds.size.height), dest.view.frame.size.width, dest.view.frame.size.height);
		
		[UIView animateWithDuration:0.5 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
			src.view.layer.frame = CGRectMake(0, screen.bounds.size.height, src.view.frame.size.width, src.view.frame.size.height);
			dest.view.layer.frame = CGRectMake(0, 0, dest.view.frame.size.width, dest.view.frame.size.height);
		} completion:^(BOOL finished) {
			if (finished) {
				[dest.view removeFromSuperview];
				[src dismissViewControllerAnimated:NO completion:nil];
			}
		}];
	}
}

@end
