//
//  TextfieldLogin.m
//  Iffy
//
//  Created by Joseph Ayo-Vaughan on 1/9/14.
//  Copyright (c) 2014 Joseph Ayo-Vaughan. All rights reserved.
//

#import "TextfieldLogin.h"

@implementation TextfieldLogin

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
		
		self.dxOffset = 20.0;
		if (is5thGen) {
			[[UITextField appearance] setTintColor:[UIColor colorWithRed:255.0/255.0 green:114.0/255.0 blue:45.0/255.0 alpha:1]];
		}
		
		
    }
    return self;
}

-(CGRect)textRectForBounds:(CGRect)bounds {
	[super textRectForBounds:bounds];
	return CGRectInset(bounds, self.dxOffset, 0);
}

-(CGRect)editingRectForBounds:(CGRect)bounds {
	[super editingRectForBounds:bounds];
	return CGRectInset(bounds, self.dxOffset, 0);
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
