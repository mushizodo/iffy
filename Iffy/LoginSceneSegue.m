//
//  LoginSceneSegue.m
//  Iffy
//
//  Created by Joseph Ayo-Vaughan on 1/14/14.
//  Copyright (c) 2014 Joseph Ayo-Vaughan. All rights reserved.
//

#import "LoginSceneSegue.h"
#import "LoginViewController.h"
#import "FinishTwitterViewController.h"
#import "VerificationViewController.h"
#import <Parse/Parse.h>

@implementation LoginSceneSegue

-(void)perform {
	UIViewController *src = self.sourceViewController;
	UIViewController *dest = self.destinationViewController;
	
	[src.view.superview addSubview:dest.view];
	
	if ([src isKindOfClass:[VerificationViewController class]]) {
		((LoginViewController *)dest).emailField.text = [PFUser currentUser].email;
		NSLog(@"YO: %@", ((VerificationViewController *)src).ogEmail);
	}
	
	if ([self.identifier isEqualToString:@"toGetEmail"]) {
		((FinishTwitterViewController *)dest).tNameField.text = ((LoginViewController *)src).userName;
	}
	
	UIScreen *screen = [UIScreen mainScreen];
	
	dest.view.frame = CGRectMake(0, screen.bounds.size.height, dest.view.frame.size.width, dest.view.frame.size.height);
	
	[UIView animateWithDuration:0.5
						  delay:0
						options:UIViewAnimationOptionCurveEaseOut
					 animations:^{
						 src.view.frame = CGRectMake(0, -(screen.bounds.size.height), src.view.frame.size.width, src.view.frame.size.height);
						 dest.view.frame = CGRectMake(0, 0, dest.view.frame.size.width, dest.view.frame.size.height);
					 } completion:^(BOOL finished) {
						 if (finished) {
							[dest.view removeFromSuperview];
							 [src presentViewController:dest animated:NO completion:nil];
							 if ([src isKindOfClass:[VerificationViewController class]]) {
								 [(LoginViewController *)dest animateForLogin:((LoginViewController *)dest).loginButton];
							 }
						 }
					 }
	 ];
}

@end
