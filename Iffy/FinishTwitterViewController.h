//
//  FinishTwitterViewController.h
//  Iffy
//
//  Created by Joseph Ayo-Vaughan on 1/10/14.
//  Copyright (c) 2014 Joseph Ayo-Vaughan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FinishTwitterViewController : UIViewController <UITextFieldDelegate, UIAlertViewDelegate>

@property (weak, nonatomic) IBOutlet UITextField *tEmailField;
@property (weak, nonatomic) IBOutlet UITextField *tNameField;
@property (weak, nonatomic) IBOutlet UIButton *finishTwitterButton;
@property (weak, nonatomic) IBOutlet UIButton *backButton;
@property (weak, nonatomic) IBOutlet UIButton *infoButton;
@property (weak, nonatomic) IBOutlet UILabel *hiLabel;
@property (weak, nonatomic) IBOutlet UILabel *emailLabel;
@property (weak, nonatomic) IBOutlet UIButton *resignerButton;

- (IBAction)finishTwitterSignup:(id)sender;
- (IBAction)resignKeyboard:(id)sender;
- (IBAction)updateViewForEditing:(UITextField *)sender;
- (IBAction)updateViewForFinishedEditing:(id)sender;
- (IBAction)slidePageView:(UIPanGestureRecognizer *)recognizer;
- (IBAction)validateTextFieldInput:(UITextField *)sender;
@end
