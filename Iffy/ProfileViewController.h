//
//  ProfileViewController.h
//  Iffy
//
//  Created by Joseph Ayo-Vaughan on 1/25/14.
//  Copyright (c) 2014 Joseph Ayo-Vaughan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProfileViewController : UIViewController <UISearchBarDelegate, UISearchDisplayDelegate, UIScrollViewDelegate, UITableViewDelegate>
@property (weak, nonatomic) IBOutlet UIScrollView *mainScrollView;
@property (weak, nonatomic) IBOutlet UIView *nameBG;
@property (weak, nonatomic) IBOutlet UIButton *viewPhotoButton;
@property (weak, nonatomic) IBOutlet UIImageView *profilePhotoView;
@property (weak, nonatomic) IBOutlet UIImageView *profilePhotoViewBG;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UITableView *mainTableView;

- (IBAction)viewProfilePic:(UIButton *)sender;
- (void)beginQueriesWithSearchText: (NSString *)searchText;
- (void)slowDownScrollWithGestureRecognizer: (UIPanGestureRecognizer *)recognizer;
- (void)refreshSearchTableView;
@end
