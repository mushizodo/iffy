
// To check if a library is compiled with CocoaPods you
// can use the `COCOAPODS` macro definition which is
// defined in the xcconfigs so it is available in
// headers also when they are imported in the client
// project.


// AMBubbleTableViewController
#define COCOAPODS_POD_AVAILABLE_AMBubbleTableViewController
#define COCOAPODS_VERSION_MAJOR_AMBubbleTableViewController 0
#define COCOAPODS_VERSION_MINOR_AMBubbleTableViewController 5
#define COCOAPODS_VERSION_PATCH_AMBubbleTableViewController 1

// Bolts
#define COCOAPODS_POD_AVAILABLE_Bolts
#define COCOAPODS_VERSION_MAJOR_Bolts 1
#define COCOAPODS_VERSION_MINOR_Bolts 1
#define COCOAPODS_VERSION_PATCH_Bolts 0

// Facebook-iOS-SDK
#define COCOAPODS_POD_AVAILABLE_Facebook_iOS_SDK
#define COCOAPODS_VERSION_MAJOR_Facebook_iOS_SDK 3
#define COCOAPODS_VERSION_MINOR_Facebook_iOS_SDK 15
#define COCOAPODS_VERSION_PATCH_Facebook_iOS_SDK 0

// HPGrowingTextView
#define COCOAPODS_POD_AVAILABLE_HPGrowingTextView
#define COCOAPODS_VERSION_MAJOR_HPGrowingTextView 1
#define COCOAPODS_VERSION_MINOR_HPGrowingTextView 1
#define COCOAPODS_VERSION_PATCH_HPGrowingTextView 0

// Parse
#define COCOAPODS_POD_AVAILABLE_Parse
#define COCOAPODS_VERSION_MAJOR_Parse 1
#define COCOAPODS_VERSION_MINOR_Parse 2
#define COCOAPODS_VERSION_PATCH_Parse 19

// Reachability
#define COCOAPODS_POD_AVAILABLE_Reachability
#define COCOAPODS_VERSION_MAJOR_Reachability 3
#define COCOAPODS_VERSION_MINOR_Reachability 1
#define COCOAPODS_VERSION_PATCH_Reachability 1

// SWTableViewCell
#define COCOAPODS_POD_AVAILABLE_SWTableViewCell
#define COCOAPODS_VERSION_MAJOR_SWTableViewCell 0
#define COCOAPODS_VERSION_MINOR_SWTableViewCell 3
#define COCOAPODS_VERSION_PATCH_SWTableViewCell 0

// SinchRTC
#define COCOAPODS_POD_AVAILABLE_SinchRTC
#define COCOAPODS_VERSION_MAJOR_SinchRTC 3
#define COCOAPODS_VERSION_MINOR_SinchRTC 1
#define COCOAPODS_VERSION_PATCH_SinchRTC 1

// TURecipientBar
#define COCOAPODS_POD_AVAILABLE_TURecipientBar
#define COCOAPODS_VERSION_MAJOR_TURecipientBar 1
#define COCOAPODS_VERSION_MINOR_TURecipientBar 1
#define COCOAPODS_VERSION_PATCH_TURecipientBar 1

